Droiddemo
====

This is a demo to explore various features of the Android system. Some highlight include:

- Show current activity name of any app in notification thanks to accessibility service.
- Show various `android.os.Build` constant
- Show return value of various path-related function
- Show system properties
- Locale explorer
- Create launcher shortcut
- Resolve sms/phone app
- how to configure gradle to build multiple version app, and override app name in debug build
- how to modularize app
