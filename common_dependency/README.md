Android Common Dependency Library
=================================

This is a common Dependency library intended to be used by all Android 
library. It provide some basic dependencies that all libraries will need.

# Build Requirement

These dependencies are required if you want to builid this library. Not
needed if you just want to use this library, of cause you can only use
this library in Android.

1. Environment variable `ANDROID_HOME` properly set.
2. `support-annotations:25.0.1` (`com.android.support:support-annotations:25.0.1` 
in `${ANDROID_HOME}/extras/android/m2repository`, use Android SDK Manager to download it)
3. `Android 7.1 SDK` (`${ANDROID_HOME}/platforms/android-25/android.jar`)
