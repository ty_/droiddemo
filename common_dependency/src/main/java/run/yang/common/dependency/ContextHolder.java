package run.yang.common.dependency;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;

/**
 * Created by zisxks on 2016-06-13 16:52
 */

public class ContextHolder {
    @SuppressLint("StaticFieldLeak")
    private static Context sContext;

    public static void init(@NonNull Context context) {
        if (context instanceof Application) {
            sContext = context;
        } else {
            sContext = context.getApplicationContext();
        }
    }

    @NonNull
    public static Context appContext() {
        return sContext;
    }
}
