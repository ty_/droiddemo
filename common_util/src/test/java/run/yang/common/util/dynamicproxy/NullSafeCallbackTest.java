package run.yang.common.util.dynamicproxy;

import android.annotation.SuppressLint;
import android.app.Application;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import run.yang.common.util.dynamicproxy.callback.ChatUiConfigDependencyDefaultImpl;
import run.yang.common.util.dynamicproxy.callback.IChatUiConfigDependency;
import run.yang.common.util.exception.ExceptionTool;
import run.yang.common.util.sdk.CommonSdkDependencyDefaultImpl;
import run.yang.common.util.sdk.CommonUtilSdk;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * 创建时间: 2017/05/15 18:23 <br>
 * 作者: Yang Tianmei <br>
 * 描述:
 */
@RunWith(MockitoJUnitRunner.class)
public class NullSafeCallbackTest {

    private static final String TEST_KEY = "test_key";

    @SuppressLint("StaticFieldLeak")
    @Mock
    static Application mContext;

    private IChatUiConfigDependency mDependencyImpl;

    @Before
    public void setUp() throws Exception {
        mDependencyImpl = new ChatUiConfigDependencyDefaultImpl();

        CommonUtilSdk.init(mContext, new CommonSdkDependencyDefaultImpl(), false);
    }

    @After
    public void tearDown() throws Exception {
        mDependencyImpl = null;
    }

    @Test
    public void wrap() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class);

        assertNotNull(dependency);
        assertNull(NullSafeCallback.getCallback(dependency));
    }

    @Test
    public void wrapWithInstance() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class, mDependencyImpl);

        assertNotNull(dependency);
        assertEquals(NullSafeCallback.getCallback(dependency), mDependencyImpl);
    }

    @Test
    public void wrapWithInstanceAndClassLoader() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();

        IChatUiConfigDependency dependency = NullSafeCallback.wrap(classLoader, IChatUiConfigDependency.class, mDependencyImpl);

        assertNotNull(dependency);
        assertEquals(NullSafeCallback.getCallback(dependency), mDependencyImpl);
    }

    @Test
    public void update_InitiallyNull() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class);

        NullSafeCallback.update(dependency, mDependencyImpl);
        assertEquals(NullSafeCallback.getCallback(dependency), mDependencyImpl);

        NullSafeCallback.update(dependency, null);
        assertNull(NullSafeCallback.getCallback(dependency));
    }

    @Test
    public void update_InitiallyNotNull() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class, mDependencyImpl);

        NullSafeCallback.update(dependency, null);
        assertNull(NullSafeCallback.getCallback(dependency));
    }

    @Test
    public void getCallback() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class);
        assertNull(NullSafeCallback.getCallback(dependency));

        dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class, mDependencyImpl);
        assertEquals(NullSafeCallback.getCallback(dependency), mDependencyImpl);
    }

    @Test
    public void getBooleanValue() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class);
        assertEquals(false, dependency.getBooleanValue(TEST_KEY, true));

        NullSafeCallback.update(dependency, mDependencyImpl);
        assertEquals(true, dependency.getBooleanValue(TEST_KEY, true));
    }

    @Test
    public void getByteValue() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class);
        assertEquals((byte) 0, dependency.getByteValue(TEST_KEY, (byte) 42));

        NullSafeCallback.update(dependency, mDependencyImpl);
        assertEquals((byte) 42, dependency.getByteValue(TEST_KEY, (byte) 42));
    }

    @Test
    public void getCharValue() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class);
        assertEquals(0, dependency.getCharValue(TEST_KEY, 'c'));

        NullSafeCallback.update(dependency, mDependencyImpl);
        assertEquals('c', dependency.getCharValue(TEST_KEY, 'c'));
    }

    @Test
    public void getShortValue() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class);
        assertEquals(0, dependency.getShortValue(TEST_KEY, (short) 42));

        NullSafeCallback.update(dependency, mDependencyImpl);
        assertEquals((short) 42, dependency.getShortValue(TEST_KEY, (short) 42));
    }

    @Test
    public void getIntValue() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class);
        assertEquals(0, dependency.getIntValue(TEST_KEY, 42));

        NullSafeCallback.update(dependency, mDependencyImpl);
        assertEquals(42, dependency.getIntValue(TEST_KEY, 42));
    }

    @Test
    public void getIntegerValue() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class);
        assertEquals(null, dependency.getIntegerValue(TEST_KEY, 42));
        assertEquals(null, dependency.getIntegerValue(TEST_KEY, null));

        NullSafeCallback.update(dependency, mDependencyImpl);
        assertEquals((Object) 42, dependency.getIntegerValue(TEST_KEY, 42));
        assertEquals(null, dependency.getIntegerValue(TEST_KEY, null));
    }

    @Test
    public void getIntegerValueWithBoxedDefault() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class);
        assertEquals(0, dependency.getIntegerValueWithBoxedDefault(TEST_KEY, 42));
        assertEquals(0, dependency.getIntegerValueWithBoxedDefault(TEST_KEY, null));

        NullSafeCallback.update(dependency, mDependencyImpl);
        assertEquals(42, dependency.getIntegerValueWithBoxedDefault(TEST_KEY, 42));
    }

    @Test(expected = NullPointerException.class)
    public void getIntegerValueWithBoxedDefaultNullDefault() throws Throwable {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class, mDependencyImpl);

        try {
            assertEquals(0, dependency.getIntegerValueWithBoxedDefault(TEST_KEY, null));
        } catch (UndeclaredThrowableException e) {
            throw ExceptionTool.getRootCause(e);
        }
    }

    @Test
    public void getLongValue() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class);
        assertEquals(0L, dependency.getLongValue(TEST_KEY, 42L));

        NullSafeCallback.update(dependency, mDependencyImpl);
        assertEquals(42L, dependency.getLongValue(TEST_KEY, 42L));
    }

    @Test
    public void getFloatValue() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class);
        assertEquals(0F, dependency.getFloatValue(TEST_KEY, 42F), 1E-8);

        NullSafeCallback.update(dependency, mDependencyImpl);
        assertEquals(42F, dependency.getFloatValue(TEST_KEY, 42F), 1E-8);
    }

    @Test
    public void getDoubleValue() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class);
        assertEquals(0, dependency.getDoubleValue(TEST_KEY, 42), 1E-12D);

        NullSafeCallback.update(dependency, mDependencyImpl);
        assertEquals(42, dependency.getDoubleValue(TEST_KEY, 42), 1E-12);
    }

    @Test
    public void getStringValue() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class);
        assertEquals(null, dependency.getStringValue(TEST_KEY, "string"));

        NullSafeCallback.update(dependency, mDependencyImpl);
        assertEquals("string", dependency.getStringValue(TEST_KEY, "string"));
        assertEquals(null, dependency.getStringValue(TEST_KEY, null));
    }

    @Test
    public void getStringSetValue() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class);
        Set<String> defaultValue = Collections.emptySet();

        assertEquals(null, dependency.getStringSetValue(TEST_KEY, defaultValue));

        NullSafeCallback.update(dependency, mDependencyImpl);
        assertEquals(defaultValue, dependency.getStringSetValue(TEST_KEY, defaultValue));
        assertEquals(null, dependency.getStringSetValue(TEST_KEY, null));

        defaultValue = new HashSet<>();
        defaultValue.add("String");
        assertEquals(defaultValue, dependency.getStringSetValue(TEST_KEY, defaultValue));
    }

    @Test
    public void voidMethod() throws Exception {
        IChatUiConfigDependency dependency = NullSafeCallback.wrap(IChatUiConfigDependency.class);
        dependency.voidMethod(TEST_KEY);

        NullSafeCallback.update(dependency, mDependencyImpl);
        dependency.voidMethod(TEST_KEY);
    }
}
