package run.yang.common.util.dynamicproxy.callback;

import android.content.SharedPreferences;

import java.util.Set;

import androidx.annotation.NonNull;

/**
 * 创建时间: 2017/05/11 12:13 <br>
 * 作者: Yang Tianmei <br>
 * 描述: chatui SDK 配置接口，以 key - value 的形式支持各种配置
 * <p>
 * <p>支持的接口与 {@link SharedPreferences} 对应</p>
 * <p>
 * <p>{@link run.yang.common.util.dynamicproxy.NullSafeCallback} 要测试的接口</p>
 * <p>
 * boolean, byte, char, short, int, long, float, and double.
 */

public interface IChatUiConfigDependency {

    boolean getBooleanValue(@NonNull String key, boolean defaultValue);

    byte getByteValue(@NonNull String key, byte defaultValue);

    char getCharValue(@NonNull String key, char defaultValue);

    short getShortValue(@NonNull String key, short defaultValue);

    int getIntValue(@NonNull String key, int defaultValue);

    Integer getIntegerValue(@NonNull String key, Integer defaultValue);

    int getIntegerValueWithBoxedDefault(@NonNull String key, Integer defaultValue);

    long getLongValue(@NonNull String key, long defaultValue);

    float getFloatValue(@NonNull String key, float defaultValue);

    double getDoubleValue(@NonNull String key, double defaultValue);

    String getStringValue(@NonNull String key, String defaultValue);

    Set<String> getStringSetValue(@NonNull String key, Set<String> defaultValue);

    void voidMethod(@NonNull String key);
}
