package run.yang.common.util.dynamicproxy;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import run.yang.common.util.dynamicproxy.callback.ChatUiConfigDependencyDefaultImpl;
import run.yang.common.util.dynamicproxy.callback.IChatUiConfigDependency;

/**
 * 创建时间: 2017/05/15 18:12 <br>
 * 作者: Yang Tianmei <br>
 * 描述:
 */
public class NullSafeCallbackWrapperTest {
    private NullSafeCallbackWrapper<IChatUiConfigDependency> mWrapper;
    private IChatUiConfigDependency mDependencyImpl;

    @Before
    public void setUp() throws Exception {
        mWrapper = new NullSafeCallbackWrapper<>(null);
        mDependencyImpl = new ChatUiConfigDependencyDefaultImpl();
    }

    @After
    public void tearDown() throws Exception {
        mWrapper = null;
        mDependencyImpl = null;
    }

    @Test
    public void setCallback() throws Exception {
        mWrapper.setCallback(mDependencyImpl);
        Assert.assertEquals(mDependencyImpl, mWrapper.getCallback());
    }

    @Test
    public void getCallback() throws Exception {
        Assert.assertNull(mWrapper.getCallback());

        mWrapper.setCallback(mDependencyImpl);
        Assert.assertEquals(mWrapper.getCallback(), mDependencyImpl);

        mWrapper.setCallback(null);
        Assert.assertNull(mWrapper.getCallback());
    }
}
