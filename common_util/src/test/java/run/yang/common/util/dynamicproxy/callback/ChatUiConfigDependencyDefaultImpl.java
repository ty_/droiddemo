package run.yang.common.util.dynamicproxy.callback;

import java.util.Set;

import androidx.annotation.NonNull;

/**
 * 创建时间: 2017/05/15 18:17 <br>
 * 作者: Yang Tianmei <br>
 * 描述:
 */

public class ChatUiConfigDependencyDefaultImpl implements IChatUiConfigDependency {

    @Override
    public boolean getBooleanValue(@NonNull String key, boolean defaultValue) {
        return defaultValue;
    }

    @Override
    public byte getByteValue(@NonNull String key, byte defaultValue) {
        return defaultValue;
    }

    @Override
    public char getCharValue(@NonNull String key, char defaultValue) {
        return defaultValue;
    }

    @Override
    public short getShortValue(@NonNull String key, short defaultValue) {
        return defaultValue;
    }

    @Override
    public int getIntValue(@NonNull String key, int defaultValue) {
        return defaultValue;
    }

    @Override
    public Integer getIntegerValue(@NonNull String key, Integer defaultValue) {
        return defaultValue;
    }

    @Override
    public int getIntegerValueWithBoxedDefault(@NonNull String key, Integer defaultValue) {
        return defaultValue;
    }

    @Override
    public long getLongValue(@NonNull String key, long defaultValue) {
        return defaultValue;
    }

    @Override
    public float getFloatValue(@NonNull String key, float defaultValue) {
        return defaultValue;
    }

    @Override
    public double getDoubleValue(@NonNull String key, double defaultValue) {
        return defaultValue;
    }

    @Override
    public String getStringValue(@NonNull String key, String defaultValue) {
        return defaultValue;
    }

    @Override
    public Set<String> getStringSetValue(@NonNull String key, Set<String> defaultValue) {
        return defaultValue;
    }

    @Override
    public void voidMethod(@NonNull String key) {

    }
}
