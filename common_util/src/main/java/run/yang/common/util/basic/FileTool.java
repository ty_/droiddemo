package run.yang.common.util.basic;

import android.text.TextUtils;

import java.io.File;

import androidx.annotation.Nullable;

/**
 * Created by Yang Tianmei on 2015-08-05 20:56.
 */
public class FileTool {

    public static boolean deleteFileTree(@Nullable final String rootDirectory, final boolean keepRoot) {
        if (TextUtils.isEmpty(rootDirectory)) {
            return false;
        }
        final File root = new File(rootDirectory).getAbsoluteFile();
        boolean ret = deleteFileTreeRecursive(root.listFiles());
        if (!keepRoot) {
            ret &= root.delete();
        }
        return ret;
    }

    private static boolean deleteFileTreeRecursive(@Nullable final File[] files) {
        if (files == null) {
            return true;
        }
        boolean ret = true;
        for (File file : files) {
            if (file.isDirectory()) {
                File[] filesChild = file.listFiles();
                deleteFileTreeRecursive(filesChild);
            }
            ret &= file.delete();
        }
        return ret;
    }
}
