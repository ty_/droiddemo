package run.yang.common.util.root;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by Tim Yang on 2015-05-11 19:59
 * <p/>
 * Function:
 */
public class CommandResult {
    public int exit_code = 0;

    @Nullable
    public String stdout = null;

    @Nullable
    public String stderr = null;

    @Nullable
    public Throwable exception = null;

    @NonNull
    public String getResultDescription() {
        StringBuilder displayMessage = new StringBuilder();
        displayMessage.append("exit code: ").append(exit_code).append("\n\n");

        displayMessage.append("Command stdout:\n");
        displayMessage.append(stdout).append('\n');

        if (stderr != null) {
            displayMessage.append("Command stderr:\n");
            displayMessage.append(stderr).append('\n');
        }

        if (exception != null) {
            displayMessage.append("Exception occurred:\n");
            displayMessage.append(getExceptionMessage());
        }

        return displayMessage.toString();
    }

    public String getExceptionMessage() {
        return Log.getStackTraceString(exception);
    }

    @NonNull
    @Override
    public String toString() {
        return "CommandResult{" +
                "exit_code=" + exit_code +
                ", stdout='" + stdout + '\'' +
                ", stderr='" + stderr + '\'' +
                ", exception=" + exception +
                '}';
    }
}
