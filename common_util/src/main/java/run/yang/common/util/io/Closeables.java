package run.yang.common.util.io;

import android.content.ContentProviderClient;

import java.io.Closeable;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.Nullable;
import run.yang.common.util.log.Logg;


/**
 * Created by ty on 11/11/15.
 */
public class Closeables {

    private static final String TAG = Logg.getTag(Closeables.class);

    private static final int LOG_POLICY_SILENT = 1;
    private static final int LOG_POLICY_ONE_LINE = 2;
    private static final int LOG_POLICY_STACK_TRACE = 3;

    @IntDef({LOG_POLICY_SILENT, LOG_POLICY_ONE_LINE, LOG_POLICY_STACK_TRACE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LOG_POLICY {
    }

    /**
     * throw IOException.
     *
     * @throws IOException
     */
    public static void close(@Nullable Closeable closeable) throws IOException {
        if (closeable != null) {
            closeable.close();
        }
    }

    /**
     * Log full stack trace on IOException.
     */
    public static void closeStacktrace(@Nullable Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                doLog(e, LOG_POLICY_STACK_TRACE);
            }
        }
    }

    /**
     * Log short one-line message on IOException.
     */
    public static void closeOneLiner(@Nullable Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                doLog(e, LOG_POLICY_ONE_LINE);
            }
        }
    }

    /**
     * don't log anything on IOException, just ignore and continue.
     */
    public static void closeQuietly(@Nullable Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                doLog(e, LOG_POLICY_SILENT);
            }
        }
    }

    private static void doLog(Throwable throwable, @LOG_POLICY int logPolicy) {
        switch (logPolicy) {
            case LOG_POLICY_SILENT:
                break;
            case LOG_POLICY_ONE_LINE:
                Logg.w(TAG, throwable.getMessage());
                break;
            case LOG_POLICY_STACK_TRACE:
                throwable.printStackTrace();
                break;
            default:
                break;
        }
    }

    public static void close(ContentProviderClient client) {
        if (client != null) {
            client.release();
        }
    }
}
