package run.yang.common.util.component;

import android.Manifest;
import android.content.pm.PackageManager;
import android.provider.Settings;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import run.yang.common.dependency.ContextHolder;
import run.yang.common.util.android.Version;

/**
 * permission check methods name must start with "check" or "enforce" and end with "Permission" to pass lint check.
 *
 * <a href="https://android.googlesource.com/platform/tools/base/+/master/lint/libs/lint-checks/src/main/java/com/android/tools/lint/checks/SupportAnnotationDetector.java" target="_blank">SupportAnnotationDetector.java</a>
 * <p>
 * Created by Yang Tianmei on 2015-09-08 11:07.
 */
public class PermissionTool {

    public static boolean granted(int grantResult) {
        return grantResult == PackageManager.PERMISSION_GRANTED;
    }

    @CheckResult
    public static boolean checkSelfGrantedPermission(@NonNull String permission) {
        return ContextCompat.checkSelfPermission(ContextHolder.appContext(), permission) == PackageManager.PERMISSION_GRANTED;
    }

    @CheckResult
    public static boolean checkLocationPermission() {
        return checkSelfGrantedPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                || checkSelfGrantedPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
    }

    @CheckResult
    public static boolean checkSystemAlertWindowPermission() {
        if (Version.lessThan(Version.V23_60)) {
            return true;
        }
        return Settings.canDrawOverlays(ContextHolder.appContext());
    }

    @CheckResult
    public static boolean checkReadExternalStoragePermission() {
        if (Version.atLeast(Version.V16_41)) {
            return checkSelfGrantedPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
        } else {
            return true;
        }
    }

    @CheckResult
    public static boolean checkWriteExternalStoragePermission() {
        return checkSelfGrantedPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    @CheckResult
    public static boolean checkReadContactPermission() {
        return checkSelfGrantedPermission(Manifest.permission.READ_CONTACTS);
    }
}
