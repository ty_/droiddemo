package run.yang.common.util.view;

import android.view.View;

import java.util.BitSet;

import androidx.annotation.NonNull;

/**
 * Created by Tim Yang on 2016-04-05 21:25.
 * <p/>
 * call like this, following code set horizontal(left and right) and top padding, with bottom padding unchanged.
 * <pre>
 *     PaddingHelper.with(headerView).horizontal(20).top(10).done();
 * </pre>
 */
public class PaddingHelper {
    private static final int LEFT = 0;
    private static final int TOP = 1;
    private static final int RIGHT = 2;
    private static final int BOTTOM = 3;

    private final View mView;
    private final int padding[] = new int[4];
    private final BitSet mSet = new BitSet(4);

    public static PaddingHelper with(@NonNull View view) {
        return new PaddingHelper(view);
    }

    public PaddingHelper(@NonNull View view) {
        mView = view;
    }

    public void done() {
        int left = mSet.get(LEFT) ? padding[LEFT] : mView.getPaddingLeft();
        int top = mSet.get(TOP) ? padding[TOP] : mView.getPaddingTop();
        int right = mSet.get(RIGHT) ? padding[RIGHT] : mView.getPaddingRight();
        int bottom = mSet.get(BOTTOM) ? padding[BOTTOM] : mView.getPaddingBottom();
        mView.setPadding(left, top, right, bottom);
    }

    public PaddingHelper left(int leftPadding) {
        set(LEFT, leftPadding);
        return this;
    }

    public PaddingHelper top(int topPadding) {
        set(TOP, topPadding);
        return this;
    }

    public PaddingHelper right(int rightPadding) {
        set(RIGHT, rightPadding);
        return this;
    }

    public PaddingHelper bottom(int bottomPadding) {
        set(BOTTOM, bottomPadding);
        return this;
    }

    /**
     * set left and right padding to  {@code padding}
     */
    public PaddingHelper horizontal(int padding) {
        set(LEFT, padding);
        set((RIGHT), padding);
        return this;
    }

    /**
     * set top and bottom padding to  {@code padding}
     */
    public PaddingHelper vertical(int padding) {
        set(TOP, padding);
        set(BOTTOM, padding);
        return this;
    }

    private void set(int index, int padding) {
        this.padding[index] = padding;
        mSet.set(index);
    }
}
