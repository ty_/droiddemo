package run.yang.common.util.view;

import android.app.Activity;
import android.view.View;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;

/**
 * Created by Yang Tianmei on 2016-03-21 16:10.
 */
public class V {
    public static <T extends View> T findView(@NonNull View root, @IdRes int viewId) {
        //noinspection unchecked
        return (T) root.findViewById(viewId);
    }

    public static <T extends View> T findView(@NonNull Activity root, @IdRes int viewId) {
        //noinspection unchecked
        return (T) root.findViewById(viewId);
    }
}
