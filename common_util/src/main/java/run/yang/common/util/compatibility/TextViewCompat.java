package run.yang.common.util.compatibility;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import run.yang.common.util.android.Version;

/**
 * Created by zisxks on 2016-01-25 15:48.
 */
public class TextViewCompat {

    /**
     * @param mayUseOnClick If set to false, on Android 2.3 and below, text will not be selectable.
     *                      If set to true, a new OnClickListener will be set and replace any
     *                      previously set listener.
     */
    public static void enableTextSelectable(@NonNull TextView textView, boolean mayUseOnClick) {
        if (Version.atLeast(Version.V11_30)) {
            if (!textView.isTextSelectable()) {
                textView.setTextIsSelectable(true);
            }
        } else if (mayUseOnClick) {
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CharSequence text = ((TextView) v).getText();
                    ClipboardCompat.setText(v.getContext(), text);
                }
            });
        }
    }
}
