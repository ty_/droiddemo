package run.yang.common.util.system;

import java.lang.reflect.InvocationTargetException;

import run.yang.common.util.reflect.ReflectClass;

/**
 * Created by ty on 8/31/17.
 */

public class SystemProperties {

    private static final ReflectClass sClass;

    static {
        ReflectClass clazz = null;
        try {
            clazz = ReflectClass.of(Class.forName("android.os.SystemProperties"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (clazz == null) {
            clazz = ReflectClass.of(Object.class);
        }
        sClass = clazz;
    }

    public static String get(String key) {
        try {
            return (String) sClass.getMethod("get", String.class)
                    .invoke(null, key);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String get(String key, String def) {
        try {
            return (String) sClass.getMethod("get", String.class, String.class)
                    .invoke(null, key, def);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return def;
    }

    public static int getInt(String key, int def) {
        try {
            return (int) sClass.getMethod("getInt", String.class, int.class)
                    .invoke(null, key, def);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return def;
    }

    public static long getLong(String key, long def) {
        try {
            return (long) sClass.getMethod("getLong", String.class, long.class)
                    .invoke(null, key, def);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return def;
    }

    public static boolean getBoolean(String key, boolean def) {
        try {
            return (boolean) sClass.getMethod("getBoolean", String.class, boolean.class)
                    .invoke(null, key, def);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return def;
    }
}
