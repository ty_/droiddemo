package run.yang.common.util.root;

import android.text.TextUtils;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.util.io.Closeables;
import run.yang.common.util.io.Closer;
import run.yang.common.util.io.file.Files;

/**
 * Created by Tim Yang on 2015-05-11 17:23
 * <p/>
 * 以root 权限执行命令
 * <p/>
 * See <a href="https://github.com/Trinea/android-common/blob/master/src/cn/trinea/android/common/util/ShellUtils.java>Trinea/android-common/ShellUtils.java</a>
 */

public class RootTool {
    private static final String COMMAND_LINE_END = "\n";
    private static final String COMMAND_EXIT = "exit\n";
    private static final String COMMAND_SU = "su";
    private static final String COMMAND_SH = "sh";

    public static CommandResult exec(@NonNull String command, @Nullable ExecOptions options) {
        return exec(new String[]{command}, options);
    }

    @NonNull
    public static CommandResult exec(@NonNull String[] commands, @Nullable ExecOptions options) {
        CommandResult result = new CommandResult();

        if (options == null) {
            options = ExecOptions.getDefault();
        }

        Process p = null;
        Closer closer = Closer.create();
        try {
            p = Runtime.getRuntime().exec(options.runAsRoot ? COMMAND_SU : COMMAND_SH);
            OutputStream os = p.getOutputStream();
            closer.register(os);

            for (String command : commands) {
                if (TextUtils.isEmpty(command)) {
                    continue;
                }

                if (Thread.interrupted()) {
                    throw new InterruptedException();
                }

                final String str = command + COMMAND_LINE_END;
                os.write(str.getBytes(Charset.defaultCharset()));
                os.flush();
            }
            os.write(COMMAND_EXIT.getBytes(Charset.defaultCharset()));
            os.flush();

            result.exit_code = p.waitFor();

            if (options.needOutput) {
                InputStream is = p.getInputStream();

                if (is != null) {
                    closer.register(is);
                    result.stdout = Files.readFully(is);
                } else {
                    result.stdout = null;
                }

                InputStream errorStream = p.getErrorStream();
                if (errorStream != null) {
                    closer.register(errorStream);
                    result.stderr = Files.readFully(errorStream);
                } else {
                    result.stderr = null;
                }
            }
            result.exception = null;
        } catch (Throwable e) {
            e.printStackTrace();
            result.exception = e;
        } finally {
            Closeables.closeOneLiner(closer);
            if (p != null) {
                p.destroy();
            }
        }

        return result;
    }

    public static class ExecOptions {
        public boolean runAsRoot = false;
        public boolean needOutput = true;

        public static ExecOptions getDefault() {
            return new ExecOptions();
        }
    }
}
