package run.yang.common.util.debug;

import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.StyleSpan;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.collection.CircularArray;
import androidx.core.app.NotificationCompat;
import run.yang.common.util.basic.TextTool;
import run.yang.common.util.log.Logg;

/**
 * Created by ty on 10/30/15.
 */
public class CurrentActivityNotification {

    private static final String TAG = Logg.getTag(CurrentActivityNotification.class);

    private static final int MAX_NOTIFICATION_ITEM = 5;

    private final int mNotificationId;
    private final NotificationManager mNotificationManager;
    private final NotificationCompat.Builder mBuilder;
    private final NotificationCompat.BigTextStyle mBigTextStyle;

    private final CircularArray<NotificationItemInfo> mNotificationHistory = new CircularArray<>(MAX_NOTIFICATION_ITEM);

    public CurrentActivityNotification(@NonNull Context context, @DrawableRes int notificationIconRes, int notificationId) {
        mNotificationId = notificationId;

        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setSmallIcon(notificationIconRes);
        mBuilder.setCategory(NotificationCompat.CATEGORY_STATUS);
        mBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        mBuilder.setAutoCancel(false);
        mBuilder.setPriority(NotificationCompat.PRIORITY_DEFAULT);

        mBigTextStyle = new NotificationCompat.BigTextStyle(mBuilder);
    }

    public void show(String packageName, String fullClassName) {
        Logg.d(TAG, "show " + fullClassName);

        if (packageName == null && fullClassName == null) {
            Logg.e(TAG, "package name and fullClassName are null");
            return;
        }
        if (packageName == null) {
            packageName = "";
            Logg.w(TAG, "packageName is null, set to empty string");
        }
        if (fullClassName == null) {
            fullClassName = "";
            Logg.w(TAG, "fullClassName is null, set to empty string");
        }

        addToHistory(packageName, fullClassName);

        String activityName = TextTool.lastPartExclude(fullClassName, '.');
        String packageBaseName = TextTool.removeLastPartExclude(fullClassName, '.');
        mBuilder.setContentTitle(activityName);
        mBuilder.setContentText(packageBaseName);
        mBuilder.setTicker(activityName);

        showBigTextStyle();
//        showInboxStyle();

        mNotificationManager.notify(mNotificationId, mBuilder.build());
    }

    private boolean sameAsLastClass(String packageName, String fullClassName) {
        if (!mNotificationHistory.isEmpty()) {
            NotificationItemInfo first = mNotificationHistory.getFirst();
            return TextUtils.equals(first.fullClassName, fullClassName)
                    && TextUtils.equals(first.packageName, packageName);
        }

        return false;
    }

    private static String formatNotificationElapsedTime(long diffTimeMillis) {
        StringBuilder builder = new StringBuilder();
        if (diffTimeMillis < 1000) {
            builder.append(diffTimeMillis).append("ms");
        } else if (diffTimeMillis < TimeUnit.SECONDS.toMillis(10)) {
            builder.append(String.format(Locale.US, "%.1f", diffTimeMillis / 1000F)).append("s");
        } else if (diffTimeMillis < TimeUnit.MINUTES.toMillis(1)) {
            builder.append(TimeUnit.MILLISECONDS.toSeconds(diffTimeMillis)).append("s");
        } else if (diffTimeMillis < TimeUnit.MINUTES.toMillis(10)) {
            int minute = (int) TimeUnit.MILLISECONDS.toMinutes(diffTimeMillis);
            int seconds = (int) (TimeUnit.MILLISECONDS.toSeconds(diffTimeMillis) - TimeUnit.MINUTES.toSeconds(minute));
            builder.append(minute).append("m").append(seconds).append("s");
        } else if (diffTimeMillis < TimeUnit.HOURS.toMillis(1)) {
            builder.append(TimeUnit.MILLISECONDS.toMinutes(diffTimeMillis)).append("m");
        } else {
            builder.append(">1h");
        }

        return builder.toString();
    }

    private void showBigTextStyle() {
        NotificationItemInfo first = mNotificationHistory.getFirst();
        int size = mNotificationHistory.size();
        SpannableStringBuilder builder = new SpannableStringBuilder();
        int start;
        long previousTime = System.currentTimeMillis();
        for (int i = 0; i < size; i++) {
            NotificationItemInfo info = mNotificationHistory.get(i);

            builder.append(TextTool.NON_BREAKING_SPACE);
            start = builder.length();
            builder.append(info.fullClassName);
            if (info.fullClassName.startsWith(info.packageName)) {
                int end = start + info.packageName.length();
                builder.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

            long diffTime = previousTime - info.addTime;
            String text = formatNotificationElapsedTime(diffTime);
            start = builder.length();
            builder.append(TextTool.combineToSingleLine(String.format(Locale.US, " %-6s", text)));
            builder.setSpan(new StyleSpan(Typeface.BOLD), start, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.append('\n');

            previousTime = info.addTime;
        }
        mBigTextStyle.bigText(builder);
        mBuilder.setContentTitle(TextTool.lastPartExclude(first.fullClassName, '.'));
        mBuilder.setContentText(TextTool.removeLastPartExclude(first.fullClassName, '.'));
    }

    private void addToHistory(String packageName, String fullClassName) {
        if (sameAsLastClass(packageName, fullClassName)) {
            // update first item add time
            mNotificationHistory.getFirst().addTime = System.currentTimeMillis();
            return;
        }

        NotificationItemInfo info;
        if (mNotificationHistory.size() < MAX_NOTIFICATION_ITEM) {
            info = new NotificationItemInfo();
        } else {
            info = mNotificationHistory.popLast();
        }
        info.addTime = System.currentTimeMillis();
        info.packageName = packageName;
        info.fullClassName = fullClassName;
        mNotificationHistory.addFirst(info);
    }

    private static String getOneLineClassName(String fullClassName) {
        final int MAX_LENGTH = 33;
        if (TextUtils.isEmpty(fullClassName)) {
            return fullClassName;
        }

        final int length = fullClassName.length();
        if (length <= MAX_LENGTH) {
            return fullClassName;
        }

        int start = length - MAX_LENGTH;
        int dotPosition = fullClassName.indexOf('.', start);
        if (dotPosition == -1) {
            return TextTool.lastPartInclude(fullClassName, '.');
        }

        if (dotPosition - start <= 5) {
            return fullClassName.substring(dotPosition);
        }

        return ".." + fullClassName.substring(start + 2);
    }

    private static class NotificationItemInfo {
        public String packageName;
        public String fullClassName;
        public long addTime;
    }
}
