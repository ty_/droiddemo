package run.yang.common.util.basic;

import android.text.TextUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by Yang Tianmei on 2015-08-05 20:12.
 */
public class TextTool {

    public static final String NON_BREAKING_SPACE = "\u00A0";

    /**
     * @see String#format(Locale, String, Object...)
     */
    public static String format(@NonNull String format, Object... args) {
        return String.format(Locale.ROOT, format, args);
    }

    public static boolean contains(@NonNull CharSequence[] haystack, CharSequence needle) {
        for (CharSequence s : haystack) {
            if (TextUtils.equals(s, needle)) {
                return true;
            }
        }
        return false;
    }

    /**
     * lastPartInclude("abc.txt", ".") return ".txt"
     * <p/>
     * if delimiter not found or sequence is empty, return the original sequence
     */
    public static String lastPartInclude(@Nullable String sequence, @NonNull String delimiter) {
        if (TextUtils.isEmpty(sequence)) {
            return sequence;
        }
        int index = sequence.lastIndexOf(delimiter);
        if (index != -1) {
            return sequence.substring(index);
        }
        return sequence;
    }

    /**
     * lastPartInclude("abc.txt", '.') return ".txt"
     * <p/>
     * if delimiter not found or sequence is empty, return the original sequence
     */
    public static String lastPartInclude(@Nullable String sequence, int delimiter) {
        if (TextUtils.isEmpty(sequence)) {
            return sequence;
        }
        int index = sequence.lastIndexOf(delimiter);
        if (index != -1) {
            return sequence.substring(index);
        }
        return sequence;
    }

    /**
     * lastPartExclude("abc.txt", ".") return "txt"
     * <p/>
     * if delimiter not found or sequence is empty, return the original sequence
     */
    public static String lastPartExclude(@Nullable String sequence, @NonNull String delimiter) {
        if (TextUtils.isEmpty(sequence)) {
            return sequence;
        }
        int index = sequence.lastIndexOf(delimiter);
        if (index != -1) {
            return sequence.substring(index + delimiter.length());
        }
        return sequence;
    }

    /**
     * lastPartExclude("abc.txt", '.') return "txt"
     * <p/>
     * if delimiter not found or sequence is empty, return the original sequence
     */
    public static String lastPartExclude(@Nullable String sequence, int delimiter) {
        if (TextUtils.isEmpty(sequence)) {
            return sequence;
        }
        int index = sequence.lastIndexOf(delimiter);
        if (index != -1) {
            return sequence.substring(index + 1);
        }
        return sequence;
    }

    /**
     * removeLastPartExclude("abc.def.txt", ".") return "abc.def"
     * <p/>
     * if delimiter not found or sequence is empty, return the original sequence
     */
    public static String removeLastPartExclude(@Nullable String sequence, @NonNull String delimiter) {
        if (TextUtils.isEmpty(sequence)) {
            return sequence;
        }
        int index = sequence.lastIndexOf(delimiter);
        if (index != -1) {
            return sequence.substring(0, index);
        }
        return sequence;
    }

    /**
     * removeLastPartExclude("abc.def.txt", '.') return "abc.def"
     * <p/>
     * if delimiter not found or sequence is empty, return the original sequence
     */
    public static String removeLastPartExclude(@Nullable String sequence, int delimiter) {
        if (TextUtils.isEmpty(sequence)) {
            return sequence;
        }
        int index = sequence.lastIndexOf(delimiter);
        if (index != -1) {
            return sequence.substring(0, index);
        }
        return sequence;
    }

    /**
     * removeLastPartInclude("abc.def.txt", ".") return "abc.def."
     * <p/>
     * if delimiter not found or sequence is empty, return the original sequence
     */
    public static String removeLastPartInclude(@Nullable String sequence, @NonNull String delimiter) {
        if (TextUtils.isEmpty(sequence)) {
            return sequence;
        }
        int index = sequence.lastIndexOf(delimiter);
        if (index != -1) {
            return sequence.substring(0, index + delimiter.length());
        }
        return sequence;
    }

    /**
     * removeLastPartInclude("abc.def.txt", '.') return "abc.def."
     * <p/>
     * if delimiter not found or sequence is empty, return the original sequence
     */
    public static String removeLastPartInclude(@Nullable String sequence, int delimiter) {
        if (TextUtils.isEmpty(sequence)) {
            return sequence;
        }
        int index = sequence.lastIndexOf(delimiter);
        if (index != -1) {
            return sequence.substring(0, index + 1);
        }
        return sequence;
    }

    /**
     * src: https://raw.githubusercontent.com/JakeWharton/hugo/master/hugo-runtime/src/main/java/hugo/weaving/internal/Strings.java
     */
    public static String toReadableString(Object obj) {
        if (obj == null) {
            return "null";
        }
        if (obj instanceof CharSequence) {
            return '"' + printableToString(obj.toString()) + '"';
        }

        Class<?> cls = obj.getClass();
        if (Byte.class == cls) {
            return byteToString((Byte) obj);
        }

        if (cls.isArray()) {
            return arrayToString(cls.getComponentType(), obj);
        }
        return obj.toString();
    }

    public static String toString(@Nullable Object object) {
        if (object == null) {
            return "null";
        }

        Class<?> objectClass = object.getClass();
        if (Byte.class == objectClass) {
            return byteToString((Byte) object);
        }

        if (objectClass.isArray()) {
            return arrayToString(objectClass, object);
        }

        return object.toString();
    }

    private static String printableToString(String string) {
        int length = string.length();
        StringBuilder builder = new StringBuilder(length);
        for (int i = 0; i < length; ) {
            int codePoint = string.codePointAt(i);
            switch (Character.getType(codePoint)) {
                case Character.CONTROL:
                case Character.FORMAT:
                case Character.PRIVATE_USE:
                case Character.SURROGATE:
                case Character.UNASSIGNED:
                    switch (codePoint) {
                        case '\n':
                            builder.append("\\n");
                            break;
                        case '\r':
                            builder.append("\\r");
                            break;
                        case '\t':
                            builder.append("\\t");
                            break;
                        case '\f':
                            builder.append("\\f");
                            break;
                        case '\b':
                            builder.append("\\b");
                            break;
                        default:
                            builder.append("\\u").append(String.format("%04x", codePoint).toUpperCase(Locale.US));
                            break;
                    }
                    break;
                default:
                    builder.append(Character.toChars(codePoint));
                    break;
            }
            i += Character.charCount(codePoint);
        }
        return builder.toString();
    }

    private static String arrayToString(Class<?> cls, Object obj) {
        if (byte.class == cls) {
            return byteArrayToString((byte[]) obj);
        }
        if (short.class == cls) {
            return Arrays.toString((short[]) obj);
        }
        if (char.class == cls) {
            return Arrays.toString((char[]) obj);
        }
        if (int.class == cls) {
            return Arrays.toString((int[]) obj);
        }
        if (long.class == cls) {
            return Arrays.toString((long[]) obj);
        }
        if (float.class == cls) {
            return Arrays.toString((float[]) obj);
        }
        if (double.class == cls) {
            return Arrays.toString((double[]) obj);
        }
        if (boolean.class == cls) {
            return Arrays.toString((boolean[]) obj);
        }
        return arrayToString((Object[]) obj);
    }

    /**
     * A more human-friendly version of Arrays#toString(byte[]) that uses hex representation.
     */
    private static String byteArrayToString(byte[] bytes) {
        StringBuilder builder = new StringBuilder("[");
        for (int i = 0; i < bytes.length; i++) {
            if (i > 0) {
                builder.append(", ");
            }
            builder.append(byteToString(bytes[i]));
        }
        return builder.append(']').toString();
    }

    private static String byteToString(Byte b) {
        if (b == null) {
            return "null";
        }
        return "0x" + String.format("%02x", b).toUpperCase(Locale.US);
    }

    private static String arrayToString(Object[] array) {
        StringBuilder buf = new StringBuilder();
        arrayToString(array, buf, new HashSet<Object[]>());
        return buf.toString();
    }

    private static void arrayToString(Object[] array, StringBuilder builder, Set<Object[]> seen) {
        if (array == null) {
            builder.append("null");
            return;
        }

        seen.add(array);
        builder.append('[');
        for (int i = 0; i < array.length; i++) {
            if (i > 0) {
                builder.append(", ");
            }

            Object element = array[i];
            if (element == null) {
                builder.append("null");
            } else {
                Class elementClass = element.getClass();
                if (elementClass.isArray() && elementClass.getComponentType() == Object.class) {
                    Object[] arrayElement = (Object[]) element;
                    if (seen.contains(arrayElement)) {
                        builder.append("[...]");
                    } else {
                        arrayToString(arrayElement, builder, seen);
                    }
                } else {
                    builder.append(toReadableString(element));
                }
            }
        }
        builder.append(']');
        seen.remove(array);
    }

    @NonNull
    public static <T> String join(Collection<T> collection, String delimiter) {
        if (collection == null || collection.isEmpty()) {
            return "";
        }
        final Iterator<T> iterator = collection.iterator();
        final StringBuilder sb = new StringBuilder(iterator.next().toString());
        while (iterator.hasNext()) {
            sb.append(delimiter).append(iterator.next().toString());
        }
        return sb.toString();
    }

    @NonNull
    public static <T> String join(T[] array, String delimiter) {
        if (array == null || array.length == 0) {
            return "";
        }
        final StringBuilder sb = new StringBuilder(array[0].toString());
        for (int i = 1; i < array.length; i++) {
            sb.append(delimiter).append(array[i].toString());
        }
        return sb.toString();
    }

    /**
     * replace space with non-breaking space to allow TextView to wrap on characters instead of words
     *
     * @see <a href="http://stackoverflow.com/questions/5118367/android-how-to-wrap-text-by-chars-not-by-words"
     * target="_blank">Android: How to wrap text by chars? (Not by words)</a>
     */
    public static String combineToSingleLine(@Nullable String input) {
        if (input == null) {
            return null;
        }

        return input.replaceAll(" ", NON_BREAKING_SPACE);
    }

    public static boolean matchSequentially(@Nullable String textToMatch,
                                            @NonNull String[] keywords) {
        if (TextUtils.isEmpty(textToMatch)) {
            return false;
        }

        int start = -1;
        for (String keyword : keywords) {
            if (keyword == null) {
                return false;
            }
            start = textToMatch.indexOf(keyword, start + 1);
            if (start == -1) {
                return false;
            }
        }
        return true;
    }
}
