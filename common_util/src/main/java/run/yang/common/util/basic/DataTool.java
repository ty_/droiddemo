package run.yang.common.util.basic;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;
import run.yang.common.util.constant.DataConst;

/**
 * Created by Tim Yang on 2015-04-28 15:07.
 */
public class DataTool {

    @NonNull
    public static <V> Map<String, V> getListItemMap(V value1, V value2) {
        Map<String, V> map = new ArrayMap<>(2);

        map.put(DataConst.sTagKey, value1);
        map.put(DataConst.sTagValue, value2);

        return map;
    }

    public static void sortByKey(List<Map<String, String>> textList) {
        Collections.sort(textList, new Comparator<Map<String, String>>() {
            @Override
            public int compare(@NonNull Map<String, String> lhs, @NonNull Map<String, String> rhs) {
                return lhs.get(DataConst.sTagKey).compareToIgnoreCase(rhs.get(DataConst.sTagKey));
            }
        });
    }
}
