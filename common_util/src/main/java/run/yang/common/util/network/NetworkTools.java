package run.yang.common.util.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import run.yang.common.dependency.ContextHolder;
import run.yang.common.util.log.Logg;

/**
 * Created by zisxks on 2016-06-16 15:02
 */

public class NetworkTools {

    private static final String TAG = Logg.getTag(NetworkTools.class);

    public static class NetworkType {
        /**
         * 未知网络类型，即非Wifi, 非2/3/4G, 非以太网，非蓝牙共享网络等已知类型，但仍然有网络连接
         */
        public static final int TYPE_UNKNOWN = -1;

        /**
         * 无网络连接
         */
        public static final int TYPE_NOT_CONNECTED = 1;
        public static final int TYPE_2G = 2;
        public static final int TYPE_3G = 3;
        public static final int TYPE_4G = 4;
        public static final int TYPE_5G = 5;

        /**
         * 通过蓝牙共享的网络(Bluetooth tethering, 类似Wifi热点)，例如一台Android手机共享给另一台，共享源可能是移动网络
         */
        public static final int TYPE_BLUETOOTH = 9;

        /**
         * 包括 WiFi, 以太网(ethernet）
         */
        public static final int TYPE_WIFI = 10;

        @Retention(RetentionPolicy.CLASS)
        @IntDef({
                TYPE_UNKNOWN,
                TYPE_NOT_CONNECTED,
                TYPE_2G,
                TYPE_3G,
                TYPE_4G,
                TYPE_5G,
                TYPE_BLUETOOTH,
                TYPE_WIFI
        })
        public @interface NetType {
        }

        @NetType
        private final int mNetworkType;
        private final boolean mIsMobile;

        public NetworkType(@NetType int determinedNetworkType, boolean isMobile) {
            mNetworkType = determinedNetworkType;
            mIsMobile = isMobile;
        }

        public boolean isConnected() {
            return mNetworkType != TYPE_NOT_CONNECTED;
        }

        @NetType
        public int getType() {
            return mNetworkType;
        }

        public boolean isWifi() {
            return mNetworkType == TYPE_WIFI;
        }

        public boolean isMobile() {
            return mIsMobile;
        }

        public boolean is2g() {
            return mNetworkType == TYPE_2G;
        }

        public boolean is2gOrBetter() {
            return mNetworkType >= TYPE_2G;
        }

        public boolean is3g() {
            return mNetworkType == TYPE_3G;
        }

        public boolean is3gOrBetter() {
            return mNetworkType >= TYPE_3G;
        }

        public boolean is4g() {
            return mNetworkType == TYPE_4G;
        }

        public boolean is4gOrBetter() {
            return mNetworkType >= TYPE_4G;
        }

        @Override
        public String toString() {
            return "NetworkType{" +
                    "mNetworkType=" + mNetworkType +
                    "(" + networkTypeToString(mNetworkType) + ")" +
                    ", mIsMobile=" + mIsMobile +
                    '}';
        }

        private String networkTypeToString(int tmNetworkType) {
            Field[] fields = this.getClass().getDeclaredFields();
            for (Field field : fields) {
                int modifiers = field.getModifiers();
                if ((modifiers & Modifier.STATIC) == 0) {
                    continue;
                }
                if (!field.getType().equals(int.class)) {
                    continue;
                }

                if (!field.getName().startsWith("TYPE_")) {
                    continue;
                }

                field.setAccessible(true);
                int value;
                try {
                    value = field.getInt(null);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    continue;
                }
                if (tmNetworkType == value) {
                    return field.getName();
                }
            }
            return null;
        }
    }

    /**
     * @return 网络是否已连接
     */
    public static boolean isConnected() {
        final Context context = ContextHolder.appContext();
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * @return 当前网络类型
     */
    @NonNull
    public static NetworkType getNetworkType() {
        final Context context = ContextHolder.appContext();
        final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();

        if (activeNetworkInfo == null) {
            return new NetworkType(NetworkType.TYPE_NOT_CONNECTED, false);
        }

        if (!activeNetworkInfo.isConnected()) {
            return new NetworkType(NetworkType.TYPE_NOT_CONNECTED, false);
        }

        int activeNetworkInfoType = activeNetworkInfo.getType();

        switch (activeNetworkInfoType) {
            /**
             * Wifi, ETHERNET(以太网) 都可看作不需要付费，网线很快的网络，统一归类为Wifi
             */
            case ConnectivityManager.TYPE_WIFI:
            case ConnectivityManager.TYPE_ETHERNET:
                return new NetworkType(NetworkType.TYPE_WIFI, false);

            /**
             * 移动网络，根据子类型区分2/3/4G
             */
            case ConnectivityManager.TYPE_MOBILE:
                int networkType = convertMobileNetworkType(activeNetworkInfo.getSubtype());
                return new NetworkType(networkType, true);

            /**
             *通过蓝牙共享的网络，例如一台Android手机共享给另一台，共享源可能是移动网络
             */
            case ConnectivityManager.TYPE_BLUETOOTH:
                return new NetworkType(NetworkType.TYPE_BLUETOOTH, true);

            default:
                Logg.w(TAG, "unknown network type: type: " + activeNetworkInfoType + ", subtype: " + activeNetworkInfo.getSubtype());
                return new NetworkType(NetworkType.TYPE_UNKNOWN, false);
        }
    }

    /**
     * see http://stackoverflow.com/questions/9283765/how-to-determine-if-network-type-is-2g-3g-or-4g
     *
     * @param cmNetworkType 当 {@link NetworkInfo#getType()}返回的类型是{@link ConnectivityManager#TYPE_MOBILE}时
     *                      {@link NetworkInfo#getSubtype()} 的返回值
     */
    @NetworkType.NetType
    private static int convertMobileNetworkType(int cmNetworkType) {
        switch (cmNetworkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return NetworkType.TYPE_2G;

            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                /**
                 From this link https://goo.gl/R2HOjR ..NETWORK_TYPE_EVDO_0 & NETWORK_TYPE_EVDO_A
                 EV-DO is an evolution of the CDMA2000 (IS-2000) standard that supports high data rates.

                 Where CDMA2000 https://goo.gl/1y10WI .CDMA2000 is a family of 3G[1] mobile technology standards for sending voice,
                 data, and signaling data between mobile phones and cell sites.
                 */
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                //For 3g HSDPA , HSPAP(HSPA+) are main  networktype which are under 3g Network
                //But from other constants also it will 3g like HSPA,HSDPA etc which are in 3g case.
                //Some cases are added after  testing(real) in device with 3g enable data
                //and speed also matters to decide 3g network type
                //http://goo.gl/bhtVT
                return NetworkType.TYPE_3G;

            case TelephonyManager.NETWORK_TYPE_LTE:
                //No specification for the 4g but from wiki
                //I found(LTE (Long-Term Evolution, commonly marketed as 4G LTE))
                //https://goo.gl/9t7yrR
                return NetworkType.TYPE_4G;

            default:
                return NetworkType.TYPE_UNKNOWN;
        }
    }
}
