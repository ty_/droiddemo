package run.yang.common.util.compatibility;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import run.yang.common.util.android.Version;

/**
 * Created by zisxks on 2016-01-25 16:50.
 */
public class ClipboardCompat {

    /**
     * used for Android 2.3 and below, where android.widget.TextView#setTextIsSelectable is not available, to copy TextView text to clipboard when clicked.
     */
    public static View.OnClickListener sClickToCopyTextListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ClipboardCompat.setText(v.getContext(), ((TextView) v).getText());
        }
    };

    public static void setText(@NonNull Context context, CharSequence text) {
        Object systemService = context.getSystemService(Context.CLIPBOARD_SERVICE);
        if (Version.atLeast(Version.V11_30)) {
            ClipboardManager cm = (ClipboardManager) systemService;
            cm.setPrimaryClip(ClipData.newPlainText("", text));
        } else {
            @SuppressWarnings("deprecation")
            android.text.ClipboardManager cm = (android.text.ClipboardManager) systemService;
            cm.setText(text);
        }
    }
}
