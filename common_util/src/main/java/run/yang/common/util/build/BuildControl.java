package run.yang.common.util.build;

/**
 * Created by toybox on 11/1/15.
 */
public class BuildControl {
    //TODO: refactor Build Variable
    public static final boolean isRelease = false;
    public static final boolean isDebug = true;
    public static final boolean doLog = !isRelease;

    public static final String APPLICATION_ID = "";
}
