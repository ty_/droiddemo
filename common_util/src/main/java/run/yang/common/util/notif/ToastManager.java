package run.yang.common.util.notif;

import android.widget.Toast;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import run.yang.common.dependency.ContextHolder;

/**
 * Created by Tim Yang on 2015-05-14 16:42
 * <p/>
 * Function:
 */
public class ToastManager {

    @IntDef({Toast.LENGTH_SHORT, Toast.LENGTH_LONG})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Duration {
    }

    @Nullable
    private static Toast mToast = null;

    private ToastManager() {
    }

    public static synchronized void show(@Nullable CharSequence msg) {
        show(msg, Toast.LENGTH_SHORT);
    }

    public static synchronized void show(@StringRes int msgRes) {
        show(msgRes, Toast.LENGTH_SHORT);
    }

    public static synchronized void show(@Nullable CharSequence msg, @Duration int duration) {
        if (mToast != null) {
            mToast.cancel();
        }
        // TODO: do not use application context, add context as first parameter
        mToast = Toast.makeText(ContextHolder.appContext(), msg, duration);
        mToast.show();
    }

    public static synchronized void show(@StringRes int msgRes, @Duration int duration) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(ContextHolder.appContext(), msgRes, duration);
        mToast.show();
    }
}
