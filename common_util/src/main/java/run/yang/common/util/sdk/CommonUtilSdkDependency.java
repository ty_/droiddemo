package run.yang.common.util.sdk;


import run.yang.common.util.log.LogInterface;

/**
 * 创建时间: 2016/08/11 17:36 <br>
 * 作者: zisxks <br>
 * 描述:
 */
public interface CommonUtilSdkDependency {
    /**
     * Debug 版 SDK会做错误检查，发现错误抛运行时异常，以便尽早发现问题
     */
    boolean isDebug();

    /**
     * 打印日志级别，大于等于此级别的日志才会打印出来
     */
    @LogInterface.LOG_LEVEL
    int getLogLevel();

    /**
     * 写入文件的日志级别，大于等于此级别的日志将会写入Log文件。低于 {@link #getLogLevel()}的级别无效
     */
    @LogInterface.LOG_LEVEL
    int getFileLogLevel();
}
