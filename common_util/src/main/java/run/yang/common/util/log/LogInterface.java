package run.yang.common.util.log;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * 创建时间: 2016/08/11 15:07 <br>
 * 作者: zisxks <br>
 * 描述: 打Log的接口，SDK通过此接口打Log，外部定义具体实现
 */
public interface LogInterface {
    int LOG_LEVEL_ERROR = 16;
    int LOG_LEVEL_WARN = 8;
    int LOG_LEVEL_INFO = 4;
    int LOG_LEVEL_DEBUG = 2;
    int LOG_LEVEL_VERBOSE = 0;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({
            LOG_LEVEL_ERROR, LOG_LEVEL_WARN, LOG_LEVEL_INFO, LOG_LEVEL_DEBUG, LOG_LEVEL_VERBOSE,
    })
    @interface LOG_LEVEL {
    }

    void e(@NonNull String tag, @Nullable String msg);

    void e(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable);

    void w(@NonNull String tag, @Nullable String msg);

    void w(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable);

    void i(@NonNull String tag, @Nullable String msg);

    void i(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable);

    void d(@NonNull String tag, @Nullable String msg);

    void d(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable);

    void v(@NonNull String tag, @Nullable String msg);

    void v(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable);
}
