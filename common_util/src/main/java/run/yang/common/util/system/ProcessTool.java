package run.yang.common.util.system;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Process;
import android.text.TextUtils;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.util.io.file.Files;


/**
 * Created by Tim Yang on 2016-03-29 17:23.
 */
public class ProcessTool {

    /**
     * @return something like com.zisxks.droiddemo:ui
     */
    public static String currentProcessName(@NonNull Context context) {
        String name = currentProcessNameFromActivityManager(context);

        if (TextUtils.isEmpty(name)) {
            name = currentProcessNameFromProcFs();
        }

        return name;
    }

    @Nullable
    private static String currentProcessNameFromActivityManager(@NonNull Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (am == null) {
            return null;
        }
        int myPid = Process.myPid();
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = am.getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (ActivityManager.RunningAppProcessInfo runningAppProcess : runningAppProcesses) {
                if (myPid == runningAppProcess.pid) {
                    return runningAppProcess.processName;
                }
            }
        }
        return null;
    }

    private static String currentProcessNameFromProcFs() {
        String processName = Files.readFully("/proc/self/cmdline");
        processName = processName.trim();
        return processName;
    }
}
