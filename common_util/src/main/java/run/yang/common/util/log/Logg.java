package run.yang.common.util.log;

import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * 对象提供的打Log接口，提供与系统的 {Log} 相同的接口。取名为Logg是为了方便在代码自动补全中看到这个类，
 * 以便不知道的使用者发现这个类。这个名字也比较简短
 * <p>
 * 本log库会处理多进程情况，App选择一个进程作为真正打log的进程（通常选存活最久的进程，如后台进程），
 * 并调用 initLogProcess 进行初始化。除此以外的其他进程调用 initOtherProcess 进行初始化
 */
public class Logg {

    public static String getTag(@NonNull Class<?> clazz) {
        return clazz.getSimpleName();
    }

    private static LogInterface sLogInstance;

    /**
     * 在应用存活时间最长的进程里调用。其他进程的log会通过AIDL传递到本进程打印。只有一个进程调用本方法。
     * 其他进程调用 initOtherProcess
     */
    public static void initLogProcess(@Nullable LogInterface logInstance) {
        if (logInstance == null) {
            sLogInstance = new DefaultLogImpl();
        } else {
            sLogInstance = logInstance;
        }
    }

    /**
     * 除了调用 initLogProcess 的其他进程必须调用此方法进行初始化
     */
    public static void initOtherProcess() {
        if (sLogInstance != null) {
            ((LogClient) sLogInstance).unbindLogService();
        }
        LogClient logClient = new LogClient();
        logClient.bindLogService();
        sLogInstance = logClient;
    }

    private Logg() {
    }

    public static void d(String tag, String format, Object... args) {
        sLogInstance.d(tag, String.format(Locale.US, format, args));
    }

    public static void d(String tag, String msg) {
        sLogInstance.d(tag, msg);
    }

    public static void d(String tag, String msg, Throwable error) {
        sLogInstance.d(tag, msg, error);
    }

    public static void v(String tag, String msg) {
        sLogInstance.v(tag, msg);
    }

    public static void v(String tag, String msg, Throwable error) {
        sLogInstance.v(tag, msg, error);
    }

    public static void i(String tag, String msg) {
        sLogInstance.i(tag, msg);
    }

    public static void i(String tag, String msg, Throwable error) {
        sLogInstance.i(tag, msg, error);
    }

    public static void w(String tag, String msg) {
        sLogInstance.w(tag, msg);
    }

    public static void w(String tag, String msg, Throwable error) {
        sLogInstance.w(tag, msg, error);
    }

    public static void e(String tag, String msg) {
        sLogInstance.e(tag, msg);
    }

    public static void e(String tag, String format, Object... args) {
        sLogInstance.e(tag, String.format(Locale.US, format, args));
    }

    public static void e(String tag, String msg, Throwable error) {
        sLogInstance.e(tag, msg, error);
    }

    public static String getLogWithException(String msg, Throwable throwable) {
        return msg + (throwable != null ? ": " + throwable.getMessage() : "");
    }
}
