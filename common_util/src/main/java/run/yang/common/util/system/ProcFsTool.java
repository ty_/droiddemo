package run.yang.common.util.system;

import android.text.TextUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.Nullable;
import run.yang.common.util.basic.NumberTool;
import run.yang.common.util.compatibility.CharsetCompat;
import run.yang.common.util.io.Closeables;
import run.yang.common.util.io.file.Files;
import run.yang.common.util.log.Logg;
import run.yang.common.util.notif.ToastManager;

/**
 * /proc file system util
 *
 * @see <a href="http://man7.org/linux/man-pages/man5/proc.5.html" target="_blank">/proc file system manual page</a>
 * <p/>
 * Created by Yang Tianmei on 2015-10-12.
 */
public class ProcFsTool {

    private static final String TAG = Logg.getTag(ProcFsTool.class);

    @Nullable
    public static File[] getRunningProcessList() {
        File procFile = new File("/proc");
        if (!procFile.canRead()) {
            Logg.d(TAG, "/proc is not readable");
            ToastManager.show("/proc is not readable");
            return null;
        }

        return procFile.listFiles(new FilenameFilter() {
            final Pattern mPidPattern = Pattern.compile("\\d+");

            @Override
            public boolean accept(File dir, String filename) {
                return mPidPattern.matcher(filename).matches();
            }
        });
    }

    /**
     * uid or -1 if error occurs
     */
    public static int getUid(String pathPrefix) {
        final int defaultErrorUid = -1;

        if (TextUtils.isEmpty(pathPrefix)) {
            return defaultErrorUid;
        }
        File file = new File(pathPrefix, "status");
        if (!file.canRead()) {
            return defaultErrorUid;
        }

        InputStreamReader isReader = null;
        try {
            isReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(isReader);
            String line;
            while ((line = reader.readLine()) != null) {
                if (!line.contains("Uid")) {
                    continue;
                }
                final Pattern integerPattern = Pattern.compile("\\d+");
                final Matcher matcher = integerPattern.matcher(line);
                int matchCount = 0;
                while (matcher.find()) {
                    matchCount++;
                }
                if (matchCount != 4) {
                    continue;
                }
                if (matcher.find(0)) {
                    final String uid = matcher.group();
                    return NumberTool.parseInt(uid);
                }
                break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            Closeables.closeQuietly(isReader);
        }

        return defaultErrorUid;
    }

    public static String getCommandLine(@Nullable String pathPrefix) {
        if (pathPrefix == null) {
            return null;
        }
        try {
            return Files.readFirstLine(new File(pathPrefix, "cmdline"), CharsetCompat.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
