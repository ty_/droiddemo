package run.yang.common.util.safehandler;

import android.os.Message;

/**
 * Created by Tim Yang on 2015-05-07 16:32.
 */
public interface HandlerCallback {
    void handleMessage(Message msg);
}

