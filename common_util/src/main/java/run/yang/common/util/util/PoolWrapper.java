package run.yang.common.util.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pools;

/**
 * Created by Yang Tianmei on 2015-09-22.
 */
public abstract class PoolWrapper<T> {
    private final Pools.Pool<T> mPool;

    public PoolWrapper(Pools.Pool<T> pool) {
        mPool = pool;
    }

    protected abstract T newItem();

    @NonNull
    public T obtain() {
        final T item = mPool.acquire();
        return item != null ? item : newItem();
    }

    public void recycle(@Nullable T item) {
        if (item != null) {
            mPool.release(item);
        }
    }
}
