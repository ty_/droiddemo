package run.yang.common.util.sdk;

import android.content.Context;

import androidx.annotation.NonNull;
import run.yang.common.dependency.ContextHolder;
import run.yang.common.util.java.Objects;
import run.yang.common.util.log.Logg;

/**
 * Created by zisxks on 2016-06-08 15:58
 * <p/>
 * 初始化Common Util SDK
 */
public class CommonUtilSdk {

    private static CommonUtilSdkDependency sUtilSdkDependency;

    /**
     * @param isLogProccess 是否在本进程写log文件，只有一个进程可以传true，详见 {@link Logg}
     * @see Logg
     */
    public static void init(@NonNull Context context, @NonNull CommonUtilSdkDependency dependency,
                            boolean isLogProccess) {
        Objects.requireNonNull(dependency, "dependency must not be null");
        sUtilSdkDependency = dependency;

        ContextHolder.init(context);

        if (isLogProccess) {
            Logg.initLogProcess(null);
        } else {
            Logg.initOtherProcess();
        }
    }

    /**
     * 使用前必须先调 init 做初始化
     */
    @NonNull
    public static CommonUtilSdkDependency getDependency() {
        return sUtilSdkDependency;
    }
}
