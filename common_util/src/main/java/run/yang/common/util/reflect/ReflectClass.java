package run.yang.common.util.reflect;

import java.lang.reflect.Method;

import androidx.annotation.NonNull;

/**
 * Created by ty on 8/31/17.
 */

public class ReflectClass {
    @NonNull
    private final Class<?> mClass;

    private ReflectClass(@NonNull Class<?> aClass) {
        mClass = aClass;
    }

    public static ReflectClass of(@NonNull Class<?> aClass) {
        return new ReflectClass(aClass);
    }

    public ReflectMethod getMethod(String name, Class<?>... parameterTypes) throws NoSuchMethodException {
        Method declaredMethod = mClass.getDeclaredMethod(name, parameterTypes);
        declaredMethod.setAccessible(true);
        return ReflectMethod.of(declaredMethod);
    }
}
