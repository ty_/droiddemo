package run.yang.common.util.util;

import android.text.SpannableStringBuilder;

/**
 * 创建时间: 2017/06/01 17:51 <br>
 * 作者: Yang Tianmei <br>
 * 描述: Null安全的 SpannableStringBuilder，系统的版本如果CharSequence 是 null，会报空指针异常
 */

public class SafeSpannableStringBuilder extends SpannableStringBuilder {

    public SafeSpannableStringBuilder() {
        super();
    }

    public SafeSpannableStringBuilder(CharSequence text) {
        super(text);
    }

    public SafeSpannableStringBuilder(CharSequence text, int start, int end) {
        super(text, start, end);
    }

    @Override
    public SpannableStringBuilder append(CharSequence text) {
        return text != null ? super.append(text) : this;
    }

    @Override
    public SpannableStringBuilder append(CharSequence text, Object what, int flags) {
        return text != null ? super.append(text, what, flags) : this;
    }

    @Override
    public SpannableStringBuilder append(CharSequence text, int start, int end) {
        return text != null ? super.append(text, start, end) : this;
    }

    @Override
    public SpannableStringBuilder insert(int where, CharSequence tb, int start, int end) {
        return tb != null ? super.insert(where, tb, start, end) : this;
    }

    @Override
    public SpannableStringBuilder insert(int where, CharSequence tb) {
        return tb != null ? super.insert(where, tb) : this;
    }

    @Override
    public SpannableStringBuilder replace(int start, int end, CharSequence tb) {
        return tb != null ? super.replace(start, end, tb) : this;
    }

    @Override
    public SpannableStringBuilder replace(int start, int end, CharSequence tb, int tbstart, int tbend) {
        return tb != null ? super.replace(start, end, tb, tbstart, tbend) : this;
    }
}
