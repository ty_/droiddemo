package run.yang.common.util.basic;

import android.text.format.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;

/**
 * Created by Yang Tianmei on 2015-07-22 09:39.
 */
public class TimeTool {
    /**
     * ISO 8601 international standard date format
     */
    private static final ThreadLocal<SimpleDateFormat> HUMAN_READABLE_TIME_FMT = new ThreadLocal<SimpleDateFormat>() {
        @NonNull
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        }
    };

    @NonNull
    public static String formatFormalTime(@IntRange(from = 0) long milliseconds) {
        return formatFormalTime(new Date(milliseconds));
    }

    @NonNull
    public static String formatFormalTime(@NonNull Date date) {
        return HUMAN_READABLE_TIME_FMT.get().format(date);
    }

    @NonNull
    public static String formatFormalTime(@NonNull Calendar calendar) {
        return formatFormalTime(calendar.getTime());
    }

    @NonNull
    public static String formatElapsedTime(@IntRange(from = 0) long timeMilliseconds) {
        StringBuilder sb = new StringBuilder();

        DateUtils.formatElapsedTime(sb, timeMilliseconds / 1000);
        long millisecondPart = timeMilliseconds % 1000;
        sb.append(":").append(String.format(Locale.US, "%03d", millisecondPart));

        return sb.toString();
    }
}
