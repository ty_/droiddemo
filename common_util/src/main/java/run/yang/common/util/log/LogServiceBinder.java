package run.yang.common.util.log;

import android.os.RemoteException;

import run.yang.common.util.log.bean.ParcelableThrowable;

public class LogServiceBinder extends ILogService.Stub {
    private static final String TAG = "LogServiceBinder";

    @Override
    public void log(int logLevel, String tag, String message, ParcelableThrowable ex) throws RemoteException {
        int callingPid = getCallingPid();
        tag = "pid: " + callingPid + ": " + tag;

        switch (logLevel) {
            case LogInterface.LOG_LEVEL_VERBOSE:
                if (ex == null) {
                    Logg.v(tag, message);
                } else {
                    Logg.v(tag, message, ex.toThrowable());
                }
                break;

            case LogInterface.LOG_LEVEL_DEBUG:
                if (ex == null) {
                    Logg.d(tag, message);
                } else {
                    Logg.d(tag, message, ex.toThrowable());
                }
                break;

            case LogInterface.LOG_LEVEL_WARN:
                if (ex == null) {
                    Logg.w(tag, message);
                } else {
                    Logg.w(tag, message, ex.toThrowable());
                }
                break;

            case LogInterface.LOG_LEVEL_INFO:
                if (ex == null) {
                    Logg.i(tag, message);
                } else {
                    Logg.i(tag, message, ex.toThrowable());
                }
                break;

            case LogInterface.LOG_LEVEL_ERROR:
                if (ex == null) {
                    Logg.e(tag, message);
                } else {
                    Logg.e(tag, message, ex.toThrowable());
                }
                break;

            default:
                Logg.e(TAG, "unknown log level = " + logLevel);
                if (ex == null) {
                    Logg.w(tag, message);
                } else {
                    Logg.w(tag, message, ex.toThrowable());
                }
                break;
        }
    }
}
