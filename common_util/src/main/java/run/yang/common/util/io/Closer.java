package run.yang.common.util.io;

/**
 * Created by Tim Yang on 2016-04-06 11:08.
 * <p>
 * from Guava 18.0
 */
/*
 * Copyright (C) 2012 The Guava Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.annotation.TargetApi;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;

import androidx.annotation.NonNull;
import run.yang.common.util.android.Version;
import run.yang.common.util.log.Logg;


/**
 * A {@link Closeable} that collects {@code Closeable} resources and closes them all when it is
 * {@linkplain #close closed}. This is intended to approximately emulate the behavior of Java 7's
 * <a href="http://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html">
 * try-with-resources</a> statement in JDK6-compatible code. Running on Java 7, code using this
 * should be approximately equivalent in behavior to the same code written with try-with-resources.
 * Running on Java 6, exceptions that cannot be thrown must be logged rather than being added to the
 * thrown exception as a suppressed exception.
 *
 * <p>This class is intended to be used in the following pattern:
 *
 * <pre>   {@code
 *   Closer closer = Closer.create();
 *   try {
 *     InputStream in = closer.register(openInputStream());
 *     OutputStream out = closer.register(openOutputStream());
 *     // do stuff
 *   } catch (Throwable e) {
 *     // ensure that any checked exception types other than IOException that could be thrown are
 *     // provided here, e.g. throw closer.rethrow(e, CheckedException.class);
 *     // REMOVED in this port, Guava has it.
 *     // throw closer.rethrow(e);
 *   } finally {
 *     closer.close();
 *   }}</pre>
 *
 * <p>Note that this try-catch-finally block is not equivalent to a try-catch-finally block using
 * try-with-resources. To get the equivalent of that, you must wrap the above code in <i>another</i>
 * try block in order to catch any exception that may be thrown (including from the call to
 * {@code close()}).
 *
 * <p>This pattern ensures the following:
 *
 * <ul>
 * <li>Each {@code Closeable} resource that is successfully registered will be closed later.</li>
 * <li><s>If a {@code Throwable} is thrown in the try block, no exceptions that occur when attempting
 * to close resources will be thrown from the finally block. The throwable from the try block will
 * be thrown.</s></li>
 * <li><s>If no exceptions or errors were thrown in the try block, the <i>first</i> exception thrown
 * by an attempt to close a resource will be thrown.</s></li>
 * <li>Any exception caught when attempting to close a resource that is <i>not</i> thrown
 * (because another exception is already being thrown) is <i>suppressed</i>.</li>
 * </ul>
 *
 * <p>An exception that is suppressed is not thrown. The method of suppression used depends on the
 * version of Java the code is running on:
 *
 * <ul>
 * <li><b>Java 7+(Android API Level 19+):</b> Exceptions are suppressed by adding them to the exception that <i>will</i>
 * be thrown using {@code Throwable.addSuppressed(Throwable)}.</li>
 * <li><b>Java 6:</b> Exceptions are suppressed by logging them instead.</li>
 * </ul>
 *
 * @author Colin Decker
 * @since 14.0
 */
// Coffee's for {@link Closer closers} only.
//@Beta
public final class Closer implements Closeable {

    /**
     * The suppressor implementation to use for the current Java version.
     */
    private static final Suppressor SUPPRESSOR = SuppressingSuppressor.isAvailable()
            ? SuppressingSuppressor.INSTANCE
            : LoggingSuppressor.INSTANCE;

    /**
     * Creates a new {@link Closer}.
     */
    public static Closer create() {
        return new Closer(SUPPRESSOR);
    }

    final Suppressor suppressor;

    // only need space for 2 elements in most cases, so try to use the smallest array possible
    private final Deque<Closeable> stack = new ArrayDeque<>(4);

    Closer(Suppressor suppressor) {
        this.suppressor = suppressor;
    }

    /**
     * Registers the given {@code closeable} to be closed when this {@code Closer} is
     * {@linkplain #close closed}.
     *
     * @return the given {@code closeable}
     */
    // close. this word no longer has any meaning to me.
    public <C extends Closeable> C register(@NonNull C closeable) {
        if (closeable != null) {
            stack.addFirst(closeable);
        }

        return closeable;
    }

    /**
     * Closes all {@code Closeable} instances that have been added to this {@code Closer}. If an
     * exception was thrown in the try block and passed to one of the {@code exceptionThrown} methods,
     * any exceptions thrown when attempting to close a closeable will be suppressed.
     */
    @Override
    public void close() throws IOException {
        Throwable throwable = null;

        // close closeables in LIFO order
        while (!stack.isEmpty()) {
            Closeable closeable = stack.removeFirst();
            try {
                closeable.close();
            } catch (Throwable e) {
                if (throwable == null) {
                    throwable = e;
                } else {
                    suppressor.suppress(closeable, throwable, e);
                }
            }
        }
    }

    /**
     * Suppression strategy interface.
     */
    interface Suppressor {
        /**
         * Suppresses the given exception ({@code suppressed}) which was thrown when attempting to close
         * the given closeable. {@code thrown} is the exception that is actually being thrown from the
         * method. Implementations of this method should not throw under any circumstances.
         */
        void suppress(Closeable closeable, Throwable thrown, Throwable suppressed);
    }

    /**
     * Suppresses exceptions by logging them.
     */
    static final class LoggingSuppressor implements Suppressor {

        static final LoggingSuppressor INSTANCE = new LoggingSuppressor();
        private static final String TAG = Logg.getTag(LoggingSuppressor.class);

        @Override
        public void suppress(Closeable closeable, Throwable thrown, Throwable suppressed) {
            Logg.w(TAG, "Suppressing exception thrown when closing " + closeable, suppressed);
        }
    }

    /**
     * Suppresses exceptions by adding them to the exception that will be thrown using JDK7's
     * addSuppressed(Throwable) mechanism.
     */
    static final class SuppressingSuppressor implements Suppressor {

        static final SuppressingSuppressor INSTANCE = new SuppressingSuppressor();

        static boolean isAvailable() {
            return Version.atLeast(Version.V19_44);
        }

        @TargetApi(Version.V19_44)
        @Override
        public void suppress(Closeable closeable, Throwable thrown, Throwable suppressed) {
            // ensure no exceptions from addSuppressed
            if (thrown == suppressed) {
                return;
            }
            try {
                thrown.addSuppressed(suppressed);
            } catch (Throwable e) {
                // if, somehow, IllegalAccessException or another exception is thrown, fall back to logging
                LoggingSuppressor.INSTANCE.suppress(closeable, thrown, suppressed);
            }
        }
    }
}
