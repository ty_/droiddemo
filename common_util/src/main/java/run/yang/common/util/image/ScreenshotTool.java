package run.yang.common.util.image;

import android.graphics.Bitmap;
import android.view.View;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.util.io.Closeables;

/**
 * Created by zisxks on 2017-06-30 20:15.
 */
public class ScreenshotTool {

    /**
     * save view as bitmap, result bitmap is mutable
     */
    @Nullable
    public static Bitmap takeScreenshot(@Nullable View view) {
        if (view == null) {
            return null;
        }
        boolean drawingCacheEnabled = view.isDrawingCacheEnabled();
        if (!drawingCacheEnabled) {
            view.buildDrawingCache();
        }
        Bitmap bitmap = view.getDrawingCache();
        // drawingCacheBitmap will be invalid after destroyDrawingCache, make a copy
        if (bitmap != null) {
            bitmap = bitmap.copy(bitmap.getConfig(), true);
        }
        if (!drawingCacheEnabled) {
            view.destroyDrawingCache();
        }
        return bitmap;
    }

    /**
     * save Bitmap as .png image file
     *
     * @throws FileNotFoundException
     */
    @NonNull
    public static File saveBitmapAsPng(@NonNull Bitmap bitmap, @NonNull File outputFile)
            throws FileNotFoundException {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(outputFile);
            final int ignored = 100;
            bitmap.compress(Bitmap.CompressFormat.PNG, ignored, fos);
            return outputFile;
        } finally {
            Closeables.closeOneLiner(fos);
        }
    }
}
