package run.yang.common.util.dynamicproxy;

import androidx.annotation.Nullable;

/**
 * Created by zisxks on 2016-06-17 17:18
 */

public interface ProxyInstanceManagerInterface<Interface> {
    /**
     * @return previous instance
     */
    Interface setCallback(@Nullable Interface instance);

    @Nullable
    Interface getCallback();
}
