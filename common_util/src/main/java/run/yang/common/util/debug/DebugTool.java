package run.yang.common.util.debug;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.DrawableRes;
import run.yang.common.util.android.Version;
import run.yang.common.util.build.BuildControl;
import run.yang.common.util.notif.ToastManager;

/**
 * Created by Yang Tianmei on 2015-09-10 11:09.
 */
public class DebugTool {

    private static final boolean IS_DEBUG = BuildControl.isDebug;

    /**
     * Show current activity name in status bar and Toast
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static void registerActivityLifecycleCallbacks(
            final Application application, final int notificationId, @DrawableRes final int iconRes) {
        if (!IS_DEBUG || Version.lessThan(Version.V14_40)) {
            return;
        }
        final Application.ActivityLifecycleCallbacks callback = new Application.ActivityLifecycleCallbacks() {

            private CurrentActivityNotification mNotification = new CurrentActivityNotification(
                    application, iconRes, notificationId);

            @Override
            public void onActivityCreated(final Activity activity, final Bundle savedInstanceState) {
            }

            @Override
            public void onActivityStarted(final Activity activity) {
            }

            @Override
            public void onActivityResumed(final Activity activity) {
                final String className = activity.getClass().getName();

                mNotification.show(activity.getPackageName(), className);
                ToastManager.show(className);
            }

            @Override
            public void onActivityPaused(final Activity activity) {
            }

            @Override
            public void onActivityStopped(final Activity activity) {
            }

            @Override
            public void onActivitySaveInstanceState(final Activity activity, final Bundle outState) {
            }

            @Override
            public void onActivityDestroyed(final Activity activity) {
            }
        };

        application.registerActivityLifecycleCallbacks(callback);
    }
}
