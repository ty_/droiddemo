package run.yang.common.util.benchmark;

import android.os.SystemClock;

import java.util.Locale;

import run.yang.common.util.log.Logg;

/**
 * Created by zisxks on 2016-06-16 14:40
 */

public class Timing {

    private static final String TAG = Timing.class.getSimpleName();

    private long mStartTime;
    private long mTime;

    private final String mTag;

    public Timing(String tag) {
        mTag = tag;
        start();
    }

    public void log(String action) {
        String message = formatLogMessage(action, updateTime());
        Logg.d(TAG, message);
    }

    public void logAndDone(String action) {
        log(action);
        done();
    }

    public void start() {
        mTime = currentTime();
        mStartTime = mTime;
    }

    public void done() {
        String message = totalTimeMessage(currentTime() - mStartTime);
        Logg.d(TAG, message);
    }

    private long updateTime() {
        long previous = mTime;
        mTime = currentTime();
        return mTime - previous;
    }

    private String formatLogMessage(String action, long time) {
        return String.format(Locale.US, "%s: time: %,d ms <- action: %s", mTag, time, action);
    }

    private String totalTimeMessage(long totalTime) {
        return String.format(Locale.US, "%s: total time: %,d ms", mTag, totalTime);
    }

    private long currentTime() {
        return SystemClock.elapsedRealtime();
    }
}
