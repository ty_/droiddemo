package run.yang.common.util.log.bean;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class ParcelableStackTraceElement implements Parcelable {
    private final String mClassName;
    private final String mMethodName;
    private final String mFileName;
    private final int mLineNumber;

    public ParcelableStackTraceElement(@NonNull StackTraceElement element) {
        mClassName = element.getClassName();
        mMethodName = element.getMethodName();
        mFileName = element.getFileName();
        mLineNumber = element.getLineNumber();
    }

    protected ParcelableStackTraceElement(Parcel in) {
        mClassName = in.readString();
        mMethodName = in.readString();
        mFileName = in.readString();
        mLineNumber = in.readInt();
    }

    public static final Creator<ParcelableStackTraceElement> CREATOR = new Creator<ParcelableStackTraceElement>() {
        @Override
        public ParcelableStackTraceElement createFromParcel(Parcel in) {
            return new ParcelableStackTraceElement(in);
        }

        @Override
        public ParcelableStackTraceElement[] newArray(int size) {
            return new ParcelableStackTraceElement[size];
        }
    };

    public String getClassName() {
        return mClassName;
    }

    public String getMethodName() {
        return mMethodName;
    }

    public String getFileName() {
        return mFileName;
    }

    public int getLineNumber() {
        return mLineNumber;
    }

    public StackTraceElement toStackTraceElement() {
        return new StackTraceElement(mClassName, mMethodName, mFileName, mLineNumber);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mClassName);
        parcel.writeString(mMethodName);
        parcel.writeString(mFileName);
        parcel.writeInt(mLineNumber);
    }
}
