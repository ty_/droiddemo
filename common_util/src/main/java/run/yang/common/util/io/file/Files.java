package run.yang.common.util.io.file;

import android.text.TextUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.util.io.Closeables;
import run.yang.common.util.io.Closer;

/**
 * Created by zisxks on 2016-01-08 17:46.
 * <p>
 * See Oracle JDK 1.8 implementation and Guava.
 */
public class Files {

    /**
     * The maximum size of array to allocate.
     * Some VMs reserve some header words in an array.
     * Attempts to allocate larger arrays may result in
     * OutOfMemoryError: Requested array size exceeds VM limit
     */
    private static final int MAX_BUFFER_SIZE = Integer.MAX_VALUE - 8;

    // buffer size used for reading and writing
    private static final int BUFFER_SIZE = 8192;

    /**
     * @param path the file to read from.
     * @return file content or "" if error occurred.
     */
    public static String readFully(String path) {
        try {
            byte[] bytes = readAllBytes(path);
            return bytesToString(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * @param file the file to read from.
     * @return file content or "" if error occurred.
     */
    public static String readFully(File file) {
        try {
            byte[] bytes = readAllBytes(file);
            return bytesToString(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * @param source the source stream to read from.
     * @return file content or "" if error occurred.
     */
    public static String readFully(InputStream source) {
        try {
            byte[] bytes = readAllBytes(source);
            return bytesToString(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @NonNull
    private static String bytesToString(byte[] bytes) throws UnsupportedEncodingException {
        if (bytes != null) {
            return new String(bytes, "UTF-8");
        }
        return "";
    }

    /**
     * Reads all the bytes from a file. The method ensures that the file is
     * closed when all bytes have been read or an I/O error, or other runtime
     * exception, is thrown.
     *
     * <p> Note that this method is intended for simple cases where it is
     * convenient to read all bytes into a byte array. It is not intended for
     * reading in large files.
     *
     * @param path the path to the file
     * @return a byte array containing the bytes read from the file
     * @throws IOException       if an I/O error occurs reading from the stream
     * @throws OutOfMemoryError  if an array of the required size cannot be allocated, for
     *                           example the file is larger that {@code 2GB}
     * @throws SecurityException In the case of the default provider, and a security manager is
     *                           installed, the {@link SecurityManager#checkRead(String) checkRead}
     *                           method is invoked to check read access to the file.
     */
    public static byte[] readAllBytes(String path) throws IOException {
        if (TextUtils.isEmpty(path)) {
            return null;
        }

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(path);
            return readAllBytes(fis);
        } finally {
            Closeables.closeQuietly(fis);
        }
    }

    /**
     * Reads all the bytes from a file. The method ensures that the file is
     * closed when all bytes have been read or an I/O error, or other runtime
     * exception, is thrown.
     *
     * <p> Note that this method is intended for simple cases where it is
     * convenient to read all bytes into a byte array. It is not intended for
     * reading in large files.
     *
     * @param path the path to the file
     * @return a byte array containing the bytes read from the file
     * @throws IOException       if an I/O error occurs reading from the stream
     * @throws OutOfMemoryError  if an array of the required size cannot be allocated, for
     *                           example the file is larger that {@code 2GB}
     * @throws SecurityException In the case of the default provider, and a security manager is
     *                           installed, the {@link SecurityManager#checkRead(String) checkRead}
     *                           method is invoked to check read access to the file.
     */
    public static byte[] readAllBytes(File path) throws IOException {
        if (path == null) {
            return null;
        }

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(path);
            return readAllBytes(fis);
        } finally {
            Closeables.closeQuietly(fis);
        }
    }

    /**
     * Reads all the bytes from an input stream. Uses {@code  source.available } as a hint
     * about how many bytes the stream will have. If source.available is not positive number,
     * use {@code BUFFER_SIZE} instead.
     *
     * @param source the input stream to read from
     * @return a byte array containing the bytes read from the file
     * @throws IOException      if an I/O error occurs reading from the stream
     * @throws OutOfMemoryError if an array of the required size cannot be allocated
     */
    public static byte[] readAllBytes(@Nullable InputStream source) throws IOException {
        if (source == null) {
            return null;
        }
        int available = source.available();
        if (available <= 0) {
            available = BUFFER_SIZE;
        }
        return read(source, available);
    }


    /**
     * Reads all the bytes from an input stream. Uses {@code initialSize} as a hint
     * about how many bytes the stream will have.
     *
     * @param source      the input stream to read from
     * @param initialSize the initial size of the byte array to allocate
     * @return a byte array containing the bytes read from the file
     * @throws IOException      if an I/O error occurs reading from the stream
     * @throws OutOfMemoryError if an array of the required size cannot be allocated
     */
    private static byte[] read(InputStream source, int initialSize) throws IOException {
        int capacity = initialSize;
        byte[] buf = new byte[capacity];
        int nread = 0;
        int n;
        for (; ; ) {
            // read to EOF which may read more or less than initialSize (eg: file
            // is truncated while we are reading)
            while ((n = source.read(buf, nread, capacity - nread)) > 0)
                nread += n;

            // if last call to source.read() returned -1, we are done
            // otherwise, try to read one more byte; if that failed we're done too
            if (n < 0 || (n = source.read()) < 0)
                break;

            // one more byte was read; need to allocate a larger buffer
            if (capacity <= MAX_BUFFER_SIZE - capacity) {
                capacity = Math.max(capacity << 1, BUFFER_SIZE);
            } else {
                if (capacity == MAX_BUFFER_SIZE)
                    throw new OutOfMemoryError("Required array size too large");
                capacity = MAX_BUFFER_SIZE;
            }
            buf = Arrays.copyOf(buf, capacity);
            buf[nread++] = (byte) n;
        }
        return (capacity == nread) ? buf : Arrays.copyOf(buf, nread);
    }

    /**
     * Returns a buffered reader that reads from a file using default
     * character set.
     *
     * @param file the file to read from
     * @return the buffered reader
     */
    public static BufferedReader newReader(@NonNull File file) throws FileNotFoundException {
        return newReader(file, Charset.defaultCharset());
    }

    /**
     * Returns a buffered reader that reads from a file using the given
     * character set.
     *
     * @param file    the file to read from
     * @param charset the charset used to decode the input stream
     * @return the buffered reader
     */
    public static BufferedReader newReader(@NonNull File file, @NonNull Charset charset)
            throws FileNotFoundException {
        return new BufferedReader(
                new InputStreamReader(new FileInputStream(file), charset));
    }

    /**
     * Returns a buffered writer that writes to a file using default
     * character set.
     *
     * @param file the file to write to
     * @return the buffered writer
     */
    public static BufferedWriter newWriter(@NonNull File file)
            throws FileNotFoundException {
        return newWriter(file, Charset.defaultCharset());
    }

    /**
     * Returns a buffered writer that writes to a file using the given
     * character set.
     *
     * @param file    the file to write to
     * @param charset the charset used to encode the output stream
     * @return the buffered writer
     */
    public static BufferedWriter newWriter(@NonNull File file, @NonNull Charset charset)
            throws FileNotFoundException {
        return new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(file), charset));
    }

    public static String readFirstLine(File file, Charset charset) throws IOException {
        Closer closer = Closer.create();
        try {
            return closer.register(newReader(file, charset)).readLine();
        } finally {
            closer.close();
        }
    }
}
