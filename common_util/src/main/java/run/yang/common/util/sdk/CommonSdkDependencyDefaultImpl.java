package run.yang.common.util.sdk;


import run.yang.common.util.log.LogInterface;

/**
 * 创建时间: 2016/08/11 17:36 <br>
 * 作者: zisxks <br>
 * 描述: 对CommonSDK依赖的默认实现，接入方可选择继承此类，只重写感兴趣的方法
 */
public class CommonSdkDependencyDefaultImpl implements CommonUtilSdkDependency {
    @Override
    public boolean isDebug() {
        return false;
    }

    @Override
    public int getLogLevel() {
        return LogInterface.LOG_LEVEL_DEBUG;
    }

    @Override
    public int getFileLogLevel() {
        return LogInterface.LOG_LEVEL_DEBUG;
    }
}
