package run.yang.common.util.image.blur;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.util.android.Version;


/**
 * Created by zisxks on 2016-03-14 20:41.
 */
public class BlurBitmap {

    public static class Options {
        public Options(float radius) {
            this.radius = radius;
        }

        public Options(float radius, int downScaleFactor) {
            this(radius);
            this.downScale = true;
            this.downScaleFactor = downScaleFactor;
        }

        float radius;

        /**
         * Blur前是否 downscale, 可以加快速度，减少内存占用，如果设置为true，需同时设置 {@code downScaleFactor}
         */
        boolean downScale;

        /**
         * downScale设置为true，这个参数才会生效
         */
        int downScaleFactor;
    }

    @Nullable
    public static Bitmap blur(@Nullable Context context, @Nullable Bitmap inputBitmap, @NonNull Options options) {
        if (context == null || inputBitmap == null) {
            return null;
        }
        Bitmap bitmap;
        if (options.downScale) {
            bitmap = Bitmap.createScaledBitmap(inputBitmap,
                    inputBitmap.getWidth() / options.downScaleFactor,
                    inputBitmap.getHeight() / options.downScaleFactor,
                    true);
        } else {
            bitmap = inputBitmap;
        }

        if (Version.atLeast(Version.V17_42)) {
            bitmap = blurImageRenderScript(context, bitmap, options.radius);
        } else {
            bitmap = blurImageJavaAlgorithm(context, bitmap, options.radius);
        }
        if (options.downScale) {
            bitmap = Bitmap.createScaledBitmap(bitmap, inputBitmap.getWidth(), inputBitmap.getHeight(), true);
        }

        return bitmap;
    }

    @TargetApi(Version.V17_42)
    @Nullable
    private static Bitmap blurImageRenderScript(Context context, @Nullable Bitmap inputBitmap, float radius) {
        if (inputBitmap == null) {
            return null;
        }
        RenderScript rs = RenderScript.create(context);

        Bitmap blurredBitmap = inputBitmap.copy(Bitmap.Config.ARGB_8888, true);
        ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        Allocation input = Allocation.createFromBitmap(rs, inputBitmap);
        Allocation output = Allocation.createFromBitmap(rs, blurredBitmap);
        script.setInput(input);
        script.setRadius(radius);
        script.forEach(output);

        output.copyTo(blurredBitmap);

        return blurredBitmap;
    }

    private static Bitmap blurImageJavaAlgorithm(Context context, Bitmap inputBitmap, float radius) {
        return null;
    }
}
