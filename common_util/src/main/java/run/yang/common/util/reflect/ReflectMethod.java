package run.yang.common.util.reflect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import androidx.annotation.NonNull;

/**
 * Created by ty on 8/31/17.
 */

public class ReflectMethod {
    @NonNull
    private final Method mMethod;

    private ReflectMethod(@NonNull Method method) {
        mMethod = method;
    }

    public static ReflectMethod of(@NonNull Method method) {
        return new ReflectMethod(method);
    }

    public Object invoke(Object receiver, Object... args) throws InvocationTargetException, IllegalAccessException {
        return mMethod.invoke(receiver, args);
    }
}
