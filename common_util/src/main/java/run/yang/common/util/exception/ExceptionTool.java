package run.yang.common.util.exception;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by Yang Tianmei on 2016-03-24 13:04.
 */
public class ExceptionTool {
    @Nullable
    public static Throwable getRootCause(@Nullable Throwable e) {
        if (e == null) {
            return null;
        }

        Throwable root = e;
        while (root.getCause() != null) {
            root = root.getCause();
        }
        return root;
    }

    @NonNull
    public static String getShortDescription(@Nullable Throwable e) {
        if (e == null) {
            return "null";
        }

        return e.getClass().getSimpleName() + ": " + e.getLocalizedMessage();
    }
}
