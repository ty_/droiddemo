package run.yang.common.util.rom;

import android.text.TextUtils;

import run.yang.common.util.system.SystemProperties;

/**
 * CyanogenMod(CM) ROM tool
 * Created by Yang Tianmei on 2015-09-14 16:13.
 */
public class CyanogenModTool {

    public static final String CM_VERSION = SystemProperties.get("ro.modversion");

    public static boolean isCM() {
        return !TextUtils.isEmpty(CM_VERSION);
    }
}
