package run.yang.common.util.java.sizeof;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashSet;

import androidx.annotation.NonNull;
import run.yang.common.util.java.Unsafe;
import run.yang.common.util.system.DeviceTool;

/**
 * Created by ty on 11/20/17.
 *
 * <a href="https://github.com/dweiss/java-sizeof">https://github.com/dweiss/java-sizeof/</a>
 */
public class ObjectSizeEstimator {

    private static final int ALIGNMENT = DeviceTool.is64BitVm() ? 8 : 4;

    /**
     * @param clazz
     * @return
     */
    public static long sizeof(@NonNull Class<?> clazz) {
        try {
            final Unsafe u = Unsafe.getUnsafe();
            HashSet<Field> fields = new HashSet<>();
            while (clazz != Object.class) {
                for (Field f : clazz.getDeclaredFields()) {
                    if ((f.getModifiers() & Modifier.STATIC) == 0) {
                        fields.add(f);
                    }
                }
                clazz = clazz.getSuperclass();
            }

            // get offset
            long maxSize = 0;
            for (Field f : fields) {
                long offset = u.objectFieldOffset(f);
                if (offset > maxSize) {
                    maxSize = offset;
                }
            }
            return ((maxSize / ALIGNMENT) + 1) * ALIGNMENT;   // padding
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
