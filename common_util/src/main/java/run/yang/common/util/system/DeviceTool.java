package run.yang.common.util.system;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.Nullable;
import run.yang.common.dependency.ContextHolder;
import run.yang.common.util.android.Version;
import run.yang.common.util.basic.NumberTool;
import run.yang.common.util.io.Closeables;
import run.yang.common.util.io.Closer;
import run.yang.common.util.io.file.Files;
import run.yang.common.util.log.Logg;

/**
 * Created by Yang Tianmei on 2015-09-23.
 */
public class DeviceTool {
    private static final String TAG = DeviceTool.class.getSimpleName();

    @Nullable
    public static String getDeviceId() {
        try {
            Context context = ContextHolder.appContext();
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            return tm.getDeviceId();
        } catch (Exception e) {
            Logg.d(TAG, e.getMessage());
            return null;
        }
    }

    public static String getAndroidId() {
        final ContentResolver resolver = ContextHolder.appContext().getContentResolver();
        return Settings.System.getString(resolver, Settings.Secure.ANDROID_ID);
    }

    public static long getRamSizeApi16() {
        if (Version.atLeast(Version.V16_41)) {
            ActivityManager am = (ActivityManager) ContextHolder.appContext().getSystemService(Context.ACTIVITY_SERVICE);
            final ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            am.getMemoryInfo(memoryInfo);
            return memoryInfo.totalMem;
        }
        return 0;
    }

    /**
     * return value in kB, read from /proc/meminfo
     */
    public static long getRamSizeFromMemInfoInKb() {
        Closer closer = Closer.create();
        try {
            BufferedReader bufferedReader = closer.register(Files.newReader(new File("/proc/meminfo")));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (TextUtils.isEmpty(line)) {
                    continue;
                }

                if (line.contains("MemTotal")) {
                    final Pattern pattern = Pattern.compile("(\\d+)");
                    final Matcher matcher = pattern.matcher(line);
                    if (matcher.find()) {
                        String ramSize = matcher.group(1);
                        return NumberTool.parseLong(ramSize);
                    }
                    return 0;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            Closeables.closeOneLiner(closer);
        }

        return 0;
    }

    /**
     * <a href="https://stackoverflow.com/questions/32452924/how-to-detect-android-device-is-64-bit-or-32-bit-processor">
     * How to detect android device is 64 bit or 32 bit processor?</a>
     */
    @SuppressLint("NewApi")
    public static boolean is64BitVm() {
        return Version.atLeast(Version.V21_50) && Build.SUPPORTED_64_BIT_ABIS.length > 0;
    }
}
