package run.yang.common.util.log.bean;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import run.yang.common.util.android.Version;

/**
 * 创建时间: 2016/08/16 13:59 <br>
 * 作者: Yang Tianmei <br>
 * 描述:
 */
public class ParcelableThrowable implements Parcelable {

    private final ParcelableStackTraceElement[] mStackTraceElements;
    private final ParcelableThrowable mCause;
    private String mLocalizedMessage;
    private String mMessage;
    private ParcelableThrowable[] mSuppressed;
    private String mOriginalClassName;

    protected ParcelableThrowable(Parcel in) {
        mStackTraceElements = in.createTypedArray(ParcelableStackTraceElement.CREATOR);
        mCause = in.readParcelable(ParcelableThrowable.class.getClassLoader());
        mLocalizedMessage = in.readString();
        mMessage = in.readString();
        mSuppressed = in.createTypedArray(ParcelableThrowable.CREATOR);
        mOriginalClassName = in.readString();
    }

    public static final Creator<ParcelableThrowable> CREATOR = new Creator<ParcelableThrowable>() {
        @Override
        public ParcelableThrowable createFromParcel(Parcel in) {
            return new ParcelableThrowable(in);
        }

        @Override
        public ParcelableThrowable[] newArray(int size) {
            return new ParcelableThrowable[size];
        }
    };

    @Nullable
    public static ParcelableThrowable newInstance(@Nullable Throwable ex) {
        return ex == null ? null : new ParcelableThrowable(ex);
    }

    public ParcelableThrowable(Throwable ex) {
        StackTraceElement[] stackTraceElements = ex.getStackTrace();
        mStackTraceElements = new ParcelableStackTraceElement[stackTraceElements.length];
        for (int i = 0; i < stackTraceElements.length; i++) {
            mStackTraceElements[i] = new ParcelableStackTraceElement(stackTraceElements[i]);
        }
        mCause = newInstance(ex.getCause());
        mLocalizedMessage = ex.getLocalizedMessage();
        mMessage = ex.getMessage();
        mOriginalClassName = ex.getClass().getName();

        if (Version.atLeast(Version.V19_44)) {
            Throwable[] suppressed = ex.getSuppressed();
            mSuppressed = new ParcelableThrowable[suppressed.length];
            for (int i = 0; i < suppressed.length; i++) {
                mSuppressed[i] = new ParcelableThrowable(suppressed[i]);
            }
        } else {
            mSuppressed = null;
        }
    }

    public ParcelableThrowable[] getSuppressed() {
        return mSuppressed;
    }

    public ParcelableStackTraceElement[] getStackTraceElements() {
        return mStackTraceElements;
    }

    public ParcelableThrowable getCause() {
        return mCause;
    }

    public String getLocalizedMessage() {
        return mLocalizedMessage;
    }

    public String getMessage() {
        return mMessage;
    }

    public String getOriginalClassName() {
        return mOriginalClassName;
    }

    public Throwable toThrowable() {
        Throwable throwable = constructOriginalThrowable();

        StackTraceElement stackTraceElements[] = new StackTraceElement[mStackTraceElements.length];
        for (int i = 0; i < mStackTraceElements.length; i++) {
            stackTraceElements[i] = mStackTraceElements[i].toStackTraceElement();
        }
        throwable.setStackTrace(stackTraceElements);

        if (Version.atLeast(Version.V19_44)) {
            if (mSuppressed != null) {
                for (ParcelableThrowable parcelableThrowable : mSuppressed) {
                    throwable.addSuppressed(parcelableThrowable.toThrowable());
                }
            }
        }
        return throwable;
    }

    private Throwable constructOriginalThrowable() {
        String message = mMessage;
        Throwable cause = mCause == null ? null : mCause.toThrowable();

        if (!TextUtils.isEmpty(mOriginalClassName)) {
            try {
                Class<?> aClass = Class.forName(mOriginalClassName);
                if (Throwable.class.isAssignableFrom(aClass)) {
                    Throwable throwable = (Throwable) aClass.getConstructor(String.class).newInstance(message);
                    if (cause != null) {
                        throwable.initCause(cause);
                    }
                    return throwable;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return new Throwable(message, cause);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedArray(mStackTraceElements, i);
        parcel.writeParcelable(mCause, i);
        parcel.writeString(mLocalizedMessage);
        parcel.writeString(mMessage);
        parcel.writeTypedArray(mSuppressed, i);
        parcel.writeString(mOriginalClassName);
    }
}
