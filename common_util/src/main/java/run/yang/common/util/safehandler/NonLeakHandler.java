package run.yang.common.util.safehandler;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.lang.ref.WeakReference;

import androidx.annotation.NonNull;

/**
 * 直接在Activity内继承 Handler，eclipse有警告，会造成内存泄露。使用这个帮助类可以解决内存泄露的问题。
 * <p/>
 * OuterClass 通常是Activity，必需实现 <code>HandlerCallback</code> 接口
 * <p/>
 * Created by Tim Yang on 2015-05-07 16:29.
 */
public class NonLeakHandler<OuterClass extends HandlerCallback> extends Handler {
    @NonNull
    private final WeakReference<OuterClass> mOuterClass;

    public NonLeakHandler(OuterClass outerClass) {
        mOuterClass = new WeakReference<>(outerClass);
    }

    public NonLeakHandler(OuterClass outerClass, Looper looper) {
        super(looper);

        mOuterClass = new WeakReference<OuterClass>(outerClass);
    }

    @Override
    public void handleMessage(Message msg) {
        final OuterClass callback = mOuterClass.get();
        if (callback != null) {
            callback.handleMessage(msg);
        }
    }
}

