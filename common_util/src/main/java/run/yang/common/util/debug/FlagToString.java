package run.yang.common.util.debug;

import android.text.TextUtils;
import android.util.SparseArray;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;

/**
 * Created by ty on 2017-05-08.
 * <p> 将 int 型的 flag 转成对应的常量值 </p>
 * <p>
 * <p>使用示例： </p>
 *
 * <pre><code>
 * final FlagToString intentFlagToString = new FlagToString(Intent.class, "FLAG_");
 *
 * Intent intent = getIntent();
 * String flags = intentFlagToString.toString(intent.getFlags());
 * </code></pre>
 */

public class FlagToString {

    public static final int PUBLIC_STATIC_FINAL = Modifier.STATIC | Modifier.FINAL | Modifier.PUBLIC;

    private final Class<?> mClass;
    private SparseArray<String> mMapCache;
    private final FieldFilter mFieldFilter;
    private boolean mInited = false;

    public FlagToString(@NonNull Class<?> aClass, @NonNull String fieldNamePrefix) {
        mClass = aClass;
        mFieldFilter = new FieldNamePrefixFilter(fieldNamePrefix);
    }

    public FlagToString(@NonNull Class<?> aClass, @NonNull FieldFilter fieldFilter) {
        mClass = aClass;
        mFieldFilter = fieldFilter;
    }

    @NonNull
    public String toString(int flags) {
        return TextUtils.join(", ", toStringList(flags));
    }

    @NonNull
    public List<String> toStringList(int flags) {
        if (!mInited) {
            init();
            mInited = true;
        }
        final int size = mMapCache.size();
        if (size == 0) {
            return Collections.emptyList();
        }

        final List<String> values = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            int key = mMapCache.keyAt(i);
            if ((flags & key) == key) {
                values.add(mMapCache.valueAt(i));
            }
        }
        return values;
    }

    private void init() {
        mMapCache = new SparseArray<>();

        final Field[] fields = mClass.getDeclaredFields();
        try {
            for (Field field : fields) {
                if (Modifier.isStatic(field.getModifiers())
                        && field.getType() == int.class
                        && mFieldFilter.filter(field)) {
                    mMapCache.put((int) field.get(null), field.getName());
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public interface FieldFilter {
        boolean filter(@NonNull Field field);
    }

    static class FieldNamePrefixFilter implements FieldFilter {
        private final String mFieldPrefix;

        FieldNamePrefixFilter(@NonNull String fieldPrefix) {
            mFieldPrefix = fieldPrefix;
        }

        @Override
        public boolean filter(@NonNull Field field) {
            return (field.getModifiers() & PUBLIC_STATIC_FINAL) == PUBLIC_STATIC_FINAL
                    && field.getName().startsWith(mFieldPrefix);
        }
    }
}
