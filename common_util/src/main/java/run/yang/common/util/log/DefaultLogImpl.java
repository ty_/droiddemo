package run.yang.common.util.log;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.dependency.ContextHolder;
import run.yang.common.util.sdk.CommonUtilSdk;

/**
 * 创建时间: 2016/08/11 15:13 <br>
 * 作者: zisxks <br>
 * 描述:  Common Util库的默认打Log实现。log文件保存在 SD卡/lianjia/包名/log/lianjia*.log
 * <p>
 * __注意__: Log文件保存的目录，不使用 context.getExternalCacheDir 等路径在 SD卡/Android/app/包名
 * 下的路径，应用卸载时，这些文件会一同被删掉。给追查Log造成困难
 */
public class DefaultLogImpl implements LogInterface {
    /**
     * 统一TAG
     **/
    private static final String LOG_TAG_STRING = "Logg";

    /**
     * 日志总开关
     **/
    @LogInterface.LOG_LEVEL
    private static final int LOGCAT_LEVEL =
            CommonUtilSdk.getDependency().getLogLevel();

    @LogInterface.LOG_LEVEL
    private static final int FILE_LOG_LEVEL =
            CommonUtilSdk.getDependency().getFileLogLevel();

    private static final boolean VERBOSE = (LOGCAT_LEVEL <= LogInterface.LOG_LEVEL_VERBOSE);
    private static final boolean DEBUG = (LOGCAT_LEVEL <= LogInterface.LOG_LEVEL_DEBUG);
    private static final boolean INFO = (LOGCAT_LEVEL <= LogInterface.LOG_LEVEL_INFO);
    private static final boolean WARN = (LOGCAT_LEVEL <= LogInterface.LOG_LEVEL_WARN);
    private static final boolean ERROR = (LOGCAT_LEVEL <= LogInterface.LOG_LEVEL_ERROR);

    /**
     * 当前日志日志文件名
     **/
    private static final String LOG_FILE_NAME = "com.lianjia.log";

    /**
     * Log日志所在目录， SDCard/lianjia/package.name/log/
     */
    private static final String LOG_FILE_DIR_BASE = "lianjia/%s/log";

    /**
     * 日志文件大小阀值，只在commit方法调用时构建文件
     **/
    private static final long LOG_SIZE = 5 * 1024 * 1024;
    /**
     * 日志格式，形如：[2010-01-22 13:39:1][D][com.a.c]throwable occured
     **/
    private static final String LOG_ENTRY_FORMAT = "[%tF %tT][%s][%s]%s\n";

    private static PrintStream logStream;

    private static boolean initialized = false;

    private static void init() {
        if (initialized) {
            return;
        }
        synchronized (Logg.class) {
            if (initialized) {
                return;
            }
            try {
                changeToNewLogFileByFileSize();

                File cacheRoot = getLogFileDir();
                if (cacheRoot != null) {
                    File logFile = new File(cacheRoot, LOG_FILE_NAME);
                    logFile.createNewFile();
                    d(logFile);
                    if (logStream != null) {
                        logStream.close();
                    }
                    logStream = getLogFileStream(logFile);
                    initialized = true;
                }
            } catch (Exception e) {
                e("catch root throwable", e);
            }
        }
    }

    @Override
    public void e(@NonNull String tag, @Nullable String msg) {
        if (ERROR) {
            tag = getLogTagInner(tag);
            Log.e(LOG_TAG_STRING, tag + " : " + msg);
            if (FILE_LOG_LEVEL <= LogInterface.LOG_LEVEL_ERROR) write("E", tag, msg, null);
        }
    }

    @Override
    public void e(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable) {
        if (ERROR) {
            tag = getLogTagInner(tag);
            Log.e(LOG_TAG_STRING, tag + " : " + msg, throwable);
            if (FILE_LOG_LEVEL <= LogInterface.LOG_LEVEL_ERROR) write("E", tag, msg, throwable);
        }
    }

    @Override
    public void w(@NonNull String tag, @Nullable String msg) {
        if (WARN) {
            tag = getLogTagInner(tag);
            Log.w(LOG_TAG_STRING, tag + " : " + msg);
            if (FILE_LOG_LEVEL <= LogInterface.LOG_LEVEL_WARN) write("W", tag, msg, null);
        }
    }

    @Override
    public void w(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable) {
        if (WARN) {
            tag = getLogTagInner(tag);
            Log.w(LOG_TAG_STRING, tag + " : " + msg, throwable);
            if (FILE_LOG_LEVEL <= LogInterface.LOG_LEVEL_WARN) write("W", tag, msg, throwable);
        }
    }

    @Override
    public void i(@NonNull String tag, @Nullable String msg) {
        if (INFO) {
            tag = getLogTagInner(tag);
            Log.i(LOG_TAG_STRING, tag + " : " + msg);
            if (FILE_LOG_LEVEL <= LogInterface.LOG_LEVEL_INFO) write("I", tag, msg, null);
        }
    }

    @Override
    public void i(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable) {
        if (INFO) {
            tag = getLogTagInner(tag);
            Log.i(LOG_TAG_STRING, tag + " : " + msg, throwable);
            if (FILE_LOG_LEVEL <= LogInterface.LOG_LEVEL_INFO) write("I", tag, msg, throwable);
        }
    }

    @Override
    public void d(@NonNull String tag, @Nullable String msg) {
        if (DEBUG) {
            tag = getLogTagInner(tag);
            Log.d(LOG_TAG_STRING, tag + " : " + msg);
            if (FILE_LOG_LEVEL <= LogInterface.LOG_LEVEL_DEBUG) write("D", tag, msg, null);
        }
    }

    @Override
    public void d(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable) {
        if (DEBUG) {
            tag = getLogTagInner(tag);
            Log.d(LOG_TAG_STRING, tag + " : " + msg, throwable);
            if (FILE_LOG_LEVEL <= LogInterface.LOG_LEVEL_DEBUG) write("D", tag, msg, throwable);
        }
    }

    @Override
    public void v(@NonNull String tag, @Nullable String msg) {
        if (VERBOSE) {
            tag = getLogTagInner(tag);
            Log.v(LOG_TAG_STRING, tag + " : " + msg);
            if (FILE_LOG_LEVEL <= LogInterface.LOG_LEVEL_DEBUG) write("V", tag, msg, null);
        }
    }

    @Override
    public void v(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable) {
        if (VERBOSE) {
            tag = getLogTagInner(tag);
            Log.v(LOG_TAG_STRING, tag + " : " + msg, throwable);
            if (FILE_LOG_LEVEL <= LogInterface.LOG_LEVEL_DEBUG) write("V", tag, msg, throwable);
        }
    }

    /**
     * 开启新的日志文件，旧的日志命名为 lianjia_{日期}.log
     */
    public static synchronized String changeToNewLogFile() {
        try {
            File cacheRoot = getLogFileDir();

            if (cacheRoot != null) {
                File logFile = new File(cacheRoot, LOG_FILE_NAME);
                File backupFile = getBackupFile(cacheRoot);
                logFile.renameTo(backupFile);
                logFile.delete();
                logFile.createNewFile();
                v(backupFile);
                if (logStream != null) {
                    logStream.close();
                }
                logStream = getLogFileStream(logFile);
                return backupFile.getAbsolutePath();
            }
            return null;
        } catch (IOException e) {
            e("Create back log file & initLogProcess log stream failed", e);
            return null;
        }
    }

    private static void write(String level, String tag, String msg, Throwable throwable) {
        if (!initialized) init();
        if (logStream == null || logStream.checkError()) {
            initialized = false;
            return;
        }
        Date now = new Date();

        logStream.println(
                String.format(Locale.US, LOG_ENTRY_FORMAT, now, now, level, tag, " : " + msg));

        if (throwable != null) {
            throwable.printStackTrace(logStream);
            logStream.println();
        }
    }

    private static boolean isSdCardAvailable() {
        File file = Environment.getExternalStorageDirectory();
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED) && (file != null
                && file.exists());
    }

    /**
     * Log文件保存的目录，不使用 context.getExternalCacheDir 等路径在 SD卡/Android/app/包名
     * 下的路径，应用卸载时，这些文件会一同被删掉。给追查Log造成困难
     */
    @Nullable
    @CheckResult
    private static File getLogFileDir() {
        if (isSdCardAvailable()) {
            Context context = ContextHolder.appContext();
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            String logDirBase = String.format(Locale.US, LOG_FILE_DIR_BASE, context.getPackageName());
            File logDir = new File(externalStorageDirectory, logDirBase);
            if (!logDir.exists()) {
                if (!logDir.mkdirs()) {
                    warn_local("can't create cache dir, mkdirs return false");
                    return null;
                }
            }
            return logDir;
        } else {
            return null;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        if (logStream != null) logStream.close();
        super.finalize();
    }

    /**
     * 根据文件大小手动切分日志文件 默认为大于5M
     */
    private static synchronized void changeToNewLogFileByFileSize() {
        try {
            File cacheRoot = getLogFileDir();
            if (cacheRoot != null) {
                File logFile = new File(cacheRoot, LOG_FILE_NAME);
                if (logFile.length() > LOG_SIZE) {
                    File backfile = getBackupFile(cacheRoot);
                    logFile.renameTo(backfile);
                    logFile.delete();
                    logFile.createNewFile();
                    v(backfile);
                    if (logStream != null) {
                        logStream.close();
                    }
                    logStream = getLogFileStream(logFile);
                }
            }
        } catch (IOException e) {
            e("Create back log file & initLogProcess log stream failed", e);
        }
    }

    @NonNull
    private static File getBackupFile(File cacheRoot) {
        final String currentTimePart =
                new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.US).format(new Date());
        final String name = LOG_TAG_STRING + "_" + currentTimePart + ".log";
        return new File(cacheRoot, name);
    }

    @NonNull
    private static PrintStream getLogFileStream(@NonNull File logFile)
            throws FileNotFoundException, UnsupportedEncodingException {
        return new PrintStream(new FileOutputStream(logFile, true), true, "UTF-8");
    }

    @NonNull
    private String getLogTagInner(@NonNull String tag) {
        return Thread.currentThread().getName() + ":" + tag;
    }

    /**
     * 不写文件的错误日志
     */
    private static void e(String msg, Exception e) {
        if (ERROR) {
            Log.e(LOG_TAG_STRING, msg, e);
        }
    }

    /**
     * 不写文件的w
     */
    private static void warn_local(String message) {
        if (WARN) {
            Log.w(LOG_TAG_STRING, message);
        }
    }

    /**
     * 不写文件的debug日志
     */
    private static void d(File logFile) {
        if (DEBUG) {
            Log.d(LOG_TAG_STRING, "Log to file : " + logFile);
        }
    }

    /**
     * 不写文件的verbose日志
     */
    private static void v(File backfile) {
        if (VERBOSE) {
            Log.v(LOG_TAG_STRING, "Create back log file : " + backfile.getName());
        }
    }
}
