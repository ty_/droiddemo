package run.yang.common.util.rom;

import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.NonNull;
import run.yang.common.util.component.ContextTool;
import run.yang.common.util.system.SystemProperties;

/**
 * Created by Yang Tianmei on 2015-08-10 16:06.
 */
public class MiuiTool {
    private static final String TAG = MiuiTool.class.getSimpleName();

    public static final String MIUI_VERSION_STR = SystemProperties.get("ro.miui.ui.version.name");

    private static final boolean IS_XIAOMI_DEVICE = "xiaomi".equalsIgnoreCase(Build.MANUFACTURER);
    private static final boolean IS_MIUI_V5 = "V5".equals(MIUI_VERSION_STR);
    private static final boolean IS_MIUI_V6 = "V6".equals(MIUI_VERSION_STR);

    /**
     * is Xiaomi Device (look in Build.MANUFACTURER)
     *
     * @see <a href="http://dev.xiaomi.com/doc/p=254/index.html"/>http://dev.xiaomi.com/doc/p=254/index.html</a>
     */
    public static boolean isXiaoMiDevice() {
        return IS_XIAOMI_DEVICE;
    }

    /**
     * Is Xiaomi Miui ROM
     *
     * @see <a href="http://dev.xiaomi.com/doc/p=254/index.html"/>http://dev.xiaomi.com/doc/p=254/index.html</a>
     */
    public static boolean isMiui() {
        return MIUI_VERSION_STR != null && MIUI_VERSION_STR.startsWith("V");
    }

    /**
     * Is Xiaomi MiuiV5 ROM
     *
     * @see <a href="http://dev.xiaomi.com/doc/p=254/index.html"/>http://dev.xiaomi.com/doc/p=254/index.html</a>
     */
    public static boolean isMiuiV5() {
        return IS_MIUI_V5;
    }

    /**
     * s Xiaomi MiuiV6 ROM
     *
     * @see <a href="http://dev.xiaomi.com/doc/p=254/index.html"/>http://dev.xiaomi.com/doc/p=254/index.html</a>
     */
    public static boolean isMiuiV6() {
        return IS_MIUI_V6;
    }

    public static boolean startPermissionManager(@NonNull Context context, @NonNull String packageName) {
        Intent intent = new Intent();
        intent.setAction("miui.intent.action.APP_PERM_EDITOR");
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.putExtra("extra_pkgname", packageName);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return ContextTool.startActivityChecked(context, intent);
    }
}
