package run.yang.common.util.component;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import run.yang.common.util.android.Version;
import run.yang.common.util.log.Logg;

/**
 * Created by Yang Tianmei on 2015-08-05 19:38.
 */
public class ContextTool {
    private static final String TAG = ContextTool.class.getSimpleName();

    /**
     * return null if context or filter is null, otherwise return what Context.registerReceiver would return
     */
    public static <T extends BroadcastReceiver> Intent registerReceiver(
            @Nullable Context context,
            @Nullable T receiver,
            @Nullable IntentFilter filter) {

        if (context == null || filter == null) {
            return null;
        }

        Logg.d(TAG, "register receiver: " + (receiver != null ? receiver.getClass().getSimpleName() : null));
        return context.registerReceiver(receiver, filter);
    }

    public static <T extends BroadcastReceiver> void unregisterReceiver(
            @Nullable Context context,
            @Nullable T receiver) {
        if (context == null || receiver == null) {
            return;
        }
        context.unregisterReceiver(receiver);
        Logg.d(TAG, "unregister receiver: " + receiver.getClass().getSimpleName());
    }

    /**
     * 启动本APP内的Activity
     */
    public static void startActivity(@NonNull Context context, @NonNull Intent intent) {
        context.startActivity(intent);
    }

    /**
     * 启动本APP内的Activity
     */
    public static void startActivity(@NonNull Context context, @NonNull Intent intent, @Nullable Bundle options) {
        if (Version.atLeast(Version.V16_41)) {
            context.startActivity(intent, options);
        } else {
            context.startActivity(intent);
        }
    }

    /**
     * 启动外部Activity，如果启动失败（如Activity不存在），则返回false
     * <p>
     * 用于打开系统设置项等可能不存在的页面
     */
    public static boolean startActivityChecked(@NonNull Context context, @NonNull Intent intent) {
        try {
            startActivity(context, intent);
        } catch (ActivityNotFoundException e) {
            Logg.d(TAG, "unable to start activity, intent: " + intent.toString(), e);
            return false;
        } catch (SecurityException e) {
            // no permission, maybe target activity not exported
            Logg.d(TAG, "SecurityException: " + e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * 启动外部Activity，如果启动失败（如Activity不存在），则返回false
     * <p>
     * 用于打开系统设置项等可能不存在的页面
     */
    public static boolean startActivityChecked(@NonNull Context context, @NonNull Intent intent, @Nullable Bundle options) {
        try {
            if (Version.atLeast(Version.V16_41)) {
                startActivity(context, intent, options);
            } else {
                startActivity(context, intent);
            }
        } catch (ActivityNotFoundException e) {
            Logg.d(TAG, "unable to start activity, intent: " + intent.toString(), e);
            return false;
        } catch (SecurityException e) {
            // no permission, maybe target activity not exported
            Logg.d(TAG, "SecurityException: " + e.getMessage());
            return false;
        }
        return true;
    }

    public static void startActivityForResult(@NonNull Fragment fragment, @NonNull Intent intent, int requestCode) {
        fragment.startActivityForResult(intent, requestCode);
    }

    public static void startActivityForResult(@NonNull Activity activity, @NonNull Intent intent, int requestCode) {
        activity.startActivityForResult(intent, requestCode);
    }

    public static boolean startActivityForResultChecked(@NonNull Fragment fragment, @NonNull Intent intent, int requestCode) {
        try {
            startActivityForResult(fragment, intent, requestCode);
        } catch (ActivityNotFoundException e) {
            Logg.d(TAG, "unable to start activity, intent: " + intent.toString(), e);
            return false;
        } catch (SecurityException e) {
            // no permission, maybe target activity not exported
            Logg.d(TAG, "SecurityException: " + e.getMessage());
            return false;
        }
        return true;
    }

    public static boolean startActivityForResultChecked(@NonNull Activity activity, @NonNull Intent intent, int requestCode) {
        try {
            startActivityForResult(activity, intent, requestCode);
        } catch (ActivityNotFoundException e) {
            Logg.d(TAG, "unable to start activity, intent: " + intent.toString(), e);
            return false;
        } catch (SecurityException e) {
            // no permission, maybe target activity not exported
            Logg.d(TAG, "SecurityException: " + e.getMessage());
            return false;
        }
        return true;
    }
}
