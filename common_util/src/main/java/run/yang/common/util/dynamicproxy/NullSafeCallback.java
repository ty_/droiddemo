package run.yang.common.util.dynamicproxy;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.util.sdk.CommonUtilSdk;

/**
 * 调用接口方法前避免检查回调是否为空
 *
 * <p>see <a href="https://code.google.com/p/android/issues/detail?id=58753">https://code.google
 * .com/p/android/issues/detail?id=58753</a> for a case that this class can't be used on Android</p>
 * <p>
 * Created by zisxks on 2016-06-17 17:15
 */
public class NullSafeCallback {
    /**
     * 产生新的wrapper
     */
    @NonNull
    public static <Callback> Callback wrap(@NonNull Class<Callback> clazz) {
        return wrap(clazz, null);
    }

    /**
     * 产生新的wrapper,并设置被代理的实例的初值
     */
    @NonNull
    public static <Callback> Callback wrap(@NonNull Class<Callback> clazz,
                                           @Nullable Callback instance) {
        return wrap(clazz.getClassLoader(), clazz, instance);
    }

    /**
     * 产生新的 wrapper
     */
    @NonNull
    public static <Callback> Callback wrap(@NonNull ClassLoader classLoader,
                                           @NonNull Class<Callback> interfaceClass, @Nullable Callback instance) {
        if (CommonUtilSdk.getDependency().isDebug()) {
            checkAndroidDynamicProxyBugAndThrow(interfaceClass);
        }
        //noinspection unchecked
        return (Callback) Proxy.newProxyInstance(classLoader, new Class<?>[]{interfaceClass},
                new NullSafeCallbackWrapper<>(instance));
    }

    /**
     * 设置新的接口实现类
     *
     * @param proxy    通过 wrap 函数返回的接口代理。如果传递的不是wrap返回的值，
     *                 会抛出{@code java.lang.IllegalArgumentException }异常
     * @param instance 新的实例
     * @return previous Callback
     */
    public static <Callback> Callback update(@NonNull Callback proxy, @Nullable Callback instance) {
        return getCallbackProxy(proxy).setCallback(instance);
    }

    /**
     * @param proxy 通过 wrap 函数返回的接口代理。如果传递的不是wrap返回的值，
     *              会抛出{@code java.lang.IllegalArgumentException }异常
     * @return 被代理的实例类
     */
    public static <Callback> Callback getCallback(@NonNull Callback proxy) {
        return getCallbackProxy(proxy).getCallback();
    }

    private static <Callback> NullSafeCallbackWrapper<Callback> getCallbackProxy(
            @NonNull Callback proxy) {
        //noinspection unchecked
        return (NullSafeCallbackWrapper<Callback>) Proxy.getInvocationHandler(proxy);
    }

    private static void checkAndroidDynamicProxyBugAndThrow(Class<?> clazz) {
        if (!clazz.isInterface()) {
            throw new IllegalArgumentException(clazz + " is not interface");
        }

        final Map<String, Method> methodMap = new HashMap<>();

        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            methodMap.put(method.getName(), method);
        }

        List<Class<?>> interfaceList = new LinkedList<>();
        collectAllInterfaces(interfaceList, clazz);

        for (Class<?> declaredClass : interfaceList) {
            checkMethodNotOverridden(declaredClass, methodMap);
        }
    }

    private static void collectAllInterfaces(List<Class<?>> interfaceList, Class<?> clazz) {
        Class<?>[] interfaces = clazz.getInterfaces();
        for (Class<?> anInterface : interfaces) {
            if (!interfaceList.contains(anInterface)) {
                interfaceList.add(anInterface);
                collectAllInterfaces(interfaceList, anInterface);
            }
        }
    }

    private static void checkMethodNotOverridden(Class<?> declaredClass, Map<String, Method> methodMap) {
        if (!declaredClass.isInterface()) {
            return;
        }
        for (Method declaredMethod : declaredClass.getDeclaredMethods()) {
            String declaredMethodName = declaredMethod.getName();
            Method existingMethod = methodMap.get(declaredMethodName);
            if (existingMethod == null) {
                methodMap.put(declaredMethodName, declaredMethod);
                continue;
            }
            if (!Arrays.equals(declaredMethod.getParameterTypes(), existingMethod.getParameterTypes())) {
                methodMap.put(declaredMethodName, declaredMethod);
                continue;
            }
            if (declaredMethod.getReturnType().equals(existingMethod.getReturnType())) {
                methodMap.put(declaredMethodName, declaredMethod);
                continue;
            }
            String exceptionMsg = String.format(Locale.ROOT, "method '%s' override '%s' but with different return " +
                            "type, " +
                            "will cause bug in lower Android version, don't use NullSafeCallback in this case. see " +
                            "https://code.google.com/p/android/issues/detail?id=58753 for detail about the bug",
                    existingMethod,
                    declaredMethod);
            throw new IllegalArgumentException(exceptionMsg);
        }
    }

    private NullSafeCallback() {
    }
}
