package run.yang.common.util.system;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.util.log.Logg;

/**
 * Created by Yang Tianmei on 2015-09-09 20:38.
 */
public class PropertyTool {

    private static final String TAG = PropertyTool.class.getSimpleName();

    /**
     * on CyanogenMod 12.1, System.getProperties() returned properties don't have "unchangeableSystemProperties" part
     * so use reflection to try to get a full list
     */
    public static TreeMap<String, String> getSystemProperties() {
        final TreeMap<String, String> treeMap = new TreeMap<>();

        final Properties properties = System.getProperties();
        addPropertyValueToMap(treeMap, properties);

        final Properties unchangeableSystemProperties = getSystemPropertiesWithReflect("unchangeableSystemProperties");
        addPropertyValueToMap(treeMap, unchangeableSystemProperties);

        final Properties systemProperties = getSystemPropertiesWithReflect("systemProperties");
        addPropertyValueToMap(treeMap, systemProperties);

        return treeMap;
    }

    private static Properties getSystemPropertiesWithReflect(@NonNull final String fieldName) {
        try {
            Field field = System.class.getDeclaredField(fieldName);
            if (field != null) {
                field.setAccessible(true);
                return (Properties) field.get(System.class);
            }
        } catch (NoSuchFieldException | IllegalAccessException | ClassCastException e) {
            Logg.d(TAG, e.getMessage());
        }
        return null;
    }

    private static void addPropertyValueToMap(@NonNull final Map<String, String> map, @Nullable final Properties properties) {
        if (properties != null) {
            for (String key : properties.stringPropertyNames()) {
                if (!map.containsKey(key)) {
                    final String value = properties.getProperty(key);
                    map.put(key, value);
                }
            }
        }
    }
}
