package run.yang.common.util.dynamicproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicReference;

import androidx.annotation.Nullable;

/**
 * 创建时间: 2016/08/24 16:10 <br>
 * 作者: ty <br>
 * 描述: 如果接口实例为null，调用接口方法时，阻断调用并返回null
 */
class NullSafeCallbackWrapper<T> implements InvocationHandler, ProxyInstanceManagerInterface<T> {

    private final AtomicReference<T> mCallback;

    NullSafeCallbackWrapper(@Nullable T instance) {
        mCallback = new AtomicReference<>(instance);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        T instance = this.mCallback.get();
        return instance == null ? getDefaultValue(method) : method.invoke(instance, args);
    }

    @Override
    public T setCallback(@Nullable T instance) {
        return mCallback.getAndSet(instance);
    }

    @Nullable
    @Override
    public T getCallback() {
        return mCallback.get();
    }

    @Nullable
    private static Object getDefaultValue(Method method) {
        final Class<?> returnType = method.getReturnType();
        return returnType.isPrimitive() ? getPrimitiveTypeDefaultValue(returnType) : null;
    }

    /**
     * @param primitiveType boolean, byte, char, short, int, long, float, and double.
     * @see <a href="https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html">
     * https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html</a>
     */
    private static Object getPrimitiveTypeDefaultValue(Class<?> primitiveType) {
        if (primitiveType == boolean.class) {
            return false;
        } else if (primitiveType == byte.class) {
            return (byte) 0;
        } else if (primitiveType == char.class) {
            return (char) 0;
        } else if (primitiveType == short.class) {
            return (short) 0;
        } else if (primitiveType == int.class) {
            return 0;
        } else if (primitiveType == long.class) {
            return 0L;
        } else if (primitiveType == float.class) {
            return 0F;
        } else if (primitiveType == double.class) {
            return 0D;
        } else if (primitiveType == void.class) {
            return null;
        } else {
            throw new AssertionError("unknown primitive type: " + primitiveType);
        }
    }
}
