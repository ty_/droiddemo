package run.yang.common.util.log;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.dependency.ContextHolder;
import run.yang.common.util.log.bean.ParcelableThrowable;

/**
 * 创建时间: 2016/08/16 14:45 <br>
 * 作者: zisxks <br>
 * 描述:
 */
class LogClient implements LogInterface {

    private ILogService mILogService;

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mILogService = ILogService.Stub.asInterface(iBinder);
            try {
                iBinder.linkToDeath(mDeathRecipient, 0);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mILogService = null;
        }
    };

    private IBinder.DeathRecipient mDeathRecipient = new IBinder.DeathRecipient() {
        @Override
        public void binderDied() {
            mILogService = null;
        }
    };

    @Override
    public void e(@NonNull String tag, @Nullable String msg) {
        ILogService service = mILogService;
        if (service != null) {
            try {
                service.log(LOG_LEVEL_ERROR, tag, msg, null);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void e(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable) {
        ILogService service = mILogService;
        if (service != null) {
            try {
                service.log(LOG_LEVEL_ERROR, tag, msg, ParcelableThrowable.newInstance(throwable));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void w(@NonNull String tag, @Nullable String msg) {
        ILogService service = mILogService;
        if (service != null) {
            try {
                service.log(LOG_LEVEL_WARN, tag, msg, null);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void w(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable) {
        ILogService service = mILogService;
        if (service != null) {
            try {
                service.log(LOG_LEVEL_WARN, tag, msg, ParcelableThrowable.newInstance(throwable));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void i(@NonNull String tag, @Nullable String msg) {
        ILogService service = mILogService;
        if (service != null) {
            try {
                service.log(LOG_LEVEL_INFO, tag, msg, null);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void i(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable) {
        ILogService service = mILogService;
        if (service != null) {
            try {
                service.log(LOG_LEVEL_INFO, tag, msg, ParcelableThrowable.newInstance(throwable));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void d(@NonNull String tag, @Nullable String msg) {
        ILogService service = mILogService;
        if (service != null) {
            try {
                service.log(LOG_LEVEL_DEBUG, tag, msg, null);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void d(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable) {
        ILogService service = mILogService;
        if (service != null) {
            try {
                service.log(LOG_LEVEL_DEBUG, tag, msg, ParcelableThrowable.newInstance(throwable));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void v(@NonNull String tag, @Nullable String msg) {
        ILogService service = mILogService;
        if (service != null) {
            try {
                service.log(LOG_LEVEL_VERBOSE, tag, msg, null);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void v(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable) {
        ILogService service = mILogService;
        if (service != null) {
            try {
                service.log(LOG_LEVEL_VERBOSE, tag, msg, ParcelableThrowable.newInstance(throwable));
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    void bindLogService() {
        Context context = ContextHolder.appContext();

        Intent service = new Intent(context, LogService.class);
        context.bindService(service, mServiceConnection,
                Context.BIND_AUTO_CREATE | Context.BIND_IMPORTANT | Context.BIND_ABOVE_CLIENT);
    }

    void unbindLogService() {
        Context context = ContextHolder.appContext();
        context.unbindService(mServiceConnection);
    }
}
