package run.yang.common.util.compatibility;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import run.yang.common.util.android.Version;

/**
 * Created by Tim Yang on 2015-05-11 17:05.
 */
public class CharsetCompat {
    public static final Charset UTF_8;

    static {
        if (Version.atLeast(Version.V19_44)) {
            UTF_8 = StandardCharsets.UTF_8;
        } else {
            UTF_8 = Charset.forName("UTF-8");
        }
    }
}
