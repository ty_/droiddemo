package run.yang.common.util.log;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class LogService extends Service {

    private LogServiceBinder mBinder = new LogServiceBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
}
