package run.yang.common.util.component;

import android.content.Intent;
import android.os.BadParcelableException;

import androidx.annotation.CheckResult;
import run.yang.common.util.build.BuildControl;
import run.yang.common.util.log.Logg;

/**
 * Created by Tim Yang on 2016-03-28 16:05.
 */
public class IntentTool {

    private static final String TAG = Logg.getTag(IntentTool.class);

    /**
     * 检测App外部传递过来的Intent里是否包含本App里没有的类，以免调用 {@code Intent.getXXXExtra}时发生崩溃
     * 造成拒绝服务攻击
     * <p>
     * 参考：<a href="http://jaq.alibaba.com/blog.htm?id=55">Android应用本地拒绝服务漏洞</a>
     *
     * @param intent Activity 里通过 {@code getIntent}或{@code onNewIntent}得到的Intent
     * @return 如果返回 false，后续的getXXXExtra()也会发生异常，调用者应该将当前Activity的intent设置为一个
     * 空的Intent，以免发生意外。即
     * <pre>
     *   setIntent(new Intent());
     * </pre>
     */
    @CheckResult
    public static boolean checkIncomingIntent(Intent intent) {
        try {
            //第一次调用getXXXExtra家族函数会触发{@code Bundle.unparcel>，如果Bundle里包含本App中不存在的类，
            // 就会抛出{@code BadParcelableException}异常
            intent.getParcelableExtra("");
            return true;
        } catch (BadParcelableException e) {
            //Parcelable, see Intent source code
            if (BuildControl.isDebug || BuildControl.doLog) {
                e.printStackTrace();
            } else {
                Logg.e(TAG, "malformed intent: " + e);
            }
            return false;
        } catch (RuntimeException e) {
            //Serializable, see Intent source code
            if (BuildControl.isDebug || BuildControl.doLog) {
                e.printStackTrace();
            } else {
                Logg.e(TAG, "malformed intent: " + e);
            }
            return false;
        }
    }
}
