package run.yang.common.util.java;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * 创建时间: 2017/11/20 10:43 <br>
 * 作者: Yang Tianmei <br>
 * 描述: sun.misc.Unsafe wrapper
 *
 * @see <a href="http://mishadoff.com/blog/java-magic-part-4-sun-dot-misc-dot-unsafe/">Java Magic. Part 4: sun.misc.Unsafe</a>
 * @see <a href="http://hg.openjdk.java.net/jdk7/jdk7/jdk/file/9b8c96f96a0f/src/share/classes/sun/misc/Unsafe.java/">OpenJDK 7: sun.misc.Unsafe.java</a>
 */

public class Unsafe {

    private static Unsafe sUnsafeProxy;

    private final Object mUnsafe;
    private final Class<?> mUnsafeClass;
    private final ConcurrentMap<MethodCacheKey, Method> mMethodCache = new ConcurrentHashMap<>();

    public static Unsafe getUnsafe() throws IllegalAccessException, NoSuchFieldException, ClassNotFoundException {
        if (sUnsafeProxy == null) {
            synchronized (Unsafe.class) {
                if (sUnsafeProxy == null) {
                    sUnsafeProxy = new Unsafe();
                }
            }
        }
        return sUnsafeProxy;
    }

    private Unsafe() throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException {
        mUnsafeClass = Class.forName("sun.misc.Unsafe");

        Field f = mUnsafeClass.getDeclaredField("theUnsafe");
        f.setAccessible(true);
        mUnsafe = f.get(null);
    }

    public long objectFieldOffset(Field field) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        return (long) callMethod("objectFieldOffset", field, Field.class);
    }

    /**
     * Fetches a value from a given Java variable.
     * More specifically, fetches a field or array element within the given
     * object <code>o</code> at the given offset, or (if <code>o</code> is
     * null) from the memory address whose numerical value is the given
     * offset.
     * <p>
     * The results are undefined unless one of the following cases is true:
     * <ul>
     * <li>The offset was obtained from {@link #objectFieldOffset} on
     * the {@link Field} of some Java field and the object
     * referred to by <code>o</code> is of a class compatible with that
     * field's class.
     *
     * <li>The offset and object reference <code>o</code> (either null or
     * non-null) were both obtained via {@link #staticFieldOffset}
     * and {@link #staticFieldBase} (respectively) from the
     * reflective {@link Field} representation of some Java field.
     *
     * <li>The object referred to by <code>o</code> is an array, and the offset
     * is an integer of the form <code>B+N*S</code>, where <code>N</code> is
     * a valid index into the array, and <code>B</code> and <code>S</code> are
     * the values obtained by {@link #arrayBaseOffset} and {@link
     * #arrayIndexScale} (respectively) from the array's class.  The value
     * referred to is the <code>N</code><em>th</em> element of the array.
     *
     * </ul>
     * <p>
     * If one of the above cases is true, the call references a specific Java
     * variable (field or array element).  However, the results are undefined
     * if that variable is not in fact of the type returned by this method.
     * <p>
     * This method refers to a variable by means of two parameters, and so
     * it provides (in effect) a <em>double-register</em> addressing mode
     * for Java variables.  When the object reference is null, this method
     * uses its offset as an absolute address.  This is similar in operation
     * to methods such as {@link #getInt(long)}, which provide (in effect) a
     * <em>single-register</em> addressing mode for non-Java variables.
     * However, because Java variables may have a different layout in memory
     * from non-Java variables, programmers should not assume that these
     * two addressing modes are ever equivalent.  Also, programmers should
     * remember that offsets from the double-register addressing mode cannot
     * be portably confused with longs used in the single-register addressing
     * mode.
     *
     * @param o      Java heap object in which the variable resides, if any, else
     *               null
     * @param offset indication of where the variable resides in a Java heap
     *               object, if any, else a memory address locating the variable
     *               statically
     * @return the value fetched from the indicated Java variable
     * @throws RuntimeException No defined exceptions are thrown, not even
     *                          {@link NullPointerException}
     */
    public int getInt(Object o, long offset)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        return (int) callMethod("getInt", o, offset, Object.class, long.class);
    }

    /**
     * @see #getInt(Object, long)
     */
    public byte getByte(Object o, long offset)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        return (byte) callMethod("getByte", o, offset, Object.class, long.class);
    }

    /**
     * Report the size in bytes of a native pointer, as stored via {@link
     * #putAddress}.  This value will be either 4 or 8.  Note that the sizes of
     * other primitive types (as stored in native memory blocks) is determined
     * fully by their information content.
     */
    public int addressSize() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        return (int) callMethod("addressSize");
    }

    private Object callMethod(String methodName)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final MethodCacheKey key = MethodCacheKey.of(methodName);
        Method method = mMethodCache.get(key);
        if (method == null) {
            method = mUnsafeClass.getDeclaredMethod(methodName);
            method.setAccessible(true);
            mMethodCache.putIfAbsent(key, method);
        }
        return method.invoke(mUnsafe);
    }

    private Object callMethod(String methodName, Object arg1, Class<?> args1Class)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final MethodCacheKey key = MethodCacheKey.of(methodName, args1Class);
        Method method = mMethodCache.get(key);
        if (method == null) {
            method = mUnsafeClass.getDeclaredMethod(methodName, args1Class);
            method.setAccessible(true);
            mMethodCache.putIfAbsent(key, method);
        }
        return method.invoke(mUnsafe, arg1);
    }

    private Object callMethod(String methodName, Object arg1, Object arg2, Class<?> arg1Class, Class<?> arg2Class)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final MethodCacheKey key = MethodCacheKey.of(methodName, arg1Class, arg2Class);
        Method method = mMethodCache.get(key);
        if (method == null) {
            method = mUnsafeClass.getDeclaredMethod(methodName, arg1Class, arg2Class);
            method.setAccessible(true);
            mMethodCache.putIfAbsent(key, method);
        }
        return method.invoke(mUnsafe, arg1, arg2);
    }


    private static class MethodCacheKey {

        static MethodCacheKey of(@NonNull String methodName) {
            return new MethodCacheKey(methodName, null);
        }

        static MethodCacheKey of(@NonNull String methodName, @Nullable Class<?>... classes) {
            return new MethodCacheKey(methodName, classes);
        }

        @NonNull
        final String methodName;

        @Nullable
        final Class<?>[] classes;

        int hashcode = 0;

        private MethodCacheKey(@NonNull String methodName, @Nullable Class<?>[] classes) {
            Objects.requireNonNull(methodName);

            this.methodName = methodName;
            this.classes = classes;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (obj instanceof MethodCacheKey) {
                MethodCacheKey that = (MethodCacheKey) obj;
                return methodName.equals(that.methodName) && Arrays.equals(classes, that.classes);
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            if (hashcode == 0) {
                hashcode = 31 * methodName.hashCode() + Arrays.hashCode(classes);
            }
            return hashcode;
        }
    }
}
