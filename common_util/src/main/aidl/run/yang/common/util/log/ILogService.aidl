// ILogService.aidl
package run.yang.common.util.log;

// Declare any non-default types here with import statements
import run.yang.common.util.log.bean.ParcelableThrowable;

interface ILogService {
    void log(int logLevel, String tag, String message, in ParcelableThrowable throwable);
}
