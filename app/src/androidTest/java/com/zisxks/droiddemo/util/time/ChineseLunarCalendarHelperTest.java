package com.zisxks.droiddemo.util.time;

import android.icu.util.Calendar;
import android.icu.util.ChineseCalendar;
import android.os.Build;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.annotation.RequiresApi;
import androidx.test.filters.SmallTest;
import androidx.test.runner.AndroidJUnit4;
import run.yang.common.util.android.Version;

import static org.junit.Assert.assertEquals;

/**
 * Created by ty on 1/15/17.
 */
@RequiresApi(api = Build.VERSION_CODES.N)
@RunWith(AndroidJUnit4.class)
@SmallTest
public class ChineseLunarCalendarHelperTest {

    private ChineseCalendar mLeapMonthCalendar;
    private ChineseCalendar mOrdinaryCalendar;

    @Before
    public void setUp() throws Exception {
        // Online Chinese Lunar Calendar: http://www.hao123.com/rili

        if (Version.lessThan(Version.V24_70)) {
            throw new RuntimeException("This test requires at least Android 7.0");
        }

        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2017);
        calendar.set(Calendar.MONTH, Calendar.JULY);
        calendar.set(Calendar.DAY_OF_MONTH, 24);
        // 2017.07.24 - 农历闰六月初二
        mLeapMonthCalendar = new ChineseCalendar(calendar.getTime());

        calendar.set(Calendar.YEAR, 2017);
        calendar.set(Calendar.MONTH, Calendar.SEPTEMBER);
        calendar.set(Calendar.DAY_OF_MONTH, 11);
        // 2017.09.11 - 农历七月廿一
        mOrdinaryCalendar = new ChineseCalendar(calendar.getTime());
    }

    @After
    public void tearDown() throws Exception {
        mLeapMonthCalendar = null;
        mOrdinaryCalendar = null;
    }

    @Test
    public void getDayName() throws Exception {
        assertEquals("初二", ChineseLunarCalendarHelper.getDayName(mLeapMonthCalendar));
        assertEquals("廿一", ChineseLunarCalendarHelper.getDayName(mOrdinaryCalendar));
    }

    @Test
    public void getMonthName() throws Exception {
        assertEquals("闰六月", ChineseLunarCalendarHelper.getMonthName(mLeapMonthCalendar));
        assertEquals("七月", ChineseLunarCalendarHelper.getMonthName(mOrdinaryCalendar));
    }

    @Test
    public void getYear() throws Exception {
        assertEquals(2017, ChineseLunarCalendarHelper.getYear(mLeapMonthCalendar));
        assertEquals(2017, ChineseLunarCalendarHelper.getYear(mOrdinaryCalendar));
    }
}