package com.zisxks.droiddemo.activity.entrance.main;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.zisxks.droiddemo.R;
import com.zisxks.droiddemo.activity.fragmenthost.FragmentListActivity;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;

/**
 * Created by Yang Tianmei on 2015-08-22 18:49.
 */
class MainHelper {

    @Nullable
    static Bundle getChildStartAnimation(@NonNull final View v) {
        return ActivityOptionsCompat.makeScaleUpAnimation(v, 0, 0, v.getWidth(), v.getHeight()).toBundle();
    }

    static ArrayList<MainListItem> getListItems(Context context) {
        ArrayList<MainListItem> list = new ArrayList<>();

        MainListItem item = new MainListItem();
        item.id = FragmentListActivity.CATEGORY_SYSTEM;
        item.itemName = context.getString(R.string.activity_main_btn_system);
        list.add(item);

        item = new MainListItem();
        item.id = FragmentListActivity.CATEGORY_VIEW;
        item.itemName = context.getString(R.string.activity_main_btn_view);
        list.add(item);

        item = new MainListItem();
        item.id = FragmentListActivity.CATEGORY_ANIMATION;
        item.itemName = context.getString(R.string.activity_main_btn_animation);
        list.add(item);

        item = new MainListItem();
        item.id = FragmentListActivity.CATEGORY_THIRD_PARTY_LIBRARY;
        item.itemName = context.getString(R.string.activity_main_btn_3rd_party_library);
        list.add(item);

        item = new MainListItem();
        item.id = FragmentListActivity.CATEGORY_MATERIAL;
        item.itemName = context.getString(R.string.activity_main_btn_activity_list);
        list.add(item);

        item = new MainListItem();
        item.id = FragmentListActivity.CATEGORY_TEST_BUTTON;
        item.itemName = context.getString(R.string.activity_main_btn_magic_test);
        list.add(item);

        return list;
    }
}


