package com.zisxks.droiddemo.activity.fragmenthost;

import android.R;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import run.yang.common.ui.template.BaseActivity;
import run.yang.common.util.component.ContextTool;
import run.yang.common.util.constant.DataConst;
import run.yang.common.util.notif.ToastManager;

/**
 * Created by Tim Yang on 2015-04-21 11:04.
 */
public class FragmentListActivity extends BaseActivity implements ListView.OnItemClickListener {

    public static final String TAG = FragmentListActivity.class.getSimpleName();

    // type @ACTIVITY_CATEGORY
    public static final String sTagCategory = "category";

    @IntDef({CATEGORY_NONE, CATEGORY_SYSTEM, CATEGORY_MATERIAL, CATEGORY_ANIMATION, CATEGORY_VIEW,
            CATEGORY_THIRD_PARTY_LIBRARY, CATEGORY_TEST_BUTTON})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ACTIVITY_CATEGORY {

    }

    public static final int CATEGORY_NONE = 0;

    public static final int CATEGORY_SYSTEM = 1;

    public static final int CATEGORY_MATERIAL = 2;

    public static final int CATEGORY_ANIMATION = 3;
    public static final int CATEGORY_VIEW = 4;
    public static final int CATEGORY_THIRD_PARTY_LIBRARY = 5;
    public static final int CATEGORY_TEST_BUTTON = 6;

    private int mActivityCategory = CATEGORY_NONE;

    private List<Map<String, Object>> mDataList = null;

    private Comparator<Map<String, Object>> mListComparator = new Comparator<Map<String, Object>>() {
        @Override
        public int compare(@NonNull Map<String, Object> lhs, @NonNull Map<String, Object> rhs) {
            final String left = (String) lhs.get(DataConst.sTagKey);
            final String right = (String) rhs.get(DataConst.sTagKey);
            return left.compareTo(right);
        }
    };

    public static Intent createLaunchIntent(Context context, @ACTIVITY_CATEGORY int category) {
        Intent intent = new Intent(context, FragmentListActivity.class);
        intent.putExtra(sTagCategory, category);
        return intent;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        outState.putInt(sTagCategory, mActivityCategory);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Intent incomingIntent = getIntent();
        if (incomingIntent != null) {
            mActivityCategory = incomingIntent.getIntExtra(sTagCategory, CATEGORY_NONE);
        }

        mDataList = FragmentListHelper.getListDataByCategory(mActivityCategory);

        if (mDataList == null) {
            ToastManager.show("invalid category");
            return;
        }

        Collections.sort(mDataList, mListComparator);

        final SimpleAdapter adapter = new SimpleAdapter(this, mDataList, R.layout.simple_list_item_1,
                new String[]{DataConst.sTagKey}, new int[]{R.id.text1});

        adapter.setViewBinder(new MyViewBinder());
        adapter.setDropDownViewResource(R.layout.simple_expandable_list_item_1);

        ListView listView = new ListView(this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        setContentView(listView);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Map<String, Object> map = mDataList.get(position);

        final String title = (String) map.get(DataConst.sTagKey);
        final Class<? extends Fragment> clazz = (Class<? extends Fragment>) map.get(DataConst.sTagValue);

        final Intent intent = new Intent(this, FragmentHostActivity.class);
        intent.putExtra(FragmentHostActivity.sKeyFragmentClass, clazz);
        intent.putExtra(FragmentHostActivity.sKeyWindowTitle, title);

        ContextTool.startActivity(this, intent);
    }

    private static class MyViewBinder implements SimpleAdapter.ViewBinder {

        @Override
        public boolean setViewValue(@NonNull View view, Object data, String textRepresentation) {
            switch (view.getId()) {
                case R.id.text1:
                    ((TextView) view).setText(String.valueOf(data));
                    return true;
            }
            return false;
        }
    }
}
