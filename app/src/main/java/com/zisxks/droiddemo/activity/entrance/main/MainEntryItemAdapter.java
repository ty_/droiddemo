package com.zisxks.droiddemo.activity.entrance.main;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.zisxks.droiddemo.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import run.yang.common.ui.recycler.adapter.BaseRecyclerViewAdapter;
import run.yang.common.util.view.V;

/**
 * Created by zisxks on 2016-01-11 20:47.
 */
public class MainEntryItemAdapter extends BaseRecyclerViewAdapter<MainListItem, MainEntryItemAdapter.ViewHolder> {

    private View.OnClickListener mOnClickListener;

    public MainEntryItemAdapter(@NonNull Context context, @NonNull List<MainListItem> list, View.OnClickListener onClickListener) {
        super(context, list);
        mOnClickListener = onClickListener;

        setItemLayoutResId(R.layout.activity_main_entrance_item);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder = super.onCreateViewHolder(parent, viewType);
        holder.mButton.setOnClickListener(mOnClickListener);
        return holder;
    }

    @Override
    protected void onBindView(ViewHolder holder, MainListItem item, int position) {
        holder.mButton.setText(item.itemName);
        holder.mButton.setTag(item);
    }

    @Override
    protected ViewHolder newViewHolder(View rootView) {
        return new ViewHolder(rootView);
    }

    @Override
    public long getItemId(int position) {
        return mDataList.get(position).id;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        public Button mButton;

        public ViewHolder(View itemView) {
            super(itemView);
            mButton = V.findView(itemView, R.id.button);
        }
    }
}
