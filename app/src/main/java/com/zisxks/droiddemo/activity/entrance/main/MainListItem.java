package com.zisxks.droiddemo.activity.entrance.main;

import com.zisxks.droiddemo.activity.fragmenthost.FragmentListActivity;

/**
 * Created by zisxks on 2016-01-11 20:52.
 */
class MainListItem {
    @FragmentListActivity.ACTIVITY_CATEGORY
    public int id;
    public String itemName;
}
