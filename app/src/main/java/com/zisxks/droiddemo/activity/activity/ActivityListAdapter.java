package com.zisxks.droiddemo.activity.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Pair;
import android.view.View;

import java.util.List;

import androidx.annotation.NonNull;
import run.yang.common.ui.recycler.adapter.StringRecyclerAdapter;
import run.yang.common.util.component.ContextTool;

/**
 * Created by zisxks on 2015-07-26 23:26.
 */
class ActivityListAdapter extends StringRecyclerAdapter<Pair<String, Class<?>>> {

    private final Context mContext;
    private final View.OnClickListener mTextViewOnClickListener;

    public ActivityListAdapter(@NonNull Context context, @NonNull List<Pair<String, Class<?>>> dataList) {
        super(context, dataList);
        mContext = context;

        mTextViewOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                Pair<String, Class<?>> pair = mDataList.get(position);
                Intent intent = new Intent(mContext, pair.second);
                ContextTool.startActivity(mContext, intent);
            }
        };
    }

    @Override
    protected void onBindView(ViewHolder holder, Pair<String, Class<?>> item, int position) {
        holder.textView.setText(item.first);
        holder.textView.setTag(position);
        holder.textView.setOnClickListener(mTextViewOnClickListener);
    }
}
