package com.zisxks.droiddemo.activity.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;

import com.zisxks.droiddemo.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import run.yang.common.ui.template.BaseActivity;

public class ActivityListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_list);

        final RecyclerView recyclerView = findView(R.id.recyclerView);
        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        final List<Pair<String, Class<?>>> dataList = ActivityListHelper.getData();
        final RecyclerView.Adapter adapter = new ActivityListAdapter(this, dataList);
        recyclerView.setAdapter(adapter);
    }

    public static Intent createLaunchIntent(@NonNull Context context) {
        return new Intent(context, ActivityListActivity.class);
    }
}
