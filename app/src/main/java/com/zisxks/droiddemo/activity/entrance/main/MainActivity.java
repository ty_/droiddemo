package com.zisxks.droiddemo.activity.entrance.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.zisxks.droiddemo.activity.activity.ActivityListActivity;
import com.zisxks.droiddemo.activity.fragmenthost.FragmentListActivity;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.LayoutParams;
import run.yang.common.ui.template.BaseExportedActivity;
import run.yang.common.util.basic.UnitTool;
import run.yang.common.util.component.ContextTool;

public class MainActivity extends BaseExportedActivity implements View.OnClickListener {

    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayList<MainListItem> listItems = MainHelper.getListItems(this);
        final View.OnClickListener onClickListener = this;
        MainEntryItemAdapter itemAdapter = new MainEntryItemAdapter(this, listItems, onClickListener);

        final RecyclerView recyclerView = new RecyclerView(this);
        recyclerView.setAdapter(itemAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        final LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        final int margin = UnitTool.dp2px_i(this, 15);
        lp.setMargins(margin, margin, margin, margin);

        setContentView(recyclerView, lp);
    }

    @Override
    public void onClick(@NonNull View v) {
        MainListItem item = (MainListItem) v.getTag();
        switch (item.id) {
            case FragmentListActivity.CATEGORY_SYSTEM:
                openFragmentList(v, FragmentListActivity.CATEGORY_SYSTEM);
                break;

            case FragmentListActivity.CATEGORY_MATERIAL:
                openActivityList(v);
                break;

            case FragmentListActivity.CATEGORY_ANIMATION:
                openFragmentList(v, FragmentListActivity.CATEGORY_ANIMATION);
                break;

            case FragmentListActivity.CATEGORY_VIEW:
                openFragmentList(v, FragmentListActivity.CATEGORY_VIEW);
                break;

            case FragmentListActivity.CATEGORY_THIRD_PARTY_LIBRARY:
                openFragmentList(v, FragmentListActivity.CATEGORY_THIRD_PARTY_LIBRARY);
                break;

            case FragmentListActivity.CATEGORY_TEST_BUTTON:
                onTestButtonClicked();
                break;

            case FragmentListActivity.CATEGORY_NONE:
            default:
                throw new AssertionError("unknown category: " + item.id);
        }
    }

    private void openFragmentList(final View v,
                                  @FragmentListActivity.ACTIVITY_CATEGORY int category) {
        final Intent intent = FragmentListActivity.createLaunchIntent(this, category);
        final Bundle options = MainHelper.getChildStartAnimation(v);
        ContextTool.startActivity(this, intent, options);
    }

    private void openActivityList(final View v) {
        final Intent intent = ActivityListActivity.createLaunchIntent(this);
        final Bundle options = MainHelper.getChildStartAnimation(v);
        ContextTool.startActivity(this, intent, options);
    }

    private void onTestButtonClicked() {

    }
}
