package com.zisxks.droiddemo.activity.fragmenthost;

import android.content.Intent;
import android.os.Bundle;

import com.zisxks.droiddemo.R;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import run.yang.common.ui.template.BaseActivity;
import run.yang.common.util.log.Logg;
import run.yang.common.util.notif.ToastManager;


public class FragmentHostActivity extends BaseActivity {

    public static final String sKeyFragmentClass = "fragClass";
    public static final String sKeyWindowTitle = "title";
    private static final String TAG = Logg.getTag(FragmentHostActivity.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Intent incomingIntent = getIntent();
        if (incomingIntent == null) {
            ToastManager.show(R.string.error_get_fragment_info_null_intent);
            return;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();

        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            Logg.d(TAG, "already loaded fragments(" + fragments.size() + "):");
            for (Fragment fragment : fragments) {
                Logg.d(TAG, "loaded fragment: " + fragment);
            }
        } else {
            Logg.d(TAG, "no loaded fragment");
        }

        //noinspection unchecked
        Class<? extends Fragment> fragClass = (Class<? extends Fragment>) incomingIntent.getSerializableExtra(sKeyFragmentClass);

        Fragment fragment = null;
        try {
            fragment = fragClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        if (fragment != null) {
            fragmentManager.beginTransaction().replace(android.R.id.content, fragment).commit();
        } else {
            ToastManager.show(R.string.fail_to_create_fragment);
        }

        final String windowTitle = incomingIntent.getStringExtra(sKeyWindowTitle);
        setTitle(windowTitle);
    }
}
