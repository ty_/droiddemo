package com.zisxks.droiddemo.activity.fragmenthost;

import com.zisxks.droiddemo.fragment.animation.AnimatorFragment;
import com.zisxks.droiddemo.fragment.library.VolleyFragment;
import com.zisxks.droiddemo.fragment.library.retrofit.OkHttpRetrofitFragment;
import com.zisxks.droiddemo.fragment.misc.ColorUtilFragment;
import com.zisxks.droiddemo.fragment.misc.GradientColorFragment;
import com.zisxks.droiddemo.fragment.misc.LevelListFragment;
import com.zisxks.droiddemo.fragment.misc.ResolveIntentFragment;
import com.zisxks.droiddemo.fragment.misc.ShortcutFragment;
import com.zisxks.droiddemo.fragment.misc.SuRootFragment;
import com.zisxks.droiddemo.fragment.misc.app.AppInformationFragment;
import com.zisxks.droiddemo.fragment.misc.build_constant.BuildConstantFragment;
import com.zisxks.droiddemo.fragment.misc.contact.ContactsFragment;
import com.zisxks.droiddemo.fragment.misc.process.ProcessFragment;
import com.zisxks.droiddemo.fragment.misc.property.SystemPropertiesFragment;
import com.zisxks.droiddemo.fragment.misc.rom.CustomizedRomFragment;
import com.zisxks.droiddemo.fragment.misc.sysdir.SystemDirectoryFragment;
import com.zisxks.droiddemo.fragment.misc.time.SystemTimeFragment;
import com.zisxks.droiddemo.fragment.misc.unsafe.SunMiscUnsafeFragment;
import com.zisxks.droiddemo.fragment.system.accessibility.CurrentActivityAccessibilityFragment;
import com.zisxks.droiddemo.fragment.system.account.AccountManagerFragment;
import com.zisxks.droiddemo.fragment.system.device.DeviceInfoFragment;
import com.zisxks.droiddemo.fragment.system.locale.LocaleExplorerFragment;
import com.zisxks.droiddemo.fragment.system.location.SimpleLocationFragment;
import com.zisxks.droiddemo.fragment.system.memory.MemoryUsageFragment;
import com.zisxks.droiddemo.fragment.system.notification.BackStackBuilderFragment;
import com.zisxks.droiddemo.fragment.system.permission.PermissionFragment;
import com.zisxks.droiddemo.fragment.system.proc_fs.ProcFileSystemFragment;
import com.zisxks.droiddemo.fragment.view.BlurImageFragment;
import com.zisxks.droiddemo.fragment.view.TypedValueFragment;
import com.zisxks.droiddemo.fragment.view.bitmap.BitmapInfoFragment;
import com.zisxks.droiddemo.fragment.view.floating_window.FloatingWindowFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;
import androidx.fragment.app.Fragment;
import run.yang.common.util.android.Version;
import run.yang.common.util.constant.DataConst;

/**
 * Created by Yang Tianmei on 2015-07-24 15:07.
 */
class FragmentListHelper {

    /**
     * @param category @CATEGORY_ACTIVITY
     */
    static List<Map<String, Object>> getListDataByCategory(int category) {
        List<Map<String, Object>> retList = null;

        switch (category) {
            case FragmentListActivity.CATEGORY_SYSTEM:
                retList = initSystemFragmentData();
                break;

            case FragmentListActivity.CATEGORY_ANIMATION:
                retList = initAnimationFragmentData();
                break;

            case FragmentListActivity.CATEGORY_VIEW:
                retList = initViewFragmentData();
                break;

            case FragmentListActivity.CATEGORY_THIRD_PARTY_LIBRARY:
                retList = initThirdPartyLibraryData();
                break;

            case FragmentListActivity.CATEGORY_MATERIAL:
                // material example are activities
            case FragmentListActivity.CATEGORY_NONE:
            default:
                break;
        }

        return retList;
    }

    private static List<Map<String, Object>> initSystemFragmentData() {
        final List<Map<String, Object>> dataList = new ArrayList<>();

        dataList.add(getListEntry("Account Manager", AccountManagerFragment.class));
        dataList.add(getListEntry("App Information", AppInformationFragment.class));
        if (Version.atLeast(Version.V11_30)) {
            dataList.add(getListEntry("BackStackBuilder", BackStackBuilderFragment.class));
        }
        dataList.add(getListEntry("Build Constant", BuildConstantFragment.class));
        dataList.add(getListEntry("Contacts", ContactsFragment.class));
        if (Version.atLeast(Version.V14_40)) {
            dataList.add(getListEntry("Current Activity (Accessibility)", CurrentActivityAccessibilityFragment.class));
        }
        dataList.add(getListEntry("Customized ROM", CustomizedRomFragment.class));
        dataList.add(getListEntry("Date & Time", SystemTimeFragment.class));
        dataList.add(getListEntry("Device Info", DeviceInfoFragment.class));
        dataList.add(getListEntry("Exec shell(root) commands", SuRootFragment.class));
        dataList.add(getListEntry("Locale Explorer", LocaleExplorerFragment.class));
        dataList.add(getListEntry("Location (simple)", SimpleLocationFragment.class));
        dataList.add(getListEntry("Memory Usage", MemoryUsageFragment.class));
        dataList.add(getListEntry("Permission", PermissionFragment.class));
        dataList.add(getListEntry("/proc file system", ProcFileSystemFragment.class));
        dataList.add(getListEntry("Process Viewer", ProcessFragment.class));
        dataList.add(getListEntry("Resolve Intent", ResolveIntentFragment.class));
        dataList.add(getListEntry("Shortcut Creator", ShortcutFragment.class));
        dataList.add(getListEntry("sun.misc.Unsafe", SunMiscUnsafeFragment.class));
        dataList.add(getListEntry("System Directories", SystemDirectoryFragment.class));
        dataList.add(getListEntry("System Properties", SystemPropertiesFragment.class));

        return dataList;
    }

    private static List<Map<String, Object>> initAnimationFragmentData() {
        final List<Map<String, Object>> list = new ArrayList<>();

        list.add(getListEntry("Animator", AnimatorFragment.class));

        return list;
    }

    private static List<Map<String, Object>> initViewFragmentData() {
        final List<Map<String, Object>> dataList = new ArrayList<>();

        dataList.add(getListEntry("Bitmap Info", BitmapInfoFragment.class));
        dataList.add(getListEntry("Blur Image", BlurImageFragment.class));
        dataList.add(getListEntry("ColorUtil", ColorUtilFragment.class));
        dataList.add(getListEntry("Floating Window", FloatingWindowFragment.class));
        dataList.add(getListEntry("Gradient Color", GradientColorFragment.class));
        dataList.add(getListEntry("Level List", LevelListFragment.class));
        dataList.add(getListEntry("TypedValue", TypedValueFragment.class));

        return dataList;
    }

    private static List<Map<String, Object>> initThirdPartyLibraryData() {
        final List<Map<String, Object>> dataList = new ArrayList<>();

        dataList.add(getListEntry("OkHttp & Retrofit", OkHttpRetrofitFragment.class));
        dataList.add(getListEntry("Volley", VolleyFragment.class));

        return dataList;
    }

    @NonNull
    private static Map<String, Object> getListEntry(@NonNull String title, Class<? extends Fragment> clazz) {
        Map<String, Object> map = new ArrayMap<>(2);
        map.put(DataConst.sTagKey, title);
        map.put(DataConst.sTagValue, clazz);
        return map;
    }
}
