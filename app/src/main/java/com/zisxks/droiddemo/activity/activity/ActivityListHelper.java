package com.zisxks.droiddemo.activity.activity;

import android.util.Pair;

import com.zisxks.droiddemo.fragment.animation.login.AnimatedLoginActivity;
import com.zisxks.droiddemo.fragment.lifecycle.tabhost.TabHostFragmentLifecycleActivity;
import com.zisxks.droiddemo.fragment.lifecycle.viewpager.ViewPagerFragmentLifecycleActivity;
import com.zisxks.droiddemo.fragment.material.CodeLabMaterialActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;

/**
 * Created by zisxks on 2015-07-26 23:28.
 */
class ActivityListHelper {

    @NonNull
    static List<Pair<String, Class<?>>> getData() {
        List<Pair<String, Class<?>>> dataList = new ArrayList<>(5);

        dataList.add(Pair.create("Animated Login", AnimatedLoginActivity.class));
        dataList.add(Pair.create("CodeLab Material Examples", CodeLabMaterialActivity.class));
        dataList.add(Pair.create("Fragment Lifecycle in TabHost", TabHostFragmentLifecycleActivity.class));
        dataList.add(Pair.create("Fragment Lifecycle in ViewPager", ViewPagerFragmentLifecycleActivity.class));

        return dataList;
    }
}
