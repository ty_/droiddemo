package com.zisxks.droiddemo.activity.entrance.router;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import com.zisxks.droiddemo.BuildConfig;
import com.zisxks.droiddemo.R;

import androidx.core.text.HtmlCompat;
import run.yang.common.ui.template.BaseExportedActivity;
import run.yang.common.util.log.Logg;

public class ActionRouteActivity extends BaseExportedActivity {

    private static final String TAG = Logg.getTag(ActionRouteActivity.class);

    public static final String ACTION = "com.zisxks.droiddemo.action_router.app_info";
    public static final String EXTRA_TEST_INT = BuildConfig.APPLICATION_ID + ".action_router.extra.int";
    public static final String EXTRA_TEST_STRING = BuildConfig.APPLICATION_ID + ".action_router.extra.string";
    public static final String EXTRA_TEST_PARCEL = BuildConfig.APPLICATION_ID + ".action_router.extra.parcel";

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action_route);

        initView();

        handleIntent(getIntent());
    }

    private void initView() {
        mTextView = findView(R.id.textView);

        TextView textView = findView(R.id.activity_action_route_vulnerability_text_view);
        textView.setText(HtmlCompat.fromHtml(getString(R.string.activity_action_route_vulnerability_txt), 0));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void handleIntent(Intent intent) {
        CharSequence content = getIntentContent(intent);
        mTextView.setText(content);
    }

    private static CharSequence getIntentContent(Intent intent) {
        final SpannableStringBuilder sb = new SpannableStringBuilder();
        if (intent == null) {
            sb.append("intent is null");
            return sb;
        }

        int intExtra = intent.getIntExtra(EXTRA_TEST_INT, -1);
        String stringExtra = intent.getStringExtra(EXTRA_TEST_STRING);
        Parcelable parcelableExtra = intent.getParcelableExtra(EXTRA_TEST_PARCEL);

        sb.append("int extra: \t").append(String.valueOf(intExtra)).append("\n");
        sb.append("string extra: \t").append(String.valueOf(stringExtra)).append("\n");
        sb.append("parcelable: \t").append(String.valueOf(parcelableExtra)).append("\n");

        return sb;
    }
}
