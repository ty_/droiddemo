package com.zisxks.droiddemo.receiver;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.zisxks.droiddemo.R;
import com.zisxks.droiddemo.app.noti.NotiChannelId;
import com.zisxks.droiddemo.constant.NotifyId;

import java.io.File;
import java.nio.charset.Charset;

import androidx.core.app.NotificationCompat;
import run.yang.common.util.io.file.Files;
import run.yang.common.util.notif.ToastManager;

public class PackageFirstInstallAndReplaceReceiver extends BroadcastReceiver {
    private static final String TAG = PackageFirstInstallAndReplaceReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        final String tag = "install and update";
        final String action = intent.getAction();

        Log.d(tag, "Context: " + context.getClass().getName());
        Log.d(tag, "action: " + action);

        final String howToDoThis = "see " + PackageFirstInstallAndReplaceReceiver.class.getName() + "for how to do this with Android built-in mechanism";
        if (Intent.ACTION_MY_PACKAGE_REPLACED.equals(action)) {
            ToastManager.show("My package replaced, " + howToDoThis, Toast.LENGTH_LONG);
        } else if (Intent.ACTION_PACKAGE_FIRST_LAUNCH.equals(action)) {
            ToastManager.show("App first launch, " + howToDoThis, Toast.LENGTH_LONG);
        }

        try {
            int pid = android.os.Process.myPid();
            Log.d(tag, "new version: s pid: " + pid);

            File cmdLineFile = new File("/proc/" + pid + "/cmdline");
            String cmdLine = Files.readFirstLine(cmdLineFile, Charset.defaultCharset()).trim();

            Log.d(tag, "new version, cmdline: " + cmdLine);
        } catch (Throwable e) {
            Log.d(tag, Log.getStackTraceString(e));
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NotiChannelId.DEFAULT);
        builder.setContentTitle("Droiddemo self updated");
        builder.setContentText("Droiddemo self updated via " + action);
        builder.setAutoCancel(true);
        builder.setWhen(System.currentTimeMillis());
        builder.setSmallIcon(R.drawable.ic_launcher);

        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(NotifyId.FUNC_SELF_UPDATED, builder.build());
    }
}
