package com.zisxks.droiddemo.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import run.yang.common.util.basic.TextTool;
import run.yang.common.util.log.Logg;


public class AutoStartReceiver extends BroadcastReceiver {
    private static final String TAG = AutoStartReceiver.class.getSimpleName();

    public static final String[] ACTIONS = new String[]{
            Intent.ACTION_BOOT_COMPLETED,
    };

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        Logg.d(TAG, "received action: " + action);

        if (TextUtils.isEmpty(action) || !TextTool.contains(ACTIONS, action)) {
            return;
        }

    }
}
