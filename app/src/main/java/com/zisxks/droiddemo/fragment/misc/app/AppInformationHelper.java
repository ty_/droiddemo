package com.zisxks.droiddemo.fragment.misc.app;

import android.content.pm.ApplicationInfo;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import run.yang.common.util.basic.TimeTool;

/**
 * Created by Yang Tianmei on 2015-08-05 22:29.
 */
class AppInformationHelper {

    static CharSequence getListItemTitle(@NonNull SimpleAppInfoBean appInfo, final int position) {
        return String.format(Locale.US, "%03d. %s", position + 1, appInfo.label);
    }

    static CharSequence getListItemDetail(@NonNull SimpleAppInfoBean appInfo) {
        SpannableStringBuilder sb = new SpannableStringBuilder();

        int start = sb.length();
        sb.append("package: ").append(appInfo.packageName).append("\n");
        sb.append("versionName: ").append(String.valueOf(appInfo.versionName)).append("\n");
        sb.append("versionCode: ").append(String.valueOf(appInfo.versionCode)).append("\n");
        sb.append("Install: ").append(TimeTool.formatFormalTime(appInfo.firstInstallTime)).append("\n");
        sb.append("Update: ").append(TimeTool.formatFormalTime(appInfo.lastUpdateTime)).append("\n");
        sb.append("Installer: ").append(String.valueOf(appInfo.installerPackageName)).append("\n");
        sb.append("targetSdkVersion: ").append(String.valueOf(appInfo.targetSdkVersion)).append("\n");
        sb.append("property: ").append(getAppProperties(appInfo)).append("\n");
        sb.append("enabled: ").append(String.valueOf(appInfo.enabled)).append("\n");
        sb.append("uid: ").append(String.valueOf(appInfo.uid));
        sb.setSpan(new RelativeSizeSpan(0.8f), start, sb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sb;
    }

    private static String getAppProperties(@NonNull final SimpleAppInfoBean appInfo) {
        List<String> list = new LinkedList<>();

        if ((appInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0) {
            list.add("updated system app");
        } else if ((appInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
            list.add("system");
        }

        if ((appInfo.flags & ApplicationInfo.FLAG_IS_GAME) != 0) {
            list.add("game");
        }

        if ((appInfo.flags & ApplicationInfo.FLAG_STOPPED) != 0) {
            list.add("stopped");
        }

        if ((appInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0) {
            list.add("debuggable");
        }

        if ((appInfo.flags & ApplicationInfo.FLAG_EXTERNAL_STORAGE) != 0) {
            list.add("external storage");
        }

        if ((appInfo.flags & ApplicationInfo.FLAG_IS_DATA_ONLY) != 0) {
            list.add("data only");
        }

        if ((appInfo.flags & ApplicationInfo.FLAG_PERSISTENT) != 0) {
            list.add("persistent");
        }

        if ((appInfo.flags & ApplicationInfo.FLAG_TEST_ONLY) != 0) {
            list.add("test only");
        }

        if ((appInfo.flags & ApplicationInfo.FLAG_VM_SAFE_MODE) != 0) {
            list.add("VM safe mode");
        }

        return TextUtils.join(", ", list);
    }
}
