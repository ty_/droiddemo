package com.zisxks.droiddemo.fragment.misc.contact;

import android.provider.ContactsContract;

import java.util.List;

import androidx.annotation.Nullable;

/**
 * Created by Yang Tianmei on 2015-08-24 20:50.
 */
public class SimpleContactBean {
    public String displayName;

    @Nullable
    public List<PhoneNumber> phoneNumbers;

    /**
     * @see ContactsContract.ContactsColumns#HAS_PHONE_NUMBER
     */
    public int hasPhoneNumber;
}
