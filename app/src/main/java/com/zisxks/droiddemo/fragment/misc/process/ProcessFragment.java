package com.zisxks.droiddemo.fragment.misc.process;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.zisxks.droiddemo.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.basic.DataTool;
import run.yang.common.util.constant.DataConst;
import run.yang.common.util.log.Logg;


public class ProcessFragment extends BaseFragment implements AbsListView.OnItemClickListener {
    public static final String TAG = ProcessFragment.class.getSimpleName();
    List<ActivityManager.RunningAppProcessInfo> mRunningAppProcessInfoList;
    @Nullable
    private SimpleAdapter mAdapter;
    private ActivityManager mActivityManager;
    private PackageManager mPackageManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Context context = mContextActivity;

        mActivityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        mPackageManager = context.getPackageManager();

        mRunningAppProcessInfoList = getProcessInfo();
        List<Map<String, String>> textList = convertProcessInfoDataFormat(mRunningAppProcessInfoList);

        if (textList != null) {
            Logg.d(TAG, "running app size: " + textList.size());
        } else {
            Logg.d(TAG, "running app size null");
        }

        mAdapter = new SimpleAdapter(context, textList, android.R.layout.two_line_list_item,
                new String[]{DataConst.sTagKey, DataConst.sTagValue}, new int[]{android.R.id.text1, android.R.id.text2});
    }

    @Override
    public View onCreateViewImpl(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_process_list, container, false);

        ListView listView = (ListView) view.findViewById(android.R.id.list);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Logg.d(TAG, mRunningAppProcessInfoList != null ? mRunningAppProcessInfoList.get(position).toString() : null);
    }

    private List<ActivityManager.RunningAppProcessInfo> getProcessInfo() {
        return mActivityManager.getRunningAppProcesses();
    }

    @Nullable
    private List<Map<String, String>> convertProcessInfoDataFormat(@Nullable List<ActivityManager.RunningAppProcessInfo> runningAppProcessInfoList) {
        if (runningAppProcessInfoList == null) {
            return null;
        }

        List<Map<String, String>> list = new ArrayList<>(runningAppProcessInfoList.size());
        for (ActivityManager.RunningAppProcessInfo appInfo : runningAppProcessInfoList) {
            String label = null;
            try {
                label = String.valueOf(mPackageManager.getApplicationLabel(mPackageManager.getApplicationInfo(appInfo.pkgList[0], 0)));
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if (TextUtils.isEmpty(label)) {
                label = appInfo.processName;
            }

            final StringBuilder sb = new StringBuilder();
            if (appInfo.pkgList.length == 1 && TextUtils.equals(appInfo.processName, appInfo.pkgList[0])) {
                sb.append(appInfo.processName);
            } else {
                sb.append("process: ").append(appInfo.processName).append("\n");
                if (appInfo.pkgList.length == 1) {
                    sb.append("package: ").append(appInfo.pkgList[0]);
                } else {
                    sb.append("package list:\n").append(TextUtils.join("\n", appInfo.pkgList));
                }
            }
            list.add(DataTool.getListItemMap(label, sb.toString()));
        }

        return list;
    }

}
