package com.zisxks.droiddemo.fragment.misc.app;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by Yang Tianmei on 2015-08-05 22:30.
 */
class AppInformationLoaderThread extends Thread {
    private static final String TAG = AppInformationLoaderThread.class.getSimpleName();

    private static final int PROGRESSIVE_LOAD_THRESHOLD_APP_SIZE = 20;
    private static final int APP_ICON_MIN_SIZE_IN_DP = 48;

    private final Context mContext;
    private final LoadProcessListener mLoadProcessListener;
    private final PackageManager mPackageManager;
    private final int mAppIconWidthPixel;
    private final int mAppIconHeightPixel;

    AppInformationLoaderThread(@NonNull Context context, @Nullable final LoadProcessListener listener) {
        mContext = context;
        mPackageManager = context.getPackageManager();
        mLoadProcessListener = listener;

        final Point iconDimension = getAppIconDimension(context);
        mAppIconWidthPixel = iconDimension.x;
        mAppIconHeightPixel = iconDimension.y;
    }

    @Override
    public void run() {
        final List<PackageInfo> installedPackages = mPackageManager.getInstalledPackages(
                PackageManager.GET_UNINSTALLED_PACKAGES);
        List<SimpleAppInfoBean> tmpAppInfoList = new ArrayList<>(PROGRESSIVE_LOAD_THRESHOLD_APP_SIZE);

        for (PackageInfo packageInfo : installedPackages) {
            final SimpleAppInfoBean simpleAppInfoBean = constructAppInformation(packageInfo);
            tmpAppInfoList.add(simpleAppInfoBean);

            if (interrupted()) {
                finishLoading(tmpAppInfoList);
                return;
            }

            if (tmpAppInfoList.size() >= PROGRESSIVE_LOAD_THRESHOLD_APP_SIZE) {
                if (mLoadProcessListener != null) {
                    mLoadProcessListener.onMoreAppInfoLoaded(tmpAppInfoList);
                }
                tmpAppInfoList = new ArrayList<>(PROGRESSIVE_LOAD_THRESHOLD_APP_SIZE);
            }
        }
        finishLoading(tmpAppInfoList);
    }

    @NonNull
    private SimpleAppInfoBean constructAppInformation(final PackageInfo packageInfo) {
        SimpleAppInfoBean bean = new SimpleAppInfoBean();

        bean.packageName = packageInfo.packageName;
        bean.versionName = packageInfo.versionName;
        bean.versionCode = packageInfo.versionCode;
        bean.firstInstallTime = packageInfo.firstInstallTime;
        bean.lastUpdateTime = packageInfo.lastUpdateTime;

        final ApplicationInfo applicationInfo = packageInfo.applicationInfo;
        bean.uid = applicationInfo.uid;
        bean.label = mPackageManager.getApplicationLabel(applicationInfo);
        bean.appIcon = getScaledApplicationIcon(mPackageManager.getApplicationIcon(applicationInfo));
        bean.targetSdkVersion = applicationInfo.targetSdkVersion;
        bean.flags = applicationInfo.flags;
        bean.enabled = applicationInfo.enabled;
        bean.installerPackageName = mPackageManager.getInstallerPackageName(bean.packageName);


        return bean;
    }

    private Drawable getScaledApplicationIcon(final Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            final Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            if (bitmap.getWidth() == mAppIconWidthPixel && bitmap.getHeight() == mAppIconHeightPixel) {
                return drawable;
            }
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, mAppIconWidthPixel, mAppIconHeightPixel, false);
            return new BitmapDrawable(mContext.getResources(), scaledBitmap);
        } else {
            return drawable;
        }
    }

    private void finishLoading(final List<SimpleAppInfoBean> tmpAppInfoList) {
        if (mLoadProcessListener != null) {
            if (!tmpAppInfoList.isEmpty()) {
                mLoadProcessListener.onMoreAppInfoLoaded(tmpAppInfoList);
            }
            mLoadProcessListener.onLoadComplete();
        }
    }

    private Point getAppIconDimension(@NonNull Context context) {
        int minIconSize = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                APP_ICON_MIN_SIZE_IN_DP,
                context.getResources().getDisplayMetrics());

        int width = 0;
        int height = 0;
        Drawable defaultIcon = mPackageManager.getDefaultActivityIcon();
        if (defaultIcon instanceof BitmapDrawable) {
            final Bitmap bitmap = ((BitmapDrawable) defaultIcon).getBitmap();
            width = bitmap.getWidth();
            height = bitmap.getHeight();
            if (width < minIconSize || height < minIconSize) {
                width = height = minIconSize;
            }
        } else {
            width = height = minIconSize;
        }

        return new Point(width, height);
    }

    interface LoadProcessListener {
        /**
         * when interrupted, this method will be called with currently loaded app info before thread quit.
         * <p>
         * This method is called from background thread
         *
         * @param newlyLoadedAppInfoList the underlying thread no longer touch this list when this method is called
         */
        void onMoreAppInfoLoaded(final List<SimpleAppInfoBean> newlyLoadedAppInfoList);

        /**
         * called when load complete, or being interrupted
         */
        void onLoadComplete();
    }
}
