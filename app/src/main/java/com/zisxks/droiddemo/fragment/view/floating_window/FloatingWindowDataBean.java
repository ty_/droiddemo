package com.zisxks.droiddemo.fragment.view.floating_window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by Yang Tianmei on 2015-09-21.
 */
public class FloatingWindowDataBean {
    @NonNull
    public NetworkSpeedBean networkSpeed = new NetworkSpeedBean();
    public float rxSpeedBps = 0;
    public float txSpeedBps = 0;
    public int timeIntervalMillis = 0;

    public FloatingWindowDataBean() {
    }

    public FloatingWindowDataBean(@Nullable FloatingWindowDataBean another) {
        initWith(another);
    }

    @Override
    public String toString() {
        return "FloatingWindowDataBean{" +
                "networkSpeed=" + networkSpeed +
                ", rxSpeedBps=" + rxSpeedBps +
                ", txSpeedBps=" + txSpeedBps +
                ", timeIntervalMillis=" + timeIntervalMillis +
                '}';
    }

    public void initWith(@Nullable FloatingWindowDataBean bean) {
        if (bean != null) {
            this.networkSpeed.initWith(bean.networkSpeed);
            this.rxSpeedBps = bean.rxSpeedBps;
            this.txSpeedBps = bean.txSpeedBps;
        }
    }
}
