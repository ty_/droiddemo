package com.zisxks.droiddemo.fragment.misc.time;


import android.os.Bundle;
import android.os.Message;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.zisxks.droiddemo.R;

import java.util.List;

import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.safehandler.HandlerCallback;
import run.yang.common.util.safehandler.NonLeakHandler;
import run.yang.common.util.view.V;


public class SystemTimeFragment extends BaseFragment implements HandlerCallback {

    /**
     * send every second
     */
    private static final int MSG_TIME_TICK = 1;

    private ListView mListView;
    private List<SystemTimeHelper.TimeClockViewer> mClockList;
    private SystemTimeAdapter mSystemTimeAdapter;
    private NonLeakHandler<SystemTimeFragment> mHandler;

    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_system_time, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mListView = V.findView(view, R.id.listView);

        initData();
        updateUI();
    }

    private void initData() {
        mClockList = SystemTimeHelper.getTimeClocks(mContextActivity);
        mSystemTimeAdapter = new SystemTimeAdapter(mContextActivity, mClockList);
        mListView.setAdapter(mSystemTimeAdapter);
    }

    private void updateUI() {
        mSystemTimeAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mHandler != null) {
            mHandler.removeMessages(MSG_TIME_TICK);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mHandler == null) {
            mHandler = new NonLeakHandler<>(this);
        }

        mHandler.removeMessages(MSG_TIME_TICK);
        postTickEvent();
    }

    @Override
    public void onDestroy() {
        if (mHandler != null) {
            mHandler.removeMessages(MSG_TIME_TICK);
            mHandler = null;
        }

        super.onDestroy();
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case MSG_TIME_TICK:
                updateUI();
                postTickEvent();
                break;
            default:
                break;
        }
    }

    private void postTickEvent() {
        mHandler.sendEmptyMessageDelayed(MSG_TIME_TICK, DateUtils.SECOND_IN_MILLIS);
    }

}
