package com.zisxks.droiddemo.fragment.view.floating_window;

import android.net.TrafficStats;
import android.os.Handler;

import androidx.collection.CircularArray;
import run.yang.common.util.util.PoolWrapper;

/**
 * Created by Yang Tianmei on 2015-09-21.
 */
public class UpdateUiThread extends Thread {

    private static final String TAG = UpdateUiThread.class.getSimpleName();

    public static final int MSG_UPDATE_FLOATING_WINDOW_DATA = 1;

    private int mUpdateIntervalMillis = 800;

    private final Handler mHandler;
    private final PoolWrapper<FloatingWindowDataBean> mPool;

    /**
     * @param pool reuse FloatingWindowDataBean, this class only need one
     */
    public UpdateUiThread(Handler handler, PoolWrapper<FloatingWindowDataBean> pool) {
        mHandler = handler;
        mPool = pool;
    }

    public void setUpdateIntervalMillis(int updateIntervalMillis) {
        if (updateIntervalMillis < 100) {
            updateIntervalMillis = 100;
        }
        mUpdateIntervalMillis = updateIntervalMillis;
    }

    public int getUpdateIntervalMillis() {
        return mUpdateIntervalMillis;
    }

    @Override
    public void run() {
        final int historySize = 3;
        final CircularArray<NetworkSpeedBean> history = new CircularArray<>(historySize);
        {
            final NetworkSpeedBean firstBean = new NetworkSpeedBean();
            firstBean.rxBytesTotal = TrafficStats.getTotalRxBytes();
            firstBean.txBytesTotal = TrafficStats.getTotalTxBytes();
            history.addFirst(firstBean);
        }

        while (!Thread.interrupted()) {
            try {
                Thread.sleep(mUpdateIntervalMillis);
            } catch (InterruptedException e) {
                return;
            }

            // reuse previous bean if available to avoid GC
            final NetworkSpeedBean bean = historySize == history.size() ? history.popLast() : new NetworkSpeedBean();
            bean.rxBytesTotal = TrafficStats.getTotalRxBytes();
            bean.txBytesTotal = TrafficStats.getTotalTxBytes();
            history.addFirst(bean);

            final FloatingWindowDataBean outBean = mPool.obtain();
            outBean.networkSpeed.initWith(bean);
            calculateRealtimeSpeed(history, outBean);
            mHandler.obtainMessage(MSG_UPDATE_FLOATING_WINDOW_DATA, outBean).sendToTarget();
        }
    }

    private void calculateRealtimeSpeed(CircularArray<NetworkSpeedBean> history, FloatingWindowDataBean bean) {
        final int size = history.size();
        final NetworkSpeedBean oldest = history.get(size - 1);
        final NetworkSpeedBean newest = history.get(0);

        bean.timeIntervalMillis = mUpdateIntervalMillis * (size - 1);
        final float intervalSeconds = bean.timeIntervalMillis / (float) 1000;

        bean.rxSpeedBps = (newest.rxBytesTotal - oldest.rxBytesTotal) / intervalSeconds;
        bean.txSpeedBps = (newest.txBytesTotal - oldest.txBytesTotal) / intervalSeconds;
    }
}
