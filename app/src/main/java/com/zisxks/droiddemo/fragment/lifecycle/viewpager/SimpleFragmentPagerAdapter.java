package com.zisxks.droiddemo.fragment.lifecycle.viewpager;

import com.zisxks.droiddemo.fragment.lifecycle.LifecyclePagerItemFragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

/**
 * 创建时间: 2016/11/24 11:34 <br>
 * 作者: Yang Tianmei <br>
 * 描述:
 */

class SimpleFragmentPagerAdapter extends FragmentStatePagerAdapter {

    private static final int FRAGMENT_COUNT = 6;

    SimpleFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return LifecyclePagerItemFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return FRAGMENT_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "No. " + position;
    }
}
