package com.zisxks.droiddemo.fragment.misc.rom;

import run.yang.common.util.rom.CyanogenModTool;
import run.yang.common.util.rom.MiuiTool;

/**
 * Created by Yang Tianmei on 2015-09-14 16:18.
 */
public class CustomizedRomHelper {
    public static String getRomDescription() {

        if (MiuiTool.isMiui()) {
            return "Miui " + MiuiTool.MIUI_VERSION_STR;
        } else if (CyanogenModTool.isCM()) {
            return "CyanogenMod " + CyanogenModTool.CM_VERSION;
        } else {
            return "maybe vanilla Android";
        }
    }
}
