package com.zisxks.droiddemo.fragment.animation;


import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;

import com.zisxks.droiddemo.R;

import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.android.Version;

public class AnimatorFragment extends BaseFragment {

    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_animator, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        View targetView = view.findViewById(R.id.fragment_animator_text_view);

        if (Version.atLeast(Version.V11_30)) {
            ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(targetView, "alpha", 0, 1.0f);
            alphaAnimator.setDuration(1000);
            alphaAnimator.setInterpolator(new DecelerateInterpolator());
            alphaAnimator.start();
        }
    }
}
