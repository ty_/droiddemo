package com.zisxks.droiddemo.fragment.view.floating_window;

import android.Manifest;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Message;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresPermission;
import androidx.core.util.Pools;
import run.yang.common.util.basic.UnitTool;
import run.yang.common.util.component.PermissionTool;
import run.yang.common.util.log.Logg;
import run.yang.common.util.safehandler.HandlerCallback;
import run.yang.common.util.safehandler.NonLeakHandler;
import run.yang.common.util.util.PoolWrapper;
import run.yang.common.util.view.PaddingHelper;

/**
 * inspired by: <a target="_blank" href="http://blog.csdn.net/deng0zhaotai/article/details/16827719">Android常用控件之悬浮窗</a>
 * <br />
 * see also <a target="_blank" href="http://www.cnblogs.com/mengdd/p/3824782.html">Android悬浮窗实现 使用WindowManager</a>
 * <br/>
 * Created by Yang Tianmei on 2015-09-21.
 */
public class FloatingWindowWidget implements HandlerCallback {

    private static final String TAG = FloatingWindowWidget.class.getSimpleName();

    private UpdateUiThread mUpdateThread;
    private NonLeakHandler<FloatingWindowWidget> mHandler;
    private PoolWrapper<FloatingWindowDataBean> mPool;
    private WindowManager mWindowManager;

    private int mStatusBarHeight;
    private View mFloatingWindow;
    private WindowManager.LayoutParams mLayoutParams;
    private TextView mTextView;
    private boolean mIsShowing;

    @RequiresPermission(Manifest.permission.SYSTEM_ALERT_WINDOW)
    public void start(@NonNull Context context) {
        if (!PermissionTool.checkSystemAlertWindowPermission()) {
            Logg.w(TAG, "no SYSTEM_ALERT_WINDOW permission to start floating window");
            return;
        }
        createFloatingWindow(context);

        mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        mLayoutParams = getLayoutParameter();

        mWindowManager.addView(mFloatingWindow, mLayoutParams);
        mIsShowing = true;

        // use object pool to avoid frequent object allocation and GC. in general case,
        // only need 1 FloatingWindowDataBean, unless experiencing significant latency
        mPool = new PoolWrapper<FloatingWindowDataBean>(new Pools.SynchronizedPool<>(2)) {
            @Override
            protected FloatingWindowDataBean newItem() {
                return new FloatingWindowDataBean();
            }
        };
        mHandler = new NonLeakHandler<>(this);
        mUpdateThread = new UpdateUiThread(mHandler, mPool);
        mUpdateThread.start();
    }

    private void createFloatingWindow(@NonNull Context context) {
        mTextView = new TextView(context);
        final ViewGroup.LayoutParams textViewLayoutParams = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        mTextView.setLayoutParams(textViewLayoutParams);
        mTextView.setBackgroundColor(Color.WHITE);
        mTextView.setTextColor(Color.BLACK);
        mTextView.setTypeface(Typeface.SANS_SERIF);
        updateUiWith(new FloatingWindowDataBean());

        final int paddingLeftRight = UnitTool.dp2px_i(context, 4);
        final int paddingTopBottom = UnitTool.dp2px_i(context, 3);
        PaddingHelper.with(mTextView).horizontal(paddingLeftRight).vertical(paddingTopBottom).done();

        mFloatingWindow = mTextView;
        mFloatingWindow.setOnTouchListener(new WidgetOnTouchListener());
    }

    private WindowManager.LayoutParams getLayoutParameter() {
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        params.format = PixelFormat.TRANSPARENT;
        params.gravity = Gravity.TOP | Gravity.LEFT;

        return params;
    }

    public void destroy() {
        if (mIsShowing) {
            mUpdateThread.interrupt();
            mUpdateThread = null;
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
            mPool = null;

            mWindowManager.removeView(mFloatingWindow);
            mIsShowing = false;
        }
    }

    private void refreshView(int dx, int dy) {
        // 状态栏高度不能立即取，不然得到的值是0
        if (mStatusBarHeight == 0) {
            View rootView = mFloatingWindow.getRootView();
            Rect rect = new Rect();
            rootView.getWindowVisibleDisplayFrame(rect);
            mStatusBarHeight = rect.top;
            Logg.d(TAG, "statusBarHeight: " + mStatusBarHeight + ", rootViewRect: " + rect);
        }

        mLayoutParams.x = dx;
        // y轴减去状态栏的高度，因为状态栏不是用户可以绘制的区域，不然拖动的时候会有跳动
        mLayoutParams.y = dy - mStatusBarHeight;

        refresh();
    }

    private void refresh() {
        mWindowManager.updateViewLayout(mFloatingWindow, mLayoutParams);
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case UpdateUiThread.MSG_UPDATE_FLOATING_WINDOW_DATA:
                FloatingWindowDataBean bean = (FloatingWindowDataBean) msg.obj;
                try {
                    updateUiWith(bean);
                } finally {
                    if (mPool != null) {
                        mPool.recycle(bean);
                    }
                }
                break;
        }
    }

    private void updateUiWith(@NonNull FloatingWindowDataBean bean) {
        // \u2193 down arrow, \u2191 up arrow
        final String text = String.format(Locale.US, "%.2f KB/s\u2193 %.2f KB/s\u2191", bean.rxSpeedBps / 1024, bean.txSpeedBps / 1024);
        mTextView.setText(text);
    }

    private class WidgetOnTouchListener implements View.OnTouchListener {

        float[] tmp = new float[2];

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            final int eventAction = event.getAction();
            switch (eventAction) {
                case MotionEvent.ACTION_DOWN:
                    tmp[0] = event.getX();
                    tmp[1] = event.getY();
                    break;

                case MotionEvent.ACTION_MOVE:
                    int dx = (int) (event.getRawX() - tmp[0]);
                    int dy = (int) (event.getRawY() - tmp[1]);
                    refreshView(dx, dy);
                    break;
            }
            return false;
        }
    }
}


