package com.zisxks.droiddemo.fragment.misc.app.detail;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.ServiceInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import run.yang.common.util.log.Logg;

/**
 * Created by Tim Yang on 2016-03-30 15:08.
 */
public class AppDetailedComponentHelper {

    private static final String TAG = Logg.getTag(AppDetailedComponentHelper.class);

    @Nullable
    public static List<Pair<CharSequence, List<CharSequence>>> getComponentInfo(
            @NonNull Context context, @Nullable String packageName) {
        if (TextUtils.isEmpty(packageName)) {
            return null;
        }

        final PackageManager pm = context.getPackageManager();
        PackageInfo packageInfo = getPackageComponentInfo(pm, packageName);
        if (packageInfo == null) {
            return null;
        }

        final AttributeCallback<ActivityInfo> activityInfoAttributeCallback = new AttributeCallback<ActivityInfo>() {
            @Override
            public String getName(ActivityInfo item) {
                return item.name;
            }

            @Override
            public boolean exported(ActivityInfo item) {
                return item.exported;
            }
        };
        final AttributeCallback<ProviderInfo> providerInfoAttributeCallback = new AttributeCallback<ProviderInfo>() {
            @Override
            public String getName(ProviderInfo item) {
                return item.name;
            }

            @Override
            public boolean exported(ProviderInfo item) {
                return item.exported;
            }
        };
        final AttributeCallback<ServiceInfo> serviceInfoAttributeCallback = new AttributeCallback<ServiceInfo>() {
            @Override
            public String getName(ServiceInfo item) {
                return item.name;
            }

            @Override
            public boolean exported(ServiceInfo item) {
                return item.exported;
            }
        };

        final List<Pair<CharSequence, List<CharSequence>>> data = new ArrayList<>(4);

        Pair<CharSequence, List<CharSequence>> componentInfo = getComponentInfo(
                packageInfo.activities, activityInfoAttributeCallback, "Activity");
        data.add(componentInfo);

        componentInfo = getComponentInfo(packageInfo.receivers, activityInfoAttributeCallback, "Receiver");
        data.add(componentInfo);

        componentInfo = getComponentInfo(packageInfo.services, serviceInfoAttributeCallback, "Service");
        data.add(componentInfo);

        componentInfo = getComponentInfo(packageInfo.providers, providerInfoAttributeCallback, "Provider");
        data.add(componentInfo);

        return data;
    }

    interface AttributeCallback<T> {
        String getName(T item);

        boolean exported(T item);
    }

    private static CharSequence getSubTitle(String title, int size) {
        SpannableString s = new SpannableString(title + " (" + size + ")");
        s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        Logg.d(TAG, title);
        return s;
    }

    private static <T> Pair<CharSequence, List<CharSequence>> getComponentInfo(
            T[] componentArray, AttributeCallback<T> callback, String title) {
        final List<CharSequence> listExported = new ArrayList<>();
        final List<CharSequence> listNormal = new ArrayList<>();

        if (componentArray == null) {
            return Pair.create(getSubTitle(title, 0), listExported);
        }

        for (T info : componentArray) {
            String name = callback.getName(info);
            if (callback.exported(info)) {
                SpannableString s = new SpannableString(name);
                s.setSpan(new ForegroundColorSpan(Color.RED), 0, s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                listExported.add(s);
            } else {
                listNormal.add(name);
            }
        }

        Comparator<CharSequence> comparator = new Comparator<CharSequence>() {
            @Override
            public int compare(CharSequence lhs, CharSequence rhs) {
                return lhs.toString().compareTo(rhs.toString());
            }
        };
        Collections.sort(listExported, comparator);
        Collections.sort(listNormal, comparator);
        listExported.addAll(listNormal);

        return Pair.create(getSubTitle(title, listExported.size()), listExported);
    }

    private static PackageInfo getPackageComponentInfo(@NonNull PackageManager pm, String packageName) {
        final int flags = PackageManager.GET_ACTIVITIES | PackageManager.GET_INTENT_FILTERS
                | PackageManager.GET_SERVICES | PackageManager.GET_RECEIVERS
                | PackageManager.GET_PROVIDERS | PackageManager.GET_URI_PERMISSION_PATTERNS;
        try {
            return pm.getPackageInfo(packageName, flags);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
