package com.zisxks.droiddemo.fragment.misc.app.detail;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import run.yang.common.util.basic.UnitTool;
import run.yang.common.util.view.PaddingHelper;

/**
 * Created by Tim Yang on 2016-04-05 17:31.
 */
public class AppDetailedInfoAdapter extends BaseExpandableListAdapter {

    private List<Pair<CharSequence, List<CharSequence>>> mData;
    private int mMaxChildSegmentSizeBits;

    public void setData(@NonNull List<Pair<CharSequence, List<CharSequence>>> data) {
        mData = data;
        mMaxChildSegmentSizeBits = calculateChildSegmentSizeOffsetBits(data);
    }

    private static int calculateChildSegmentSizeOffsetBits(@NonNull List<Pair<CharSequence, List<CharSequence>>> data) {
        long maxChildCount = 0;
        for (Pair<CharSequence, List<CharSequence>> pair : data) {
            maxChildCount = Math.max(pair.second.size(), maxChildCount);
        }
        int zeros = Long.numberOfLeadingZeros(maxChildCount);
        if (zeros > 0) {
            zeros -= 1;
        }
        return Long.SIZE - zeros;
    }

    @Override
    public int getGroupCount() {
        return mData.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mData.get(groupPosition).second.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mData.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mData.get(groupPosition).second.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return ((long) groupPosition) << mMaxChildSegmentSizeBits + childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        TextView view;
        if (convertView instanceof TextView) {
            view = (TextView) convertView;
        } else {
            view = new TextView(parent.getContext());
            view.setSingleLine();
            view.setGravity(Gravity.CENTER_VERTICAL);
            PaddingHelper.with(view).vertical(UnitTool.dp2px_i(view.getContext(), 15)).done();
        }
        view.setText(mData.get(groupPosition).first);

        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        TextView view;
        if (convertView instanceof TextView) {
            view = (TextView) convertView;
        } else {
            view = new TextView(parent.getContext());
            view.setGravity(Gravity.CENTER_VERTICAL);
        }
        view.setText(mData.get(groupPosition).second.get(childPosition));
        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
