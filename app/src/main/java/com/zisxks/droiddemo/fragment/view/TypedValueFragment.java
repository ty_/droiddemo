package com.zisxks.droiddemo.fragment.view;


import android.os.Bundle;
import android.text.Editable;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.zisxks.droiddemo.R;

import run.yang.common.ui.simplified.SimpleTextWatcher;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.basic.NumberTool;
import run.yang.common.util.view.V;

public class TypedValueFragment extends BaseFragment implements View.OnClickListener {

    private static final SparseArray<String> UNIT_TYPES = new SparseArray<>(10);

    static {
        UNIT_TYPES.append(TypedValue.COMPLEX_UNIT_DIP, "DIP(Device Independent Pixel");
        UNIT_TYPES.append(TypedValue.COMPLEX_UNIT_SP, "SP(Scaled Pixel)");
        UNIT_TYPES.append(TypedValue.COMPLEX_UNIT_PX, "PX(Pixel)");
        UNIT_TYPES.append(TypedValue.COMPLEX_UNIT_PT, "PT(Point)");
        UNIT_TYPES.append(TypedValue.COMPLEX_UNIT_MM, "MM(Millimeter)");
        UNIT_TYPES.append(TypedValue.COMPLEX_UNIT_IN, "IN(Inch)");
    }

    private TextView mConvertResultTextView;
    private EditText mValueToConvertEditText;
    private Button mSelectUnitButton;

    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_typed_value, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSelectUnitButton = V.findView(view, R.id.typed_value_btn_select_unit);
        setSelectUnitButton(UNIT_TYPES.indexOfKey(TypedValue.COMPLEX_UNIT_DIP));
        mSelectUnitButton.setOnClickListener(this);
        this.registerForContextMenu(mSelectUnitButton);

        mConvertResultTextView = V.findView(view, R.id.typed_value_tv_convert_result);

        mValueToConvertEditText = V.findView(view, R.id.typed_value_btn_value_to_convert);
        mValueToConvertEditText.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                convertUnit();
            }
        });
    }

    @Override
    public void onDestroyView() {
        this.unregisterForContextMenu(mSelectUnitButton);
        super.onDestroyView();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        switch (v.getId()) {
            case R.id.typed_value_btn_select_unit:
                createSelectUnitContextMenu(menu);
                break;

            default:
                super.onCreateContextMenu(menu, v, menuInfo);
        }
    }

    private void createSelectUnitContextMenu(ContextMenu menu) {
        menu.setHeaderTitle("Select Input Unit");

        final int size = UNIT_TYPES.size();
        for (int index = 0; index < size; index++) {
            menu.add(Menu.NONE, UNIT_TYPES.keyAt(index), Menu.NONE, UNIT_TYPES.valueAt(index));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final int indexOfKey = UNIT_TYPES.indexOfKey(item.getItemId());
        if (indexOfKey >= 0) {
            setSelectUnitButton(indexOfKey);
            convertUnit();
            return true;
        }
        return super.onContextItemSelected(item);
    }

    private void setSelectUnitButton(int indexOfKey) {
        mSelectUnitButton.setTag(indexOfKey);
        mSelectUnitButton.setText(UNIT_TYPES.valueAt(indexOfKey));
    }

    private void convertUnit() {
        final String value = mValueToConvertEditText.getText().toString().trim();
        float floatValue = NumberTool.parseFloat(value);
        final DisplayMetrics displayMetrics = mContextActivity.getResources().getDisplayMetrics();
        final int selectedUnit = (int) mSelectUnitButton.getTag();

        float dimension = TypedValue.applyDimension(selectedUnit, floatValue, displayMetrics);

        mConvertResultTextView.setText(String.valueOf(dimension));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.typed_value_btn_select_unit:
                mSelectUnitButton.showContextMenu();
                break;

            default:
                break;
        }
    }
}
