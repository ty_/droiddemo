package com.zisxks.droiddemo.fragment.misc.sysdir;

/**
 * Created by Yang Tianmei on 2015-09-04 16:14.
 */
class SystemDirectoryBean {
    public static SystemDirectoryBean create(CharSequence methodName, CharSequence methodResult) {
        final SystemDirectoryBean bean = new SystemDirectoryBean();
        bean.methodName = methodName;
        bean.methodResult = methodResult;
        return bean;
    }

    public CharSequence methodName;
    public CharSequence methodResult;

    @Override
    public String toString() {
        return "SystemDirectoryBean{" +
                "methodName=" + methodName +
                ", methodResult=" + methodResult +
                '}';
    }
}
