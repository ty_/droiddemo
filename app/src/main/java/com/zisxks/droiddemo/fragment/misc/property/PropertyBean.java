package com.zisxks.droiddemo.fragment.misc.property;

import androidx.annotation.NonNull;

/**
 * Created by Yang Tianmei on 2015-09-09 21:20.
 */
public class PropertyBean implements Comparable {
    public String propertyName;
    public String propertyValue;

    @Override
    public int compareTo(@NonNull final Object another) {
        PropertyBean a = (PropertyBean) another;
        return this.propertyName.compareTo(a.propertyName);
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof PropertyBean) {
            PropertyBean other = (PropertyBean) o;
            return other.propertyName.equals(propertyName);
        }
        return false;
    }

    @Override
    public String toString() {
        return "PropertyBean{" +
                "propertyName='" + propertyName + '\'' +
                ", propertyValue='" + propertyValue + '\'' +
                '}';
    }
}
