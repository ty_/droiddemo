package com.zisxks.droiddemo.fragment.lifecycle.tabhost;

import android.os.Bundle;

import com.zisxks.droiddemo.R;
import com.zisxks.droiddemo.fragment.lifecycle.LifecyclePagerItemFragment;

import androidx.fragment.app.FragmentTabHost;
import run.yang.common.ui.template.BaseActivity;

public class TabHostFragmentLifecycleActivity extends BaseActivity {

    private static final int TABS_COUNT = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_host_fragment_lifecycle);

        FragmentTabHost tabHost = findView(android.R.id.tabhost);
        tabHost.setup(this, getSupportFragmentManager());

        for (int i = 0; i < TABS_COUNT; i++) {
            final Bundle arguments = LifecyclePagerItemFragment.getArguments(i);
            final String tag = "Tab " + i;
            tabHost.addTab(tabHost.newTabSpec(tag).setIndicator(tag), LifecyclePagerItemFragment.class, arguments);
        }
    }
}
