package com.zisxks.droiddemo.fragment.system.permission;

import android.Manifest;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Pair;

import com.zisxks.droiddemo.context.App;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.AppOpsManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;
import run.yang.common.util.android.Version;
import run.yang.common.util.log.Logg;

/**
 * Created by Yang Tianmei on 2015-09-08 17:13.
 */
public class PermissionHelper {
    private static final String TAG = PermissionHelper.class.getSimpleName();

    static List<Pair<String, String>> getAllSystemPermissions() {
        final Class<Manifest.permission> permissionClass = Manifest.permission.class;
        final Field[] fields = permissionClass.getFields();
        final List<Pair<String, String>> result = new ArrayList<>(fields.length);

        for (Field field : fields) {
            if (isPermissionConstField(field)) {
                Pair<String, String> pair = getFieldValue(permissionClass, field);
                if (pair != null) {
                    result.add(pair);
                }
            }
        }

        return result;
    }

    static List<Pair<String, String>> getOpStrs() {
        try {
            final Class<?> aClass = Class.forName("android.app.AppOpsManager");
            final Field[] fields = aClass.getFields();
            final int initialSize = fields.length > 10 ? fields.length / 2 : 10;
            final List<Pair<String, String>> result = new ArrayList<>(initialSize);

            for (Field field : fields) {
                if (isOpStrField(field)) {
                    final Pair<String, String> pair = getFieldValue(aClass, field);
                    if (pair != null) {
                        result.add(pair);
                    }
                }
            }
            return result;
        } catch (ClassNotFoundException e) {
            Logg.d(TAG, e.getMessage());
            return new ArrayList<>();
        }
    }

    private static boolean isOpStrField(final Field field) {
        final String name = field.getName();
        if (!name.startsWith("OPSTR_")) {
            return false;
        }

        final int modifiers = field.getModifiers();
        final int requiredModifiers = Modifier.PUBLIC | Modifier.STATIC | Modifier.FINAL;

        return (modifiers & requiredModifiers) == requiredModifiers;
    }

    @Nullable

    private static Pair<String, String> getFieldValue(@NonNull Class<?> clazz, @NonNull Field field) {
        try {
            String value = String.valueOf(field.get(clazz));
            String name = field.getName();
            if (field.getAnnotation(Deprecated.class) != null) {
                name += "(Deprecated)";
            }
            return Pair.create(name, value);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static boolean isPermissionConstField(final Field field) {
        final int modifiers = field.getModifiers();

        final int requiredModifiers = Modifier.PUBLIC | Modifier.STATIC | Modifier.FINAL;
        if ((modifiers & requiredModifiers) != requiredModifiers) {
            return false;
        }

        return String.class.equals(field.getType());
    }

    static SpannableStringBuilder testPermission(String permission) {
        final Context context = App.context();
        final SpannableStringBuilder sb = new SpannableStringBuilder();

        sb.append("Permission: ").append(permission);
        appendDelimiter(sb);

        sb.append("ContextCompat.checkSelfPermission(v4):\n");
        int checkPermission = ContextCompat.checkSelfPermission(context, permission);
        final String resultString = contextCompatCheckSelfPermissionToString(checkPermission);
        spanString(sb, resultString, getResultSpan());
        appendDelimiter(sb);

        final int myUid = android.os.Process.myUid();
        final int myPid = android.os.Process.myPid();
        final String myPackageName = context.getPackageName();

        sb.append("PermissionChecker.checkPermission(v4):\n");
        try {
            int permissionResult = PermissionChecker.checkPermission(context, permission, myPid, myUid, myPackageName);
            spanString(sb, permissionResultToString(permissionResult), getResultSpan());
        } catch (Exception e) {
            spanString(sb, "Error: " + e.getMessage(), getResultSpan());
        }
        appendDelimiter(sb);

        sb.append("AppOpsManagerCompat.noteOp(v4):\n");
        final String opStrCompat = AppOpsManagerCompat.permissionToOp(permission);
        if (!TextUtils.isEmpty(opStrCompat)) {
            try {
                int noteOpCompat = AppOpsManagerCompat.noteOp(context, opStrCompat, myUid, myPackageName);
                spanString(sb, noteOpCompatResultToString(noteOpCompat), getResultSpan());
            } catch (Exception e) {
                spanString(sb, "AppOpsManagerCompat.noteOp: " + e.getMessage(), getResultSpan());
            }
        } else {
            spanString(sb, "AppOpsManagerCompat.permissionToOp(permission) return null ", getResultSpan());
        }

        return sb;
    }

    private static Object getResultSpan() {
        return new ForegroundColorSpan(Color.RED);
    }

    private static void appendDelimiter(SpannableStringBuilder sb) {
        sb.append("\n\n");
    }

    public static void spanString(@NonNull SpannableStringBuilder sb, @NonNull String text, Object what) {
        int start = sb.length();
        sb.append(text);
        sb.setSpan(what, start, sb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    private static String permissionResultToString(final int permissionResult) {
        switch (permissionResult) {
            case PermissionChecker.PERMISSION_GRANTED:
                return "granted";
            case PermissionChecker.PERMISSION_DENIED:
                return "denied";
            case PermissionChecker.PERMISSION_DENIED_APP_OP:
                return "denied_app_op";
            default:
                return "not recognized: " + permissionResult;
        }
    }

    private static String contextCompatCheckSelfPermissionToString(final int checkPermission) {
        switch (checkPermission) {
            case PackageManager.PERMISSION_GRANTED:
                return "granted";
            case PackageManager.PERMISSION_DENIED:
                return "denied";
            default:
                return "not recognized: " + checkPermission;
        }
    }

    private static String noteOpCompatResultToString(final int noteOp) {
        switch (noteOp) {
            case AppOpsManagerCompat.MODE_ALLOWED:
                return "allowed";
            case AppOpsManagerCompat.MODE_DEFAULT:
                return "default";
            case AppOpsManagerCompat.MODE_IGNORED:
                return "ignored";
            default:
                return "not recognized: " + noteOp;
        }
    }

    static SpannableStringBuilder testOpStr(@NonNull String opStr) {
        final Context context = App.context();
        final SpannableStringBuilder sb = new SpannableStringBuilder();

        if (Version.atLeast(Version.V19_44)) {
            final int myUid = android.os.Process.myUid();
            final String myPackageName = context.getPackageName();

            sb.append("AppOpsManager.noteOp(v19):\n");
            final AppOpsManager appOpsManager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
            try {
                int noteOp = appOpsManager.noteOp(opStr, myUid, myPackageName);
                spanString(sb, noteOpResultToString(noteOp), getResultSpan());
            } catch (Exception e) {
                spanString(sb, "AppOpsManager.noteOp: " + e.getMessage(), getResultSpan());
            }
        } else {
            sb.append("AppOpsManager is only possible on Android 4.4(API 19) and above\nYour system version is ")
                    .append(Build.VERSION.RELEASE).append("(API ").append(String.valueOf(Build.VERSION.SDK_INT)).append(")");
        }
        return sb;
    }

    private static String noteOpResultToString(final int noteOp) {
        switch (noteOp) {
            case AppOpsManager.MODE_ALLOWED:
                return "allowed";
            case AppOpsManager.MODE_IGNORED:
                return "ignored";
            case AppOpsManager.MODE_ERRORED:
                return "errored";
            case AppOpsManager.MODE_DEFAULT:
                return "default";
            default:
                return "not recognized: " + noteOp;
        }
    }
}
