package com.zisxks.droiddemo.fragment.misc;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Telephony;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zisxks.droiddemo.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.android.Version;
import run.yang.common.util.util.SafeSpannableStringBuilder;
import run.yang.common.util.view.V;

public class ResolveIntentFragment extends BaseFragment {

    @Override
    public View onCreateViewImpl(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_resolve_intent, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        super.onViewCreated(view, savedInstanceState);

        final SafeSpannableStringBuilder stringBuilder = new SafeSpannableStringBuilder();

        resolveSmsIntent(stringBuilder);
        appendSectionDelimeter(stringBuilder);

        resolveContactsIntent(stringBuilder);
        appendSectionDelimeter(stringBuilder);

        resolveContactsIntent2(stringBuilder);
        appendSectionDelimeter(stringBuilder);

        resolveImageIntent(stringBuilder);
        appendSectionDelimeter(stringBuilder);

        resolveLauncherApps(stringBuilder);
        appendSectionDelimeter(stringBuilder);

        final TextView tv = V.findView(view, R.id.text_description);
        tv.setText(stringBuilder);
    }

    private void resolveLauncherApps(@NonNull SpannableStringBuilder stringBuilder) {
        stringBuilder.append("Launcher\n");

        Intent launcherIntent = new Intent(Intent.ACTION_MAIN);
        launcherIntent.addCategory(Intent.CATEGORY_HOME);

        queryAndGetPackageInfo(stringBuilder, launcherIntent);

        final PackageManager pm = mContextActivity.getPackageManager();
        ResolveInfo resolveInfo = pm.resolveActivity(launcherIntent, PackageManager.MATCH_DEFAULT_ONLY);
        stringBuilder.append("use resolveActivity method\n");
        addPackageInfoItem(stringBuilder, pm, resolveInfo);
    }

    private static void appendSectionDelimeter(@NonNull SpannableStringBuilder stringBuilder) {
        stringBuilder.append("\n------------------------------\n");
    }

    private void resolveSmsIntent(@NonNull SpannableStringBuilder stringBuilder) {
        stringBuilder.append("Sms/MMS:\n");

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setType("vnd.android-dir/mms-sms".toLowerCase());

        queryAndGetPackageInfo(stringBuilder, intent);

        if (Version.atLeast(Version.V19_44)) {
            String defaultSmsPackage = Telephony.Sms.getDefaultSmsPackage(mContextActivity);
            stringBuilder.append("default: ").append(defaultSmsPackage);
        }
    }

    private void resolveContactsIntent(@NonNull SpannableStringBuilder stringBuilder) {
        stringBuilder.append("Contacts(tel://):\n");

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addCategory(Intent.CATEGORY_BROWSABLE);
        intent.setData(Uri.parse("tel://"));

        queryAndGetPackageInfo(stringBuilder, intent);
    }

    private void resolveContactsIntent2(@NonNull SpannableStringBuilder stringBuilder) {
        stringBuilder.append("Contacts(CATEGORY_APP_CONTACTS):\n");

        if (Version.atLeast(Version.V15_403)) {
            Intent intent = new Intent();
            intent.addCategory(Intent.CATEGORY_APP_CONTACTS);
            queryAndGetPackageInfo(stringBuilder, intent);
        } else {
            stringBuilder.append("available on API 15+");
        }
    }

    private void resolveImageIntent(@NonNull SpannableStringBuilder stringBuilder) {
        stringBuilder.append("Photo Gallery(ACTION_PICK):\n");
        Intent pickIntent = new Intent(Intent.ACTION_PICK);
        pickIntent.addCategory(Intent.CATEGORY_DEFAULT);
        pickIntent.setDataAndType(Uri.EMPTY, "vnd.android.cursor.dir/image");
        queryAndGetPackageInfo(stringBuilder, pickIntent);

        stringBuilder.append("\nPhoto Gallery(ACTION_VIEW):\n");
        Intent viewIntent = new Intent(Intent.ACTION_VIEW);
        viewIntent.addCategory(Intent.CATEGORY_DEFAULT);
        viewIntent.setDataAndType(Uri.EMPTY, "vnd.android.cursor.dir/image");
        queryAndGetPackageInfo(stringBuilder, viewIntent);

        stringBuilder.append("\nPhoto Gallery(CATEGORY_APP_GALLERY):\n");
        if (Version.atLeast(Version.V15_403)) {
            Intent galleryIntent = Intent.makeMainSelectorActivity(Intent.ACTION_MAIN, Intent.CATEGORY_APP_GALLERY);
            queryAndGetPackageInfo(stringBuilder, galleryIntent);
        } else {
            stringBuilder.append("available on API 15+");
        }
    }

    private void queryAndGetPackageInfo(@NonNull SpannableStringBuilder stringBuilder, @NonNull Intent intent) {
        final PackageManager pm = mContextActivity.getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

        for (ResolveInfo resolveInfo : resolveInfos) {
            addPackageInfoItem(stringBuilder, pm, resolveInfo);
        }
    }

    private void addPackageInfoItem(@NonNull SpannableStringBuilder stringBuilder, @NonNull PackageManager pm, @Nullable ResolveInfo resolveInfo) {
        if (resolveInfo != null && resolveInfo.activityInfo != null) {
            final String packageName = resolveInfo.activityInfo.packageName;
            stringBuilder.append(packageName);

            CharSequence label1 = null;
            if (resolveInfo.activityInfo.applicationInfo != null) {
                label1 = pm.getApplicationLabel(resolveInfo.activityInfo.applicationInfo);
            }
            CharSequence label2 = resolveInfo.loadLabel(pm);
            CharSequence label;
            if (!TextUtils.isEmpty(label1) && TextUtils.equals(label1, label2)) {
                label = label1;
            } else {
                label = label1 + ":" + label2;
            }

            final int start = stringBuilder.length();
            stringBuilder.append("(").append(label).append(")\n");
            stringBuilder.setSpan(new ForegroundColorSpan(Color.BLUE), start, stringBuilder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            stringBuilder.append("null\n");
        }
    }
}
