package com.zisxks.droiddemo.fragment.misc;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.zisxks.droiddemo.R;

import androidx.annotation.NonNull;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.view.V;

public class LevelListFragment extends BaseFragment implements View.OnClickListener {

    private static final int MIN_IMAGE_LEVEL = 1;
    private static final int MAX_IMAGE_LEVEL = 20;

    private ImageView mLevelImageView;
    private TextView mLevelValueTextView;

    private int mImageLevelValue = 1;

    @Override
    public View onCreateViewImpl(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_level_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLevelImageView = V.findView(view, R.id.iv_level_image_view);
        mLevelValueTextView = V.findView(view, R.id.tv_level_value);

        Button increaseButton = (Button) view.findViewById(R.id.btn_increase_level);
        Button decreaseButton = (Button) view.findViewById(R.id.btn_decrease_level);

        setImageLevel(mImageLevelValue);
        increaseButton.setOnClickListener(this);
        decreaseButton.setOnClickListener(this);
    }

    @Override
    public void onClick(@NonNull View v) {
        switch (v.getId()) {
            case R.id.btn_increase_level:
                increaseImageLevel();
                break;

            case R.id.btn_decrease_level:
                decreaseImageLevel();
                break;
        }
    }

    private void decreaseImageLevel() {
        if (mImageLevelValue > MIN_IMAGE_LEVEL) {
            mImageLevelValue--;
            setImageLevel(mImageLevelValue);
        }
    }

    private void increaseImageLevel() {
        if (mImageLevelValue < MAX_IMAGE_LEVEL) {
            mImageLevelValue++;
            setImageLevel(mImageLevelValue);
        }
    }

    private void setImageLevel(int newLevel) {
        mLevelImageView.setImageLevel(newLevel);
        mLevelValueTextView.setText(String.valueOf(newLevel));
    }

}
