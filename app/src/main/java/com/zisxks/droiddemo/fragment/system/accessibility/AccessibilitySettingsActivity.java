package com.zisxks.droiddemo.fragment.system.accessibility;

import android.os.Bundle;

import com.zisxks.droiddemo.R;

import run.yang.common.ui.template.BaseActivity;

public class AccessibilitySettingsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accessibility_settings);
    }
}
