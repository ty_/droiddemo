package com.zisxks.droiddemo.fragment.misc.build_constant;

import android.os.Build;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import run.yang.common.ui.recycler.adapter.TitleContentPair;
import run.yang.common.util.basic.NumberTool;
import run.yang.common.util.basic.TextTool;
import run.yang.common.util.basic.TimeTool;
import run.yang.common.util.log.Logg;

/**
 * Created by Yang Tianmei on 2015-09-21.
 */
final class BuildConstantHelper {
    private static final String TAG = BuildConstantHelper.class.getSimpleName();

    @NonNull
    static List<TitleContentPair> getByReflection() {
        final List<TitleContentPair> list = new ArrayList<>();

        final Pattern expectedFieldNamePattern = Pattern.compile("^[A-Z_][A-Z_0-9]+$");

        Class<?> buildClass = Build.class;
        final String buildPrefix = buildClass.getSimpleName() + '.';
        getConstantField(list, expectedFieldNamePattern, buildClass, buildPrefix);

        prettifyBuildTime(list);

        Class<?> versionClass = Build.VERSION.class;
        final String versionPrefix = buildClass.getSimpleName() + '.' + versionClass.getSimpleName() + '.';
        getConstantField(list, expectedFieldNamePattern, versionClass, versionPrefix);

        return list;
    }

    private static void prettifyBuildTime(@NonNull List<TitleContentPair> list) {
        for (TitleContentPair bean : list) {
            if ("Build.TIME".equals(bean.title)) {
                final long fallback = -1;
                final long longValue = NumberTool.parseLong(String.valueOf(bean.content), fallback);
                if (longValue != fallback) {
                    bean.content = String.valueOf(bean.content) + '(' + TimeTool.formatFormalTime(longValue) + ')';
                }
                break;
            }
        }
    }

    private static void getConstantField(@NonNull List<TitleContentPair> list,
                                         @NonNull Pattern pattern,
                                         @NonNull Class<?> clazz,
                                         @NonNull String keyPrefix) {
        final Field[] fields = clazz.getFields();
        for (Field field : fields) {
            if (isExpectedConstantField(field, pattern)) {
                final TitleContentPair bean = new TitleContentPair();
                final boolean deprecated = field.getAnnotation(Deprecated.class) != null;
                if (deprecated) {
                    for (Annotation annotation : field.getAnnotations()) {
                        Logg.d(TAG, annotation.toString());
                    }
                }
                bean.title = keyPrefix + (deprecated ? field.getName() + "(Deprecated)" : field.getName());
                Object value = null;
                try {
                    value = field.get(null);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                bean.content = TextTool.toString(value);
                list.add(bean);
            }
        }
    }

    private static boolean isExpectedConstantField(@NonNull Field field, @NonNull Pattern pattern) {
        final int modifiers = Modifier.PUBLIC | Modifier.STATIC | Modifier.FINAL;
        final String fieldName = field.getName();
        return (field.getModifiers() & modifiers) == modifiers && pattern.matcher(fieldName).matches();
    }
}
