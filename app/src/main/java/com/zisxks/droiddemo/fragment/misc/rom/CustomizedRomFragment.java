package com.zisxks.droiddemo.fragment.misc.rom;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zisxks.droiddemo.R;

import androidx.annotation.NonNull;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.notif.ToastManager;
import run.yang.common.util.rom.MiuiTool;
import run.yang.common.util.view.V;

public class CustomizedRomFragment extends BaseFragment {

    @Override
    public View onCreateViewImpl(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_xiao_mi_miui, container, false);

        View openPermissionManagerButton = root.findViewById(R.id.miui_btn_open_miui_permission_manager);
        if (MiuiTool.isMiui()) {
            openPermissionManagerButton.setOnClickListener(v -> {
                boolean started = MiuiTool.startPermissionManager(mContextActivity, mContextActivity.getPackageName());
                if (!started) {
                    final StringBuilder sb = new StringBuilder("Cannot open permission manager, ");
                    if (!MiuiTool.isMiui()) {
                        sb.append(", it seems you do not have MIUI ROM");
                    }
                    ToastManager.show(sb.toString());
                }
            });
        } else {
            openPermissionManagerButton.setVisibility(View.GONE);
        }

        final TextView textView = V.findView(root, R.id.customized_rom_rom_description);
        textView.setText(CustomizedRomHelper.getRomDescription());

        return root;
    }
}
