package com.zisxks.droiddemo.fragment.system.notification;

import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.zisxks.droiddemo.R;
import com.zisxks.droiddemo.activity.entrance.main.MainActivity;
import com.zisxks.droiddemo.activity.fragmenthost.FragmentHostActivity;
import com.zisxks.droiddemo.activity.fragmenthost.FragmentListActivity;
import com.zisxks.droiddemo.app.noti.NotiChannelId;
import com.zisxks.droiddemo.constant.NotifyId;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.android.Version;
import run.yang.common.util.view.V;


@TargetApi(Version.V11_30)
public class BackStackBuilderFragment extends BaseFragment implements View.OnClickListener {

    public BackStackBuilderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_back_stack, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button createNotificationButton = V.findView(view, R.id.btn_create_notification);
        createNotificationButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_create_notification:
                postNotification();
                break;
        }
    }

    private void postNotification() {
        final TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContextActivity);

        Intent next = new Intent(mContextActivity, MainActivity.class);
        stackBuilder.addNextIntent(next);

        next = new Intent(mContextActivity, FragmentListActivity.class);
        next.putExtra(FragmentListActivity.sTagCategory, FragmentListActivity.CATEGORY_SYSTEM);
        stackBuilder.addNextIntent(next);

        next = new Intent(mContextActivity, FragmentHostActivity.class);
        next.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        next.putExtra(FragmentHostActivity.sKeyFragmentClass, BackStackBuilderFragment.class);
        next.putExtra(FragmentHostActivity.sKeyWindowTitle, "BackStackBuilder");
        stackBuilder.addNextIntent(next);

        final PendingIntent pendingIntent = stackBuilder.getPendingIntent(1, PendingIntent.FLAG_UPDATE_CURRENT);
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(mContextActivity, NotiChannelId.DEFAULT);
        builder.setContentIntent(pendingIntent);
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setContentTitle("BackStackBuilder test");
        builder.setContentText("Click here to back to app with BackStack");
        builder.setAutoCancel(true);

        final NotificationManager nm = (NotificationManager) mContextActivity.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(NotifyId.FUNC_BACKSTACK_NOTIFICATION, builder.build());
    }
}
