package com.zisxks.droiddemo.fragment.system.location;

import android.location.Location;
import android.os.SystemClock;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import androidx.core.util.TimeUtils;
import run.yang.common.util.android.Version;
import run.yang.common.util.basic.TimeTool;

/**
 * Created by Yang Tianmei on 2015-08-29 21:33.
 */
public class LocationTool {

    private static final String TAG = LocationTool.class.getSimpleName();

    private static final int LOCATION_EXPIRE_TIME_HOURS = 3;

    public static boolean isLocationExpired(@Nullable Location location) {
        if (location == null) {
            return true;
        }

        if (Version.atLeast(Version.V17_42)) {
            long diffNanos = SystemClock.elapsedRealtimeNanos() - location.getElapsedRealtimeNanos();
            if (diffNanos < 0 || diffNanos > TimeUnit.HOURS.toNanos(LOCATION_EXPIRE_TIME_HOURS)) {
                return true;
            }
        }

        final long diffMillis = System.currentTimeMillis() - location.getTime();
        return diffMillis < 0 || diffMillis > TimeUnit.HOURS.toMillis(LOCATION_EXPIRE_TIME_HOURS);
    }

    public static String toString(@Nullable Location location) {
        return toString(location, false);
    }

    public static String toPrettyString(@Nullable Location location) {
        return toString(location, true);
    }

    private static String toString(@Nullable Location location, boolean prettify) {
        if (location == null) {
            return "Location: null";
        }

        StringBuilder s = new StringBuilder();
        s.append(prettify ? "Provider: \t" : "Location[");
        s.append(location.getProvider());

        if (prettify) {
            s.append("\nLatitude: \t").append(String.format(Locale.US, "%.6f", location.getLatitude()));
            s.append("\nLongitude: \t").append(String.format(Locale.US, "%.6f", location.getLongitude()));
        } else {
            s.append(String.format(Locale.US, " %.6f,%.6f", location.getLatitude(), location.getLongitude()));
        }

        if (prettify) s.append("\nAccuracy: \t");
        if (location.hasAccuracy()) {
            s.append(String.format(Locale.US, " acc=%.0f", location.getAccuracy()));
        } else {
            s.append(prettify ? "?" : " acc=???");
        }
        if (prettify) s.append("\nTime: \t");
        long time = location.getTime();
        if (time == 0) {
            s.append(prettify ? "?" : " t=?!?");
        } else {
            s.append(prettify ? "" : " t=").append(TimeTool.formatFormalTime(time));
        }
        if (Version.atLeast(Version.V17_42)) {
            if (prettify) s.append("\nElapsedRealtimeNanos: \t");
            long elapsedRealtimeNanos = location.getElapsedRealtimeNanos();
            if (elapsedRealtimeNanos == 0) {
                s.append(prettify ? "?" : " et=?!?");
            } else {
                s.append(prettify ? "" : " et=");
                TimeUtils.formatDuration(elapsedRealtimeNanos / 1000000L, s);
            }
        }
        if (location.hasAltitude()) {
            if (prettify) s.append("\nAltitude: \t");
            s.append(" alt=").append(location.getAltitude());
        }
        if (location.hasSpeed()) {
            if (prettify) s.append("\nSpeed: \t");
            s.append(" vel=").append(location.getSpeed());
        }
        if (location.hasBearing()) {
            if (prettify) s.append("\nBear: \t");
            s.append(" bear=").append(location.getBearing());
        }
        if (Version.atLeast(Version.V18_43)) {
            if (location.isFromMockProvider()) {
                if (prettify) {
                    s.append("\nMocked");
                } else {
                    s.append(" mock");
                }
            }
        }

        if (location.getExtras() != null) {
            if (prettify) s.append("\nExtra:\t");
            s.append(" {").append(location.getExtras()).append('}');
        }
        if (!prettify) s.append(']');
        return s.toString();
    }
}
