package com.zisxks.droiddemo.fragment.misc.app;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zisxks.droiddemo.R;
import com.zisxks.droiddemo.context.App;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import run.yang.common.ui.recycler.adapter.BaseRecyclerViewAdapter;
import run.yang.common.util.android.Version;
import run.yang.common.util.basic.TextTool;
import run.yang.common.util.compatibility.ClipboardCompat;
import run.yang.common.util.log.Logg;
import run.yang.common.util.view.V;

/**
 * Created by Yang Tianmei on 2015-08-13 10:40.
 */
public class AppInformationAdapter extends BaseRecyclerViewAdapter<SimpleAppInfoBean, AppInformationAdapter.ViewHolder> {

    private static final String TAG = Logg.getTag(AppInformationAdapter.class);

    interface ActionCallback {
        void viewPackageDetail(String packageName);
    }

    private Handler mHandler;
    private final Collator mCollator = App.context().newCollatorWithUserSelectedLocale();
    private Comparator<SimpleAppInfoBean> mComparator;
    private ActionCallback mActionCallback;

    private final List<SimpleAppInfoBean> mAllItems = new ArrayList<>();
    private String mFilterText;

    private View.OnClickListener mOnAppIconClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ActionCallback callback = mActionCallback;
            if (callback == null) return;

            Object tag = v.getTag();
            if (tag instanceof SimpleAppInfoBean) {
                SimpleAppInfoBean bean = (SimpleAppInfoBean) tag;
                callback.viewPackageDetail(bean.packageName);
            }
        }
    };

    public AppInformationAdapter(@NonNull final Context context, @Nullable ActionCallback callback) {
        super(context, new ArrayList<>(100));

        mActionCallback = callback;

        setItemLayoutResId(R.layout.layout_list_item_app_detail);

        mHandler = new Handler(Looper.getMainLooper());

        final AppInformationLoaderThread.LoadProcessListener loadProcessListener = new AppInformationLoaderThread.LoadProcessListener() {
            @Override
            public void onMoreAppInfoLoaded(final List<SimpleAppInfoBean> newlyLoadedAppInfoList) {
                mHandler.post(() -> {
                    mAllItems.addAll(newlyLoadedAppInfoList);
                    sortAppList(mAllItems);

                    if (!TextUtils.isEmpty(mFilterText)) {
                        doFilter(mFilterText);
                    } else {
                        mDataList.clear();
                        mDataList.addAll(mAllItems);
                    }
                    AppInformationAdapter.this.notifyDataSetChanged();
                });
            }

            @Override
            public void onLoadComplete() {
            }
        };

        new AppInformationLoaderThread(context, loadProcessListener).start();
        this.setHasStableIds(true);
    }

    private void sortAppList(@NonNull List<SimpleAppInfoBean> items) {
        if (mComparator == null) {
            mComparator = (lhs, rhs) -> mCollator.compare(lhs.label, rhs.label);
        }

        Collections.sort(items, mComparator);
    }

    @Override
    protected void onBindView(ViewHolder holder, SimpleAppInfoBean item, int position) {
        holder.titleTextView.setText(AppInformationHelper.getListItemTitle(item, position));
        holder.detailTextView.setText(AppInformationHelper.getListItemDetail(item));
        holder.appIconImageView.setImageDrawable(item.appIcon);

        if (mActionCallback != null) {
            holder.appIconImageView.setTag(item);
        }
    }

    @Override
    protected ViewHolder newViewHolder(View rootView) {
        return new ViewHolder(rootView, mOnAppIconClickListener);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * @param filterText if null or "", show original text
     */
    @UiThread
    public void filter(@Nullable String filterText) {
        if (TextUtils.isEmpty(filterText)) {
            mDataList.clear();
            mDataList.addAll(mAllItems);
            notifyDataSetChanged();
            return;
        }

        filterText = filterText.trim();
        if (filterText.equals(mFilterText)) {
            return;
        }

        mFilterText = filterText.toLowerCase();

        doFilter(mFilterText);
    }

    @UiThread
    private void doFilter(@NonNull String keyword) {
        final String[] keywords = keyword.split("\\s+");
        mDataList.clear();

        Logg.d(TAG, "do filter: " + Arrays.toString(keywords));

        for (SimpleAppInfoBean bean : mAllItems) {
            if (TextTool.matchSequentially(String.valueOf(bean.label).toLowerCase(), keywords)
                    || TextTool.matchSequentially(bean.packageName.toLowerCase(), keywords)) {
                mDataList.add(bean);
            }
        }
        notifyDataSetChanged();


    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView titleTextView;
        public TextView detailTextView;
        public ImageView appIconImageView;

        public ViewHolder(final View itemView, @Nullable View.OnClickListener onAppIconClickListener) {
            super(itemView);

            titleTextView = V.findView(itemView, R.id.text1);
            detailTextView = V.findView(itemView, R.id.text2);
            appIconImageView = V.findView(itemView, R.id.app_info_app_icon);

            if (Version.atMost(Version.V10_233)) {
                titleTextView.setOnClickListener(ClipboardCompat.sClickToCopyTextListener);
                detailTextView.setOnClickListener(ClipboardCompat.sClickToCopyTextListener);
            }

            if (onAppIconClickListener != null) {
                appIconImageView.setOnClickListener(onAppIconClickListener);
            }
        }
    }
}
