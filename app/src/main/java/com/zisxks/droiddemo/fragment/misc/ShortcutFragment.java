package com.zisxks.droiddemo.fragment.misc;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zisxks.droiddemo.R;
import com.zisxks.droiddemo.activity.entrance.main.MainActivity;

import run.yang.common.ui.template.BaseFragment;


public class ShortcutFragment extends BaseFragment implements View.OnClickListener {

    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_shortcut, container, false);

        root.findViewById(R.id.btnCreateShortcut).setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.btnCreateShortcut:
                createShortWithInstallShortcut();
                break;

            default:
                break;
        }
    }

    private void createShortWithInstallShortcut() {
        final Intent shortcutIntent = new Intent(mContextActivity, MainActivity.class);

        final Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
        intent.putExtra("duplicate", false);
        Intent.ShortcutIconResource shortcutIconResource = Intent.ShortcutIconResource.fromContext(
                mContextActivity,
                R.drawable.material_codelab_avatar);
        intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, shortcutIconResource);

        mContextActivity.sendBroadcast(intent);
    }
}
