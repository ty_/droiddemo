package com.zisxks.droiddemo.fragment.system.locale;

import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.ui.recycler.adapter.TitleContentPair;
import run.yang.common.util.basic.TextTool;
import run.yang.common.util.exception.ExceptionTool;
import run.yang.common.util.log.Logg;

/**
 * Created by Yang Tianmei on 2016-03-23 16:43.
 */
class LocaleExplorerHelper {

    private static final String TAG = Logg.getTag(LocaleExplorerHelper.class);

    @Nullable
    static List<TitleContentPair> getLocaleDetail(@Nullable Locale locale, @Nullable Locale localizedLocale) {
        if (locale == null) {
            return null;
        }

        List<TitleContentPair> list = new ArrayList<>();
        for (Method method : Locale.class.getMethods()) {
            Class<?>[] parameterTypes = method.getParameterTypes();
            Class<?> returnType = method.getReturnType();
            if (String.class.equals(returnType)) {
                if (parameterTypes == null || parameterTypes.length == 0) {
                    addEmptyParameterMethod(list, locale, method);
                } else if (parameterTypes.length == 1 && parameterTypes[0].equals(Locale.class)) {
                    if (localizedLocale != null)
                        addLocaleParameterMethod(list, locale, method, localizedLocale);
                } else {
                    Logg.d(TAG, "unknown method signature: " + method);
                }
            } else if (Set.class.equals(returnType)) {
                if (parameterTypes == null || parameterTypes.length == 0) {
                    addEmptyParameterMethod(list, locale, method);
                }
            }
        }

        return list;
    }

    private static CharSequence getErrorContent(Throwable e) {
        SpannableStringBuilder sb = new SpannableStringBuilder("Error: ");
        sb.setSpan(new ForegroundColorSpan(Color.RED), 0, sb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        String errorDescription = ExceptionTool.getShortDescription(ExceptionTool.getRootCause(e));
        sb.append(errorDescription);
        return sb;
    }

    private static void addEmptyParameterMethod(@NonNull List<TitleContentPair> list, @NonNull Locale locale, @NonNull Method method) {
        String title = "locale." + method.getName() + "()";
        CharSequence content;
        try {
            content = TextTool.toReadableString(method.invoke(locale));
        } catch (IllegalAccessException e) {
            content = getErrorContent(e);
        } catch (InvocationTargetException e) {
            content = getErrorContent(e);
        }
        list.add(TitleContentPair.create(title, content));
    }

    private static void addLocaleParameterMethod(
            @NonNull List<TitleContentPair> list,
            @NonNull Locale locale,
            @NonNull Method method,
            @NonNull Locale parameter) {
        String title = "locale." + method.getName() + "(Locale locale)";
        CharSequence content;
        try {
            content = TextTool.toReadableString(method.invoke(locale, parameter));
        } catch (IllegalAccessException e) {
            content = getErrorContent(e);
        } catch (InvocationTargetException e) {
            content = getErrorContent(e);
        }
        list.add(TitleContentPair.create(title, content));
    }
}
