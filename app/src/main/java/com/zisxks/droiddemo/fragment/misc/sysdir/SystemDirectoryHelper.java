package com.zisxks.droiddemo.fragment.misc.sysdir;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Environment;
import android.text.TextUtils;

import com.zisxks.droiddemo.context.App;

import java.io.File;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.ArrayMap;
import androidx.core.content.ContextCompat;
import run.yang.common.util.android.Version;

/**
 * Created by Yang Tianmei on 2015-08-10 10:39.
 */
class SystemDirectoryHelper {

    private static final Map<String, String> MEDIA_STATE_MAP;

    static {
        MEDIA_STATE_MAP = new ArrayMap<>();
        MEDIA_STATE_MAP.put(Environment.MEDIA_BAD_REMOVAL, "Environment.MEDIA_BAD_REMOVAL");
        MEDIA_STATE_MAP.put(Environment.MEDIA_CHECKING, "Environment.MEDIA_CHECKING");
        MEDIA_STATE_MAP.put(Environment.MEDIA_MOUNTED, "Environment.MEDIA_MOUNTED");
        MEDIA_STATE_MAP.put(Environment.MEDIA_MOUNTED_READ_ONLY, "Environment.MEDIA_MOUNTED_READ_ONLY");
        MEDIA_STATE_MAP.put(Environment.MEDIA_NOFS, "Environment.MEDIA_NOFS");
        MEDIA_STATE_MAP.put(Environment.MEDIA_REMOVED, "Environment.MEDIA_REMOVED");
        MEDIA_STATE_MAP.put(Environment.MEDIA_SHARED, "Environment.MEDIA_SHARED");
        MEDIA_STATE_MAP.put(Environment.MEDIA_UNMOUNTABLE, "Environment.MEDIA_UNMOUNTABLE");
        MEDIA_STATE_MAP.put(Environment.MEDIA_UNMOUNTED, "Environment.MEDIA_UNMOUNTED");
        if (Version.atLeast(Version.V19_44)) {
            MEDIA_STATE_MAP.put(Environment.MEDIA_UNKNOWN, "Environment.MEDIA_UNKNOWN");
        }
    }

    @NonNull
    static List<SystemDirectoryBean> getSystemDirectories(final Context contextActivity) {
        final List<SystemDirectoryBean> list = new ArrayList<>();
        final Context context = contextActivity.getApplicationContext();

        list.add(SystemDirectoryBean.create("Context.getCacheDir()",
                String.valueOf(context.getCacheDir())));
        list.add(SystemDirectoryBean.create("Context.getDir(\"myDir\", Context.MODE_PRIVATE)",
                String.valueOf(context.getDir("myDir", Context.MODE_PRIVATE))));
        list.add(SystemDirectoryBean.create("Context.getExternalCacheDir()",
                String.valueOf(context.getExternalCacheDir())));
        list.add(SystemDirectoryBean.create("Context.getFilesDir()",
                String.valueOf(context.getFilesDir())));
        if (Version.atLeast(Version.V11_30)) {
            list.add(SystemDirectoryBean.create("Context.getObbDir()",
                    String.valueOf(context.getObbDir())));
        }
        list.add(SystemDirectoryBean.create("Context.getExternalFilesDir(null)",
                String.valueOf(context.getExternalFilesDir(null))));
        list.add(SystemDirectoryBean.create("Context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)",
                String.valueOf(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES))));
        list.add(SystemDirectoryBean.create("Context.getFileStreamPath(\"log\")",
                String.valueOf(context.getFileStreamPath("log"))));

        list.add(SystemDirectoryBean.create("ContextCompat.getExternalCacheDirs(context)",
                describPathArray(ContextCompat.getExternalCacheDirs(context))));

        list.add(SystemDirectoryBean.create("ContextCompat.getExternalFilesDirs(context, Environment.DIRECTORY_DOWNLOADS)",
                describPathArray(ContextCompat.getExternalFilesDirs(context, Environment.DIRECTORY_DOWNLOADS))));

        list.add(SystemDirectoryBean.create("ContextCompat.getObbDirs(context)",
                describPathArray(ContextCompat.getObbDirs(context))));

        list.add(SystemDirectoryBean.create("Environment.getDataDirectory()",
                String.valueOf(Environment.getDataDirectory())));
        list.add(SystemDirectoryBean.create("Environment.getRootDirectory()",
                String.valueOf(Environment.getRootDirectory())));
        list.add(SystemDirectoryBean.create("Environment.getDownloadCacheDirectory()",
                String.valueOf(Environment.getDownloadCacheDirectory())));
        list.add(SystemDirectoryBean.create("Environment.getExternalStorageDirectory()",
                String.valueOf(Environment.getExternalStorageDirectory())));
        list.add(SystemDirectoryBean.create("Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)",
                String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES))));

        final String state = Environment.getExternalStorageState();
        String stateString = MEDIA_STATE_MAP.get(state);
        if (TextUtils.isEmpty(stateString)) {
            stateString = state;
        }
        list.add(SystemDirectoryBean.create("Environment.getExternalStorageState()", stateString));

        final ApplicationInfo appInfo = App.context().getApplicationInfo();
        list.add(SystemDirectoryBean.create("ApplicationInfo.className", String.valueOf(appInfo.className)));
        list.add(SystemDirectoryBean.create("ApplicationInfo.dataDir", String.valueOf(appInfo.dataDir)));
        list.add(SystemDirectoryBean.create("ApplicationInfo.nativeLibraryDir", String.valueOf(appInfo.nativeLibraryDir)));
        list.add(SystemDirectoryBean.create("ApplicationInfo.processName", String.valueOf(appInfo.processName)));
        list.add(SystemDirectoryBean.create("ApplicationInfo.publicSourceDir", String.valueOf(appInfo.publicSourceDir)));
        list.add(SystemDirectoryBean.create("ApplicationInfo.sourceDir", String.valueOf(appInfo.sourceDir)));
        list.add(SystemDirectoryBean.create("ApplicationInfo.sharedLibraryFiles",
                appInfo.sharedLibraryFiles != null ? TextUtils.join(", ", appInfo.sharedLibraryFiles) : "null"));

        if (Version.atLeast(Version.V21_50)) {
            list.add(SystemDirectoryBean.create("Context.getCodeCacheDir",
                    String.valueOf(context.getCodeCacheDir())));
            list.add(SystemDirectoryBean.create("Context.getNoBackupFilesDir",
                    String.valueOf(context.getNoBackupFilesDir())));
        }

        return list;
    }

    static void sortByMethodName(final List<SystemDirectoryBean> list) {
        final Collator collator = App.context().newCollatorWithUserSelectedLocale();

        Collections.sort(list, (lhs, rhs) -> collator.compare(lhs.methodName, rhs.methodName));
    }

    @NonNull
    private static String describPathArray(@Nullable File[] externalCacheDirs) {
        return Arrays.toString(externalCacheDirs);
    }
}
