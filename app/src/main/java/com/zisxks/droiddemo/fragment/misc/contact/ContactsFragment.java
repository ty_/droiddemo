package com.zisxks.droiddemo.fragment.misc.contact;


import android.Manifest;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zisxks.droiddemo.R;
import com.zisxks.droiddemo.constant.PermissionRequest;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import run.yang.common.ui.recycler.adapter.TitleContentAdapter;
import run.yang.common.ui.recycler.view.DividerLinearRecyclerView;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.android.Version;
import run.yang.common.util.component.PermissionTool;
import run.yang.common.util.notif.ToastManager;
import run.yang.common.util.view.V;

import static android.provider.ContactsContract.Contacts;


public class ContactsFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int CONTACTS_LOADER = 0;

    private static final String[] PROJECTION = new String[2];

    static {
        if (Version.atLeast(Version.V11_30)) {
            PROJECTION[0] = Contacts.DISPLAY_NAME_PRIMARY;
        } else {
            PROJECTION[0] = "display_name";
        }
        PROJECTION[1] = Contacts.HAS_PHONE_NUMBER;
    }

    private final int[] mColumnIndex = new int[PROJECTION.length];
    private TitleContentAdapter<SimpleContactBean> mAdapter;
    private final List<SimpleContactBean> mContactList = new ArrayList<>();

    private final Runnable mInitLoaderRunnable = () -> LoaderManager.getInstance(ContactsFragment.this).initLoader(CONTACTS_LOADER, null, ContactsFragment.this);

    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts, container, false);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final DividerLinearRecyclerView recyclerView = V.findView(view, R.id.contacts_recycle_view);
        recyclerView.setHasFixedSize(true);

        mAdapter = new TitleContentAdapter<SimpleContactBean>(mContextActivity, mContactList) {
            @Override
            protected void onBindView(ViewHolder holder, SimpleContactBean item, int position) {
                holder.title.setText(item.displayName);
                holder.content.setText(item.hasPhoneNumber == 0 ? "no number" : "has number");
            }
        };

        recyclerView.setAdapter(mAdapter);

        if (PermissionTool.checkReadContactPermission()) {
            mInitLoaderRunnable.run();
        } else {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PermissionRequest.CODE_READ_CONTACTS);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(final int id, final Bundle args) {
        switch (id) {
            case CONTACTS_LOADER:
                return new CursorLoader(mContextActivity, Contacts.CONTENT_URI, PROJECTION, null, null, null);

            default:
                break;
        }
        return null;
    }

    @Override
    public void onLoadFinished(final Loader<Cursor> loader, final Cursor data) {
        for (int i = 0; i < PROJECTION.length; i++) {
            mColumnIndex[i] = data.getColumnIndexOrThrow(PROJECTION[i]);
        }

        if (!data.moveToFirst()) {
            return;
        }

        mContactList.clear();
        do {
            SimpleContactBean bean = new SimpleContactBean();
            bean.displayName = data.getString(mColumnIndex[0]);
            bean.hasPhoneNumber = data.getInt(mColumnIndex[1]);
            mContactList.add(bean);
        } while (data.moveToNext());

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(final Loader<Cursor> loader) {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length == 0) {
            ToastManager.show(R.string.contacts_no_read_contacts_permission);
            return;
        }

        switch (requestCode) {
            case PermissionRequest.CODE_READ_CONTACTS:
                if (PermissionTool.granted(grantResults[0])) {
                    mInitLoaderRunnable.run();
                } else {
                    ToastManager.show(R.string.contacts_no_read_contacts_permission);
                }
                break;
        }
    }
}
