package com.zisxks.droiddemo.fragment.misc.time;

import android.content.Context;
import android.icu.util.ChineseCalendar;
import android.os.SystemClock;
import android.text.SpannableString;
import android.text.Spanned;

import com.zisxks.droiddemo.util.time.ChineseLunarCalendarHelper;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import run.yang.common.util.android.Version;
import run.yang.common.util.basic.TextTool;
import run.yang.common.util.basic.TimeTool;

/**
 * Created by Yang Tianmei on 2015-07-24 16:31.
 */
class SystemTimeHelper {

    interface TimeClockViewer {
        @NonNull
        Spanned getTimePresentation();

        @NonNull
        Spanned getClockName();
    }

    private static Spanned formatTime(long timeMillis, @Nullable String text) {
        return SpannableString.valueOf(String.valueOf(timeMillis) + " (" + text + ")");
    }

    @NonNull
    static List<TimeClockViewer> getTimeClocks(@NonNull Context context) {
        final List<TimeClockViewer> list = new ArrayList<>();

        list.add(new CurrentTimeMillis());
        list.add(new UpTimeMillis());
        list.add(new ElapsedRealtime());
        list.add(new CurrentThreadTimeMillis());
        if (Version.atLeast(Version.V24_70)) {
            list.add(new ChineseLunarClock());
        }

        return list;
    }

    private static class CurrentTimeMillis implements TimeClockViewer {

        private static final SpannableString CLOCK_NAME = SpannableString.valueOf("System.currentTimeMillis()");

        @NonNull
        @Override
        public Spanned getTimePresentation() {
            long currentTimeMillis = System.currentTimeMillis();
            return formatTime(currentTimeMillis, TimeTool.formatFormalTime(currentTimeMillis));
        }

        @NonNull
        @Override
        public Spanned getClockName() {
            return CLOCK_NAME;
        }
    }

    private static class UpTimeMillis implements TimeClockViewer {

        private static final SpannableString CLOCK_NAME = SpannableString.valueOf("SystemClock.uptimeMillis()\nnon-sleep uptime since boot");

        @NonNull
        @Override
        public Spanned getTimePresentation() {
            long upTimeMillis = SystemClock.uptimeMillis();
            return formatTime(upTimeMillis, TimeTool.formatElapsedTime(upTimeMillis));
        }

        @NonNull
        @Override
        public Spanned getClockName() {
            return CLOCK_NAME;
        }
    }

    private static class ElapsedRealtime implements TimeClockViewer {

        private static final SpannableString CLOCK_NAME = SpannableString.valueOf("SystemClock.elapsedRealtime()");

        @NonNull
        @Override
        public Spanned getTimePresentation() {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            return formatTime(elapsedRealtime, TimeTool.formatElapsedTime(elapsedRealtime));
        }

        @NonNull
        @Override
        public Spanned getClockName() {
            return CLOCK_NAME;
        }
    }

    private static class CurrentThreadTimeMillis implements TimeClockViewer {

        private static final SpannableString CLOCK_NAME = SpannableString.valueOf("SystemClock.currentThreadTimeMillis()");

        @NonNull
        @Override
        public Spanned getTimePresentation() {
            long threadTimeMillis = SystemClock.currentThreadTimeMillis();
            return formatTime(threadTimeMillis, TimeTool.formatElapsedTime(threadTimeMillis));
        }

        @NonNull
        @Override
        public Spanned getClockName() {
            return CLOCK_NAME;
        }
    }

    @RequiresApi(api = Version.V24_70)
    private static class ChineseLunarClock implements TimeClockViewer {

        private static final SpannableString CLOCK_NAME = SpannableString.valueOf("Chinese Lunar Calendar");

        private ChineseCalendar mLunarChineseCalendar = new ChineseCalendar();

        @NonNull
        @Override
        public Spanned getTimePresentation() {
            mLunarChineseCalendar.setTimeInMillis(System.currentTimeMillis());

            final int year = ChineseLunarCalendarHelper.getYear(mLunarChineseCalendar);
            final String month = ChineseLunarCalendarHelper.getMonthName(mLunarChineseCalendar);
            final String day = ChineseLunarCalendarHelper.getDayName(mLunarChineseCalendar);
            final String text = TextTool.format("%d年%s%s", year, month, day);

            return SpannableString.valueOf(text);
        }

        @NonNull
        @Override
        public Spanned getClockName() {
            return CLOCK_NAME;
        }
    }
}
