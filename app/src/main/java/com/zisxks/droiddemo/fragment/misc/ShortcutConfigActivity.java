package com.zisxks.droiddemo.fragment.misc;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.zisxks.droiddemo.R;
import com.zisxks.droiddemo.activity.entrance.main.MainActivity;

import androidx.appcompat.app.AppCompatActivity;
import run.yang.common.util.notif.ToastManager;

/**
 * Use Add Shortcut/Widget in your launcher to activate this activity, this activity is used to config the shortcut/widget
 */
public class ShortcutConfigActivity extends AppCompatActivity implements View.OnClickListener {

    private Button mCreateButton;
    private EditText mAppNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shortcut_config);

        mCreateButton = (Button) findViewById(R.id.btnCreate);
        mCreateButton.setOnClickListener(this);

        mAppNameEditText = (EditText) findViewById(R.id.etAppName);
        mAppNameEditText.setText(R.string.app_name);
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.btnCreate:
                createShortcut();
                break;

            default:
                break;
        }
    }

    private void createShortcut() {
        String appName = mAppNameEditText.getText().toString();
        if (TextUtils.isEmpty(appName)) {
            ToastManager.show("Empty App Name");
            return;
        }

        final Intent intent = new Intent(Intent.ACTION_CREATE_SHORTCUT);
        intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, new Intent(this, MainActivity.class));
        intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, appName);
        intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(
                this, R.drawable.material_codelab_avatar));

        setResult(RESULT_OK, intent);
        finish();
    }
}
