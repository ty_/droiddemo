package com.zisxks.droiddemo.fragment.misc.build_constant;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zisxks.droiddemo.context.App;

import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.Nullable;
import run.yang.common.ui.recycler.adapter.SimpleTitleContentAdapter;
import run.yang.common.ui.recycler.adapter.TitleContentPair;
import run.yang.common.ui.recycler.view.DividerLinearRecyclerView;
import run.yang.common.ui.template.BaseFragment;

public class BuildConstantFragment extends BaseFragment {

    private DividerLinearRecyclerView mRecyclerView;

    @Nullable
    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRecyclerView = new DividerLinearRecyclerView(mContextActivity);
        return mRecyclerView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final List<TitleContentPair> textList = BuildConstantHelper.getByReflection();

        Collections.sort(textList, new Comparator<TitleContentPair>() {
            final Collator collator = App.context().newCollatorWithUserSelectedLocale();

            @Override
            public int compare(TitleContentPair lhs, TitleContentPair rhs) {
                return collator.compare(String.valueOf(lhs.title), String.valueOf(rhs.title));
            }
        });

        final SimpleTitleContentAdapter adapter = new SimpleTitleContentAdapter(mContextActivity, textList);
        mRecyclerView.setAdapter(adapter);
    }
}
