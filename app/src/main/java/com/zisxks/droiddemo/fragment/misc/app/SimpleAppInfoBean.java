package com.zisxks.droiddemo.fragment.misc.app;

import android.content.pm.ApplicationInfo;
import android.graphics.drawable.Drawable;

import androidx.annotation.Nullable;

/**
 * Created by Yang Tianmei on 2015-08-05 22:53.
 */
public class SimpleAppInfoBean {
    /**
     * @see android.content.pm.PackageManager#getApplicationIcon(ApplicationInfo)
     */
    public Drawable appIcon;

    /**
     * @see ApplicationInfo#packageName
     */
    public String packageName;

    /**
     * @see android.content.pm.PackageManager#getApplicationLabel(ApplicationInfo)
     */
    public CharSequence label;

    /**
     * @see ApplicationInfo#uid
     */
    public int uid;

    /**
     * @see android.content.pm.PackageInfo#versionName
     */
    @Nullable
    public String versionName;

    /**
     * @see android.content.pm.PackageInfo#versionCode
     */
    public int versionCode;

    /**
     * @see ApplicationInfo#targetSdkVersion
     */
    public int targetSdkVersion;

    /**
     * @see android.content.pm.PackageInfo#firstInstallTime
     */
    public long firstInstallTime;

    /**
     * @see android.content.pm.PackageInfo#lastUpdateTime
     */
    public long lastUpdateTime;

    /**
     * @see ApplicationInfo#enabled
     */
    public boolean enabled;

    /**
     * @see ApplicationInfo#flags
     */
    public int flags;

    /**
     * @see android.content.pm.PackageManager#getInstallerPackageName(String)
     */
    public String installerPackageName;
}
