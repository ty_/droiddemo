package com.zisxks.droiddemo.fragment.misc.app;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.zisxks.droiddemo.R;
import com.zisxks.droiddemo.fragment.misc.app.detail.AppDetailedComponentFragment;

import androidx.annotation.Nullable;
import run.yang.common.ui.recycler.view.DividerLinearRecyclerView;
import run.yang.common.ui.simplified.SimpleTextWatcher;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.view.V;

/**
 * Created by Yang Tianmei on 2015-08-05 22:28.
 */
public class AppInformationFragment extends BaseFragment {

    private EditText mSearchWordEditText;
    private AppInformationAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateViewImpl(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_app_information, container, false);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initRecyclerView(view);
        initSearchView(view);
    }

    private void initSearchView(View view) {
        mSearchWordEditText = V.findView(view, R.id.edit_query);
        mSearchWordEditText.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                mAdapter.filter(s.toString().trim());
            }
        });

        mSearchWordEditText.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                hideIME(v);
                return true;
            }
            return false;
        });
    }

    private void initRecyclerView(View view) {
        final DividerLinearRecyclerView recyclerView = V.findView(view, R.id.app_info_recycle_view);

        mAdapter = new AppInformationAdapter(mContextActivity, new AppInformationAdapter.ActionCallback() {
            @Override
            public void viewPackageDetail(String packageName) {
                Bundle argument = AppDetailedComponentFragment.getArgument(packageName);
                AppDetailedComponentFragment fragment = AppDetailedComponentFragment.newInstance(argument);
                getFragmentManager().beginTransaction()
                        .replace(android.R.id.content, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        });
        recyclerView.setAdapter(mAdapter);
    }

    private void hideIME(View view) {
        InputMethodManager imm = (InputMethodManager) mContextActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
