package com.zisxks.droiddemo.fragment.system.location;

import android.Manifest;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zisxks.droiddemo.R;
import com.zisxks.droiddemo.constant.PermissionRequest;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityCompat.OnRequestPermissionsResultCallback;
import run.yang.common.ui.simplified.SimpleLocationListener;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.component.PermissionTool;
import run.yang.common.util.debug.FlagToString;
import run.yang.common.util.log.Logg;
import run.yang.common.util.reflect.ReflectClass;
import run.yang.common.util.view.V;

/**
 * Created by Yang Tianmei on 2015-08-29 21:03.
 */
public class SimpleLocationFragment extends BaseFragment implements OnRequestPermissionsResultCallback {

    private static final String TAG = SimpleLocationFragment.class.getSimpleName();

    private TextView mLocationTextView;
    private TextView mCountryTextView;
    private SimpleLocationListener mSimpleLocationListener;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateViewImpl(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_simple_location, container, false);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLocationTextView = V.findView(view, R.id.locationText);
        mCountryTextView = V.findView(view, R.id.countryText);

        if (PermissionTool.checkLocationPermission()) {
            getLocation();
        } else {
            String[] permission = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION};
            ActivityCompat.requestPermissions(mContextActivity, permission, PermissionRequest.CODE_COARSE_LOCATION);
        }

        initCountryInfo();
    }

    @Override
    public void onDestroyView() {
        LocationManager.getIns().removeUpdate(mSimpleLocationListener);
        mSimpleLocationListener = null;
        super.onDestroyView();
    }

    private void getLocation() {
        Location location = LocationManager.getIns().getBestAvailableLocation();
        boolean expired = LocationTool.isLocationExpired(location);
        Logg.d(TAG, "getBestAvailableLocation: expired: " + expired + ", " + LocationTool.toString(location));

        if (expired) {
            LocationManager.getIns().removeUpdate(mSimpleLocationListener);
            mSimpleLocationListener = new SimpleLocationListener() {
                @Override
                public void onLocationChanged(final Location location) {
                    LocationManager.getIns().removeUpdate(this);
                    if (this == mSimpleLocationListener) {
                        mSimpleLocationListener = null;
                    }

                    Logg.d(TAG, "request location result: " + LocationTool.toString(location));
                    Logg.d(TAG, LocationTool.toPrettyString(location));
                    setLocationTextView(location);
                }
            };
            LocationManager.getIns().requestSingleUpdate(mSimpleLocationListener);
        } else {
            setLocationTextView(location);
        }
    }

    private void initCountryInfo() {
        try {
            Object countryDetector = mContextActivity.getSystemService("country_detector");
            final ReflectClass countryDetectorReflect = ReflectClass.of(Class.forName("android.location.CountryDetector"));

            // android.location.Country
            Object country = countryDetectorReflect.getMethod("detectCountry").invoke(countryDetector);
            if (country != null) {
                Class<?> countryClass = Class.forName("android.location.Country");
                final ReflectClass countryReflect = ReflectClass.of(countryClass);

                // int
                Object sourceInt = countryReflect.getMethod("getSource").invoke(country);
                String sourceName = new FlagToString(countryClass, "COUNTRY_SOURCE_").toString((int) sourceInt);

                final Object countryIso = countryReflect.getMethod("getCountryIso").invoke(country);

                String text = String.format("Country: %s, source: %s", countryIso, sourceName);
                mCountryTextView.setText(text);
            }
        } catch (Exception e) {
            mCountryTextView.setText("Error get country info.\n" + e.getMessage());
        }
    }

    @UiThread
    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        if (permissions.length == 0 || grantResults.length == 0) {
            return;
        }

        switch (requestCode) {
            case PermissionRequest.CODE_COARSE_LOCATION:
                if (PermissionTool.checkLocationPermission()) {
                    getLocation();
                } else {
                    mLocationTextView.setText(R.string.location_permission_not_granted);
                }
                break;

            default:
                break;
        }
    }

    @UiThread
    private void setLocationTextView(@Nullable Location location) {
        mLocationTextView.setText(LocationTool.toPrettyString(location));
    }
}
