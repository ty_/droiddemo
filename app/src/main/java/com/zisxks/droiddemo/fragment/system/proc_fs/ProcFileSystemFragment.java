package com.zisxks.droiddemo.fragment.system.proc_fs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zisxks.droiddemo.R;

import java.util.List;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import run.yang.common.ui.recycler.adapter.TitleContentAdapter;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.view.V;

/**
 * Created by Yang Tianmei on 2015-10-13.
 */
public class ProcFileSystemFragment extends BaseFragment {

    private static final String TAG = ProcFileSystemFragment.class.getSimpleName();

    private RecyclerView mRecyclerView;

    @Nullable
    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_proc_file_system, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = V.findView(view, R.id.proc_fs_recycler_view);

        setAdapter();
    }

    private void setAdapter() {
        final List<PerformanceMetricsBean> metricsList = ProcFileSystemHelper.getPerformanceMetricsBeanList();
        initListView(metricsList);
    }

    private void initListView(List<PerformanceMetricsBean> list) {
        if (list == null) {
            return;
        }
        final TitleContentAdapter<PerformanceMetricsBean> adapter = new TitleContentAdapter<PerformanceMetricsBean>(mContextActivity, list) {
            @Override
            protected void onBindView(ViewHolder holder, PerformanceMetricsBean item, int position) {
                final String s = String.format(Locale.US, "uptime: %,6d s, cpu usage: %2.2f%%, cpu time: %,3d s",
                        item.uptime, item.cpuUsage * 100, item.totalCpuTime);

                final StringBuilder builder = new StringBuilder(String.valueOf(position));
                builder.append(": ").append(item.packageName);

                holder.title.setText(builder);
                holder.content.setText(s);
            }
        };
        mRecyclerView.setAdapter(adapter);
    }
}
