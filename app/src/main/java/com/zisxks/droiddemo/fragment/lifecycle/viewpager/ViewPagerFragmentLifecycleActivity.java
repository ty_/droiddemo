package com.zisxks.droiddemo.fragment.lifecycle.viewpager;

import android.os.Bundle;

import com.zisxks.droiddemo.R;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import run.yang.common.ui.template.BaseActivity;

public class ViewPagerFragmentLifecycleActivity extends BaseActivity {

    private ViewPager mViewPager;
    private PagerAdapter mFragmentPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager_fragment_lifecycle);

        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mViewPager.setOffscreenPageLimit(0);

        mFragmentPagerAdapter = new SimpleFragmentPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mFragmentPagerAdapter);
    }
}
