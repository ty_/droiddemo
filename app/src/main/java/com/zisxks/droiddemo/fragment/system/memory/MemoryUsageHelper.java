package com.zisxks.droiddemo.fragment.system.memory;

import android.content.Context;
import android.os.Debug;
import android.text.format.Formatter;

import com.zisxks.droiddemo.context.App;

import java.util.ArrayList;
import java.util.Map;

import run.yang.common.ui.recycler.adapter.TitleContentPair;
import run.yang.common.util.android.Version;

/**
 * Created by ty on 12/30/15.
 */
class MemoryUsageHelper {

    public static ArrayList<TitleContentPair> getListItems() {
        final ArrayList<TitleContentPair> list = new ArrayList<>();

        final Context context = App.context();
        {
            final Runtime runtime = Runtime.getRuntime();
            list.add(TitleContentPair.create("Runtime.availableProcessors", String.valueOf(runtime.availableProcessors())));
            list.add(TitleContentPair.create("Runtime.totalMemory(current heap size)", Formatter.formatFileSize(context, runtime.totalMemory())));
            list.add(TitleContentPair.create("Runtime.freeMemory", Formatter.formatFileSize(context, runtime.freeMemory())));
            list.add(TitleContentPair.create("Runtime.maxMemory", Formatter.formatFileSize(context, runtime.maxMemory())));
        }

        final int KB = 1024;
        {
            Debug.MemoryInfo memoryInfo = new Debug.MemoryInfo();
            Debug.getMemoryInfo(memoryInfo);
            list.add(TitleContentPair.create("MemoryInfo.getTotalPss", Formatter.formatFileSize(context, memoryInfo.getTotalPss() * KB)));
            list.add(TitleContentPair.create("MemoryInfo.getTotalPrivateDirty", Formatter.formatFileSize(context, memoryInfo.getTotalPrivateDirty() * KB)));
            list.add(TitleContentPair.create("MemoryInfo.dalvikPss", Formatter.formatFileSize(context, memoryInfo.dalvikPss * KB)));
            list.add(TitleContentPair.create("MemoryInfo.nativePss", Formatter.formatFileSize(context, memoryInfo.nativePss * KB)));
            list.add(TitleContentPair.create("MemoryInfo.otherPss", Formatter.formatFileSize(context, memoryInfo.otherPss * KB)));
        }

        if (Version.atLeast(Version.V14_40)) {
            list.add(TitleContentPair.create("Debug.getPss", Formatter.formatFileSize(context, Debug.getPss() * KB)));
        }
        list.add(TitleContentPair.create("Debug.getNativeHeapSize", Formatter.formatFileSize(context, Debug.getNativeHeapSize())));
        list.add(TitleContentPair.create("Debug.getNativeHeapAllocatedSize", Formatter.formatFileSize(context, Debug.getNativeHeapAllocatedSize())));
        list.add(TitleContentPair.create("Debug.getNativeHeapFreeSize", Formatter.formatFileSize(context, Debug.getNativeHeapFreeSize())));
        list.add(TitleContentPair.create("Debug.getLoadedClassCount", String.valueOf(Debug.getLoadedClassCount())));

        if (Version.atLeast(Version.V23_60)) {
            Map<String, String> runtimeStats = Debug.getRuntimeStats();
            if (runtimeStats != null) {
                for (Map.Entry<String, String> entry : runtimeStats.entrySet()) {
                    list.add(TitleContentPair.create("Debug.getRuntimeStats - " + entry.getKey(), entry.getValue()));
                }
            }
        }

        return list;
    }
}
