package com.zisxks.droiddemo.fragment.lifecycle;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zisxks.droiddemo.R;

import androidx.annotation.Nullable;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.view.V;

public class LifecyclePagerItemFragment extends BaseFragment {
    public static final String EXTRA_INDEX = "com.zisxks.droiddemo.fragment.lifecycle.index";

    public static Bundle getArguments(int index) {
        Bundle args = new Bundle();
        args.putInt(EXTRA_INDEX, index);
        return args;
    }

    public static LifecyclePagerItemFragment newInstance(int index) {
        LifecyclePagerItemFragment fragment = new LifecyclePagerItemFragment();
        fragment.setArguments(getArguments(index));

        return fragment;
    }

    public LifecyclePagerItemFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lifecycle_pager_item, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final TextView textView = V.findView(view, R.id.fragment_description_text);

        String text = null;
        final Bundle args = getArguments();
        if (args != null) {
            int index = args.getInt(EXTRA_INDEX);
            text = "Fragment " + index;
        }
        textView.setText(text);
    }
}
