package com.zisxks.droiddemo.fragment.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.material.snackbar.Snackbar;
import com.zisxks.droiddemo.R;
import com.zisxks.droiddemo.util.rx.RxHelper;

import java.io.FileNotFoundException;
import java.io.InputStream;

import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.image.blur.BlurBitmap;
import run.yang.common.util.io.Closeables;
import run.yang.common.util.log.Logg;
import run.yang.common.util.view.V;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;


/**
 * Created by Yang Tianmei on 2016-03-14 20:46.
 */
public class BlurImageFragment extends BaseFragment implements View.OnClickListener {

    private static final int REQUEST_CODE_PICK_IMAGE = 1;
    private static final String TAG = Logg.getTag(BlurImageFragment.class);

    private ImageView mImageView;

    private Bitmap mInputBitmap;
    private Bitmap mBlurredBitmap;

    private final PublishSubject<Uri> mLoadImageSubject = PublishSubject.create();
    private final PublishSubject<Bitmap> mBlurImageSubject = PublishSubject.create();
    private Subscription mLoadImageSubscription;
    private Subscription mBlurImageSubscription;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createSubscriber();
    }

    @Nullable
    @Override
    public View onCreateViewImpl(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_blur_image, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mImageView = V.findView(view, R.id.blur_image_image_view);
        view.findViewById(R.id.blur_image_select_image_button).setOnClickListener(this);
        view.findViewById(R.id.blur_image_show_original).setOnClickListener(this);
        view.findViewById(R.id.blur_image_show_blurred).setOnClickListener(this);
    }

    @Override
    public void onDestroy() {
        RxHelper.unsubscribe(mLoadImageSubscription);
        RxHelper.unsubscribe(mBlurImageSubscription);
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.blur_image_select_image_button:
                launchSelectImageChooser();
                break;

            case R.id.blur_image_show_original:
                showOriginalImage();
                break;

            case R.id.blur_image_show_blurred:
                showBlurredImage();
                break;
        }
    }

    private void createSubscriber() {
        mLoadImageSubscription = mLoadImageSubject
                .map(uri -> {
                    if (uri == null || Uri.EMPTY == uri) {
                        return null;
                    }
                    InputStream inputStream = null;
                    try {
                        try {
                            inputStream = mContextActivity.getContentResolver().openInputStream(uri);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        if (inputStream == null) {
                            return null;
                        }
                        return BitmapFactory.decodeStream(inputStream);
                    } finally {
                        Closeables.closeOneLiner(inputStream);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(bitmap -> {
                    mInputBitmap = bitmap;
                    mBlurredBitmap = null;
                    mImageView.setImageBitmap(bitmap);
                });

        mBlurImageSubscription = mBlurImageSubject
                .map(inputBitmap -> {
                    long start = SystemClock.elapsedRealtime();
                    final float radius = 6.f;
                    Bitmap bitmap = BlurBitmap.blur(mContextActivity, inputBitmap, new BlurBitmap.Options(radius, 3));
                    return Pair.create(bitmap, SystemClock.elapsedRealtime() - start);
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    mImageView.setImageBitmap(mBlurredBitmap = result.first);
                    final String message = getString(R.string.blur_image_blur_time_message, result.second);
                    Snackbar.make(mImageView, message, Snackbar.LENGTH_SHORT).show();
                });
    }

    private void launchSelectImageChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        Intent chooser = Intent.createChooser(intent, mContextActivity.getString(R.string.blur_image_select_picture));
        startActivityForResult(chooser, REQUEST_CODE_PICK_IMAGE);
    }

    private void showOriginalImage() {
        if (mInputBitmap != null) {
            mImageView.setImageBitmap(mInputBitmap);
        }
    }

    private void showBlurredImage() {
        if (mBlurredBitmap != null) {
            mImageView.setImageBitmap(mBlurredBitmap);
        } else if (mInputBitmap != null) {
            //TODO: if blur is going, we will have do it more than once.
            mBlurImageSubject.onNext(mInputBitmap);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return;
            }
            mLoadImageSubject.onNext(data.getData());
        }
    }
}
