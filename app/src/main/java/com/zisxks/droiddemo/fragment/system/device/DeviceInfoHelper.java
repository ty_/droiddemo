package com.zisxks.droiddemo.fragment.system.device;

import android.content.Context;
import android.text.format.Formatter;
import android.util.DisplayMetrics;

import com.zisxks.droiddemo.context.App;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import run.yang.common.ui.recycler.adapter.TitleContentPair;
import run.yang.common.util.android.Version;
import run.yang.common.util.basic.TextTool;
import run.yang.common.util.system.DeviceTool;

/**
 * Created by Yang Tianmei on 2015-09-23.
 */
class DeviceInfoHelper {

    public static List<TitleContentPair> getListItems() {
        Context context = App.context();

        final List<TitleContentPair> list = new ArrayList<>();

        list.add(TitleContentPair.create("ANDROID_ID", DeviceTool.getAndroidId()));
        list.add(TitleContentPair.create("DeviceId", DeviceTool.getDeviceId()));

        long ramSizeFromMemInfo = DeviceTool.getRamSizeFromMemInfoInKb();
        String formattedRamSize = Formatter.formatFileSize(context, ramSizeFromMemInfo * (1 << 10));
        list.add(TitleContentPair.create("RAM Size(/proc/meminfo)", formattedRamSize + " (" + ramSizeFromMemInfo + " KB)"));

        if (Version.atLeast(Version.V16_41)) {
            final long ramSize = DeviceTool.getRamSizeApi16();
            formattedRamSize = Formatter.formatFileSize(context, ramSize);
            list.add(TitleContentPair.create("RAM Size(MemoryInfo)", formattedRamSize + " (" + ramSize / (1 << 10) + " KB)"));
        }

        final DisplayMetrics dm = context.getResources().getDisplayMetrics();
        list.add(TitleContentPair.create("DisplayMetrics.(widthPixels, heightPixels)",
                String.format(Locale.US, "(%d, %d) px", dm.widthPixels, dm.heightPixels)));

        list.add(TitleContentPair.create("DisplayMetrics.density", String.valueOf(dm.density)));

        int densityDpi = dm.densityDpi;
        list.add(TitleContentPair.create("DisplayMetrics.densityDpi",
                densityDpi + " (" + TextTool.join(densityToStringMap(densityDpi), "/") + ")"));

        list.add(TitleContentPair.create("DisplayMetrics.(xdpi, ydpi)", String.format(Locale.US, "(%.2f, %.2f)", dm.xdpi, dm.ydpi)));

        list.add(TitleContentPair.create("DisplayMetrics.scaledDensity", String.valueOf(dm.scaledDensity)));

        return list;
    }

    private static List<String> densityToStringMap(int densityDpi) {
        final Map<String, Integer> map = new HashMap<>();
        map.put("default", DisplayMetrics.DENSITY_DEFAULT);
        map.put("low", DisplayMetrics.DENSITY_LOW);
        map.put("medium", DisplayMetrics.DENSITY_MEDIUM);
        map.put("high", DisplayMetrics.DENSITY_HIGH);
        map.put("xhigh", DisplayMetrics.DENSITY_XHIGH);
        if (Version.atLeast(Version.V16_41)) {
            map.put("xxhigh", DisplayMetrics.DENSITY_XXHIGH);
        }
        if (Version.atLeast(Version.V18_43)) {
            map.put("xxxhigh", DisplayMetrics.DENSITY_XXXHIGH);
        }

        if (Version.atLeast(Version.V22_51)) {
            map.put("280", DisplayMetrics.DENSITY_280);
        }
        if (Version.atLeast(Version.V23_60)) {
            map.put("360", DisplayMetrics.DENSITY_360);
            map.put("420", DisplayMetrics.DENSITY_420);
        }
        if (Version.atLeast(Version.V19_44)) {
            map.put("400", DisplayMetrics.DENSITY_400);
        }
        if (Version.atLeast(Version.V21_50)) {
            map.put("560", DisplayMetrics.DENSITY_560);
        }

        ArrayList<String> list = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getValue() == densityDpi) {
                list.add(entry.getKey());
            }
        }
        if (list.isEmpty()) {
            list.add("non standard: " + String.valueOf(densityDpi));
        }
        return list;
    }
}
