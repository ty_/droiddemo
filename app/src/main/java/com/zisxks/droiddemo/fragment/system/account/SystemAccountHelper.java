package com.zisxks.droiddemo.fragment.system.account;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

import com.zisxks.droiddemo.context.App;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yang Tianmei on 2015-10-08.
 */
class SystemAccountHelper {
    private static final String TAG = SystemAccountHelper.class.getSimpleName();

    static List<String> getAccountList() {
        List<String> list = new ArrayList<>(4);
        Context context = App.context();
        final Account[] accounts = AccountManager.get(context).getAccounts();
        for (Account account : accounts) {
            list.add(account.toString());
        }
        return list;
    }
}
