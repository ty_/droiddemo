package com.zisxks.droiddemo.fragment.system.location;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.text.TextUtils;

import com.zisxks.droiddemo.context.App;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.util.component.PermissionTool;
import run.yang.common.util.log.Logg;

/**
 * Created by Yang Tianmei on 2015-08-29 21:22.
 */
public class LocationManager {

    private static final String TAG = LocationManager.class.getSimpleName();

    private static final class SingletonHolder {
        private static final LocationManager sInstance = new LocationManager();
    }

    private final Context mContext;
    private final android.location.LocationManager mLocationService;

    public static LocationManager getIns() {
        return SingletonHolder.sInstance;
    }

    private LocationManager() {
        mContext = App.context().getApplicationContext();
        mLocationService = (android.location.LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
    }

    @Nullable
    public Location getBestAvailableLocation() {
        if (PermissionTool.checkLocationPermission()) {
            final Criteria criteria = newApproximateLocationCriteria();

            List<String> allProviders = mLocationService.getAllProviders();
            System.out.println(allProviders);

            String bestProvider = mLocationService.getBestProvider(criteria, true);
            Logg.d(TAG, "best provider: " + bestProvider);
            if (TextUtils.isEmpty(bestProvider)) {
                return null;
            }
            Location lastKnownLocation = mLocationService.getLastKnownLocation(bestProvider);
            Logg.d(TAG, LocationTool.toString(lastKnownLocation));

            return lastKnownLocation;
        }
        return null;
    }

    public void requestSingleUpdate(@NonNull LocationListener listener) {
        if (PermissionTool.checkLocationPermission()) {
            Criteria criteria = newApproximateLocationCriteria();
            try {
                mLocationService.requestSingleUpdate(criteria, listener, null);
                Logg.d(TAG, "request Single location update");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void removeUpdate(@Nullable LocationListener listener) {
        if (listener == null) {
            return;
        }
        if (PermissionTool.checkLocationPermission()) {
            mLocationService.removeUpdates(listener);
        }
    }

    @NonNull
    private Criteria newApproximateLocationCriteria() {
        final Criteria criteria = new Criteria();
        criteria.setHorizontalAccuracy(Criteria.ACCURACY_LOW);
        return criteria;
    }
}
