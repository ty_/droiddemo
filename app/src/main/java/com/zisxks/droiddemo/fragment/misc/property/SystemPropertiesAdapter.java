package com.zisxks.droiddemo.fragment.misc.property;

import android.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import run.yang.common.util.view.V;

/**
 * Created by Yang Tianmei on 2015-09-09 21:27.
 */
class SystemPropertiesAdapter extends BaseAdapter {

    private final List<PropertyBean> list;
    private final LayoutInflater mLayoutInflater;

    public SystemPropertiesAdapter(@NonNull Context context, @NonNull List<PropertyBean> list) {
        this.list = list;
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(final int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.simple_list_item_2, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        PropertyBean bean = list.get(position);
        holder.key.setText(bean.propertyName);
        holder.value.setText(bean.propertyValue);

        return convertView;
    }

    private static class ViewHolder {
        public ViewHolder(View root) {
            key = V.findView(root, R.id.text1);
            value = V.findView(root, R.id.text2);
        }

        public TextView key;
        public TextView value;
    }
}
