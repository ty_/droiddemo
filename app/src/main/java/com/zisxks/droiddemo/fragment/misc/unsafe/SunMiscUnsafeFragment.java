package com.zisxks.droiddemo.fragment.misc.unsafe;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zisxks.droiddemo.R;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.java.sizeof.ObjectSizeEstimator;

/**
 * 创建时间: 2017/11/20 10:38 <br>
 * 作者: Yang Tianmei <br>
 * 描述:
 */

public class SunMiscUnsafeFragment extends BaseFragment {

    @Override
    public View onCreateViewImpl(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sun_misc_unsafe, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView contentTextView = view.findViewById(R.id.content_text_view);

        final StringBuilder sb = new StringBuilder();

        calculateSize(Integer.class, sb);
        calculateSize(Long.class, sb);
        calculateSize(String.class, sb);
        calculateSize(View.class, sb);
        calculateSize(TextView.class, sb);
        calculateSize(Pair.class, sb);
        calculateSize(Context.class, sb);
        calculateSize(Activity.class, sb);
        calculateSize(Fragment.class, sb);
        calculateSize(Service.class, sb);

        contentTextView.setText(sb);
    }

    private void calculateSize(Class<?> clazz, StringBuilder sb) {
        sb.append(clazz.getSimpleName()).append(": ").append(ObjectSizeEstimator.sizeof(clazz)).append("\n");
    }
}
