package com.zisxks.droiddemo.fragment.misc;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.zisxks.droiddemo.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.view.V;

public class ColorUtilFragment extends BaseFragment {

    private static int getColorFromProgress(int progress) {
        return 0xFF << 24 | progress;
    }

    @Nullable
    @Override
    public View onCreateViewImpl(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_color_util, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final TextView textView = V.findView(view, R.id.text_view);
        final SeekBar sbForeground = V.findView(view, R.id.seekBarForeground);

        sbForeground.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textView.setTextColor(getColorFromProgress(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        SeekBar sbBackground = V.findView(view, R.id.seekBarBackground);
        sbBackground.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textView.setBackgroundColor(getColorFromProgress(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

    }
}
