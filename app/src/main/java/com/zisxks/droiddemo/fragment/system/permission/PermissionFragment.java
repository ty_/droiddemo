package com.zisxks.droiddemo.fragment.system.permission;


import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.util.Pair;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.zisxks.droiddemo.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

import androidx.annotation.IntDef;
import run.yang.common.ui.simplified.SimpleOnItemSelectedListener;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.log.Logg;
import run.yang.common.util.view.V;

public class PermissionFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = PermissionFragment.class.getSimpleName();

    private static final int MODE_PERMISSION = 1;
    private static final int MODE_OP_STR = 2;

    private static class InstanceState {
        static final String KEY_SELECTED_MODE = "selectedMode";
        static final String KEY_SELECTED_POSITION = "selectedIndex";
    }

    private Spinner mSpinner;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({MODE_PERMISSION, MODE_OP_STR})
    private @interface Mode {
    }

    private List<Pair<String, String>> mPermissionList;
    private PermissionSpinnerAdapter mPermissionSpinnerAdapter;

    private List<Pair<String, String>> mOpStrList;
    private PermissionSpinnerAdapter mOpStrSpinnerAdapter;

    @Mode
    private int mCurrentMode = MODE_PERMISSION;

    private SparseIntArray mCurrentSelectionMap = new SparseIntArray(2);

    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_permission, container, false);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.permission_rb_permission).setOnClickListener(this);
        view.findViewById(R.id.permission_rb_op_str).setOnClickListener(this);

        final TextView textView = V.findView(view, R.id.textView);

        final SimpleOnItemSelectedListener onItemSelectedListener = new SimpleOnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
                SpannableStringBuilder spannedText = null;
                switch (mCurrentMode) {
                    case MODE_PERMISSION:
                        final Pair<String, String> permission = mPermissionList.get(position);
                        spannedText = PermissionHelper.testPermission(permission.second);
                        break;

                    case MODE_OP_STR:
                        final Pair<String, String> opStr = mOpStrList.get(position);
                        spannedText = PermissionHelper.testOpStr(opStr.second);
                        break;

                    default:
                        break;
                }
                textView.setText(spannedText);
            }
        };

        mOpStrList = PermissionHelper.getOpStrs();
        mOpStrSpinnerAdapter = new PermissionSpinnerAdapter(mContextActivity, mOpStrList);

        mPermissionList = PermissionHelper.getAllSystemPermissions();
        mPermissionSpinnerAdapter = new PermissionSpinnerAdapter(mContextActivity, mPermissionList);

        mSpinner = V.findView(view, R.id.permission_spinner);
        mSpinner.setOnItemSelectedListener(onItemSelectedListener);

        restoreState(savedInstanceState);
    }

    private void restoreState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            mSpinner.setAdapter(mPermissionSpinnerAdapter);
            mCurrentMode = MODE_PERMISSION;
            Logg.d(TAG, "no saved instance bundle");
            return;
        }

        //noinspection ResourceType
        mCurrentMode = savedInstanceState.getInt(InstanceState.KEY_SELECTED_MODE, MODE_PERMISSION);
        switch (mCurrentMode) {
            case MODE_OP_STR:
                mSpinner.setAdapter(mOpStrSpinnerAdapter);
                break;
            case MODE_PERMISSION:
            default:
                mSpinner.setAdapter(mPermissionSpinnerAdapter);
                break;
        }

        int selectedPosition = savedInstanceState.getInt(InstanceState.KEY_SELECTED_POSITION, AdapterView.INVALID_POSITION);
        if (selectedPosition != AdapterView.INVALID_POSITION) {
            mSpinner.setSelection(selectedPosition, true);
            mCurrentSelectionMap.put(mCurrentMode, selectedPosition);
        }
        Logg.d(TAG, "current mode: " + mCurrentMode + ", selectedPosition: " + selectedPosition);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(InstanceState.KEY_SELECTED_MODE, mCurrentMode);
        if (mSpinner != null) {
            outState.putInt(InstanceState.KEY_SELECTED_POSITION, mSpinner.getSelectedItemPosition());
            Logg.d(TAG, "save current mode: " + mCurrentMode + ", selectedPosition: " + mSpinner.getSelectedItemPosition());
        }
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.permission_rb_permission:
                if (mCurrentMode != MODE_PERMISSION) {
                    mCurrentSelectionMap.put(mCurrentMode, mSpinner.getSelectedItemPosition());
                    mCurrentMode = MODE_PERMISSION;
                    mSpinner.setAdapter(mPermissionSpinnerAdapter);
                    mSpinner.setSelection(mCurrentSelectionMap.get(mCurrentMode));
                }
                break;

            case R.id.permission_rb_op_str:
                if (mCurrentMode != MODE_OP_STR) {
                    mCurrentSelectionMap.put(mCurrentMode, mSpinner.getSelectedItemPosition());
                    mCurrentMode = MODE_OP_STR;
                    mSpinner.setAdapter(mOpStrSpinnerAdapter);
                    mSpinner.setSelection(mCurrentSelectionMap.get(mCurrentMode));
                }
                break;
        }
    }
}
