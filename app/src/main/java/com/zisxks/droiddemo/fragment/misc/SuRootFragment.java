package com.zisxks.droiddemo.fragment.misc;


import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.zisxks.droiddemo.R;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.root.CommandResult;
import run.yang.common.util.root.RootTool;
import run.yang.common.util.view.V;


public class SuRootFragment extends BaseFragment implements View.OnClickListener {

    @NonNull
    private ExecutorService sThreadPool = Executors.newFixedThreadPool(1);
    @Nullable
    private Future<?> mCurrentTask = null;

    private EditText mEditTextCommand;
    private TextView mOutputTextView;
    private Button mRunButton;
    private CheckBox mRunAsRootCheckBox;

    public SuRootFragment() {
    }

    @Override
    public View onCreateViewImpl(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_su_root, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRunButton = V.findView(view, R.id.buttonRun);
        mRunButton.setOnClickListener(this);

        mOutputTextView = V.findView(view, R.id.outputText);
        mEditTextCommand = V.findView(view, R.id.editTextCommand);
        mRunAsRootCheckBox = V.findView(view, R.id.checkboxRunAsRoot);

        mEditTextCommand.setSelection(mEditTextCommand.getText().length());
        mEditTextCommand.setOnEditorActionListener((v, actionId, event) -> {
            // some IME don't use customized actionId
            if (actionId == EditorInfo.IME_ACTION_GO
                    || actionId == getResources().getInteger(R.integer.fragment_su_root_ime_action_run_id)) {
                final String command = mEditTextCommand.getText().toString().trim();
                if (TextUtils.isEmpty(command)) {
                    return true;
                }
                getAndRunCommand(command);
                return true;
            } else {
                return false;
            }
        });
    }

    @Override
    public void onClick(@NonNull View v) {
        switch (v.getId()) {
            case R.id.buttonRun:
                if (mCurrentTask != null && !mCurrentTask.isDone()) {
                    mCurrentTask.cancel(true);
                    mRunButton.setText(R.string.fragment_su_root_run);
                } else {
                    final String command = mEditTextCommand.getText().toString().trim();
                    if (TextUtils.isEmpty(command)) {
                        return;
                    }
                    getAndRunCommand(command);

                    mOutputTextView.setText("command running: \n\n" + command);
                    mRunButton.setText(R.string.stop);
                }
                break;
        }
    }

    private void getAndRunCommand(@NonNull final String command) {

        if (mCurrentTask != null && !mCurrentTask.isDone()) {
            mCurrentTask.cancel(true);
        }

        mCurrentTask = sThreadPool.submit((Runnable) () -> {
            final RootTool.ExecOptions options = new RootTool.ExecOptions();
            options.runAsRoot = mRunAsRootCheckBox.isChecked();
            options.needOutput = true;
            final CommandResult result = RootTool.exec(command, options);

            if (Thread.interrupted()) {
                return;
            }

            Activity activity = SuRootFragment.this.getActivity();
            if (activity != null && !activity.isFinishing()) {
                activity.runOnUiThread(() -> {
                    mOutputTextView.setText(result.getResultDescription());
                    mRunButton.setText(R.string.fragment_su_root_run);
                });
            }
        });
    }
}
