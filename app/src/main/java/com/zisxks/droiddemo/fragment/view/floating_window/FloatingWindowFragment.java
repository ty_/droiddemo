package com.zisxks.droiddemo.fragment.view.floating_window;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

import com.zisxks.droiddemo.R;
import com.zisxks.droiddemo.service.FloatingWindowService;

import androidx.annotation.NonNull;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.android.Version;
import run.yang.common.util.build.BuildControl;
import run.yang.common.util.component.ContextTool;
import run.yang.common.util.component.PermissionTool;
import run.yang.common.util.log.Logg;
import run.yang.common.util.notif.ToastManager;
import run.yang.common.util.safehandler.HandlerCallback;
import run.yang.common.util.safehandler.NonLeakHandler;
import run.yang.common.util.view.V;


public class FloatingWindowFragment extends BaseFragment implements View.OnClickListener, HandlerCallback {

    private static final String TAG = FloatingWindowFragment.class.getSimpleName();

    private static final int MSG_SHOW_FLOATING_WINDOW = 1;
    private static final long SHOW_FLOATING_WINDOW_DELAY_MILLIS = 200;

    private static final int REQ_MANAGE_OVERLAY_PERMISSION = 1;

    private NonLeakHandler<FloatingWindowFragment> mHandler = new NonLeakHandler<>(this, Looper.getMainLooper());
    private WindowManager mWindowManager;

    private View mGuideFloatingView;
    private Button mGuideFloatingWindowButton;
    private Button mNetworkSpeedButton;
    private boolean mIsGuideFloatingWindowShown = false;
    private boolean mIsNetworkSpeedShown = false;

    private Runnable mPendingRunnable = null;

    private final Runnable mShowNetworkSpeedRunnable = new Runnable() {
        @Override
        public void run() {
            FloatingWindowService.startService();
            mIsNetworkSpeedShown = true;
            updateNetworkSpeedButtonText();
        }
    };

    private final Runnable mHideNetworkSpeedRunnable = new Runnable() {
        @Override
        public void run() {
            FloatingWindowService.stopService();
            mIsNetworkSpeedShown = false;
            updateNetworkSpeedButtonText();
        }
    };

    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_floating_window, container, false);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mWindowManager = (WindowManager) mContextActivity.getApplicationContext().getSystemService(Context.WINDOW_SERVICE);

        mGuideFloatingWindowButton = V.findView(view, R.id.floating_btn_show_guide_floating_window);
        mGuideFloatingWindowButton.setOnClickListener(this);

        mNetworkSpeedButton = V.findView(view, R.id.floating_btn_show_net_speed);
        mNetworkSpeedButton.setOnClickListener(this);

        Button openDrawOverlayManager = V.findView(view, R.id.floating_btn_open_draw_overlay_permission_manager);
        if (Version.atLeast(Version.V23_60)) {
            openDrawOverlayManager.setOnClickListener(this);
        } else {
            openDrawOverlayManager.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // better way is to query the service whether floating window is shown
        if (!PermissionTool.checkSystemAlertWindowPermission()) {
            mIsNetworkSpeedShown = false;
            mPendingRunnable = null;
        }
        updateGuideButtonText();
        updateNetworkSpeedButtonText();
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.floating_btn_show_guide_floating_window:
                toggleGuideFloatingWindowVisibility();
                break;

            case R.id.floating_btn_show_net_speed:
                toggleNetworkSpeedFloatingWindowVisibility();
                break;

            case R.id.floating_btn_open_draw_overlay_permission_manager:
                if (!startDrawOverlayManager()) {
                    ToastManager.show(R.string.floating_window_failed_open_draw_overlay_manager);
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_MANAGE_OVERLAY_PERMISSION:
                if (PermissionTool.checkSystemAlertWindowPermission() && mPendingRunnable != null) {
                    mPendingRunnable.run();
                    mPendingRunnable = null;
                }
                break;
        }
    }

    @Override
    public void handleMessage(final Message msg) {
        switch (msg.what) {
            case MSG_SHOW_FLOATING_WINDOW:
                showFloatingWindow();
                break;
        }
    }

    private void updateGuideButtonText() {
        mGuideFloatingWindowButton.setText(mIsGuideFloatingWindowShown
                ? R.string.floating_window_btn_text_hide_guide_floating_window
                : R.string.floating_window_btn_text_show_guide_floating_window);
    }

    private void updateNetworkSpeedButtonText() {
        mNetworkSpeedButton.setText(mIsNetworkSpeedShown
                ? R.string.floating_window_btn_text_hide_net_speed
                : R.string.floating_window_btn_text_show_net_speed);
    }

    private void toggleGuideFloatingWindowVisibility() {
        Runnable action = mIsGuideFloatingWindowShown ? mHideGuideFloatingWindowRunnable : mShowGuideFloatingWindowRunnable;
        runOrRequestDrawOverlayPermission(action);
    }

    private void toggleNetworkSpeedFloatingWindowVisibility() {
        Runnable action = mIsNetworkSpeedShown ? mHideNetworkSpeedRunnable : mShowNetworkSpeedRunnable;
        runOrRequestDrawOverlayPermission(action);
    }

    private boolean startTargetActivity() {
        final Intent intent = new Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        return ContextTool.startActivityChecked(mContextActivity, intent);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void runOrRequestDrawOverlayPermission(@NonNull Runnable action) {
        if (PermissionTool.checkSystemAlertWindowPermission()) {
            action.run();
        } else {
            if (startDrawOverlayManager()) {
                mPendingRunnable = action;
                String msg = getString(R.string.floating_window_allow_draw_over_other_apps, getString(R.string.app_name));
                ToastManager.show(msg);
            } else {
                ToastManager.show(R.string.floating_window_failed_open_draw_overlay_manager);
            }
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    private boolean startDrawOverlayManager() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + BuildControl.APPLICATION_ID));
        return ContextTool.startActivityForResultChecked(this, intent, REQ_MANAGE_OVERLAY_PERMISSION);
    }

    private final Runnable mShowGuideFloatingWindowRunnable = new Runnable() {
        @Override
        public void run() {
            if (startTargetActivity()) {
                mHandler.sendEmptyMessageDelayed(MSG_SHOW_FLOATING_WINDOW, SHOW_FLOATING_WINDOW_DELAY_MILLIS);
            } else {
                ToastManager.show(R.string.floating_window_failed_to_start_developer_settings);
            }
        }
    };

    private void showFloatingWindow() {
        Logg.d(TAG, "showFloatingWindow");
        if (mGuideFloatingView != null) {
            mHideGuideFloatingWindowRunnable.run();
        }
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        params.type = WindowManager.LayoutParams.TYPE_TOAST;
        params.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        params.format = PixelFormat.RGBA_8888;
        params.gravity = Gravity.CENTER;
        params.packageName = mContextActivity.getPackageName();

        if (mGuideFloatingView == null) {
            mGuideFloatingView = LayoutInflater.from(mContextActivity).inflate(R.layout.floating_floating_window, null);
            mGuideFloatingView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    mHideGuideFloatingWindowRunnable.run();
                }
            });
        }

        mWindowManager.addView(mGuideFloatingView, params);
        mIsGuideFloatingWindowShown = true;
        updateGuideButtonText();
    }

    private final Runnable mHideGuideFloatingWindowRunnable = new Runnable() {
        @Override
        public void run() {
            if (mGuideFloatingView != null && mIsGuideFloatingWindowShown) {
                mWindowManager.removeView(mGuideFloatingView);
                mGuideFloatingView = null;
                mIsGuideFloatingWindowShown = false;
                updateGuideButtonText();
            }
        }
    };
}
