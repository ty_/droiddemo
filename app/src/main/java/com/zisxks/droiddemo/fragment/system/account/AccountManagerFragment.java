package com.zisxks.droiddemo.fragment.system.account;


import android.Manifest;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zisxks.droiddemo.R;
import com.zisxks.droiddemo.constant.PermissionRequest;

import java.util.List;

import androidx.annotation.NonNull;
import run.yang.common.ui.recycler.adapter.SimpleStringRecyclerAdapter;
import run.yang.common.ui.recycler.view.DividerLinearRecyclerView;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.component.PermissionTool;
import run.yang.common.util.notif.ToastManager;

public class AccountManagerFragment extends BaseFragment {

    private DividerLinearRecyclerView mRecyclerView;

    private final Runnable mLoadAccountInfoRunnable = new Runnable() {
        @Override
        public void run() {
            final List<String> accountList = SystemAccountHelper.getAccountList();
            final SimpleStringRecyclerAdapter<String> adapter = new SimpleStringRecyclerAdapter<>(mContextActivity, accountList);

            mRecyclerView.setAdapter(adapter);
        }
    };

    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        return mRecyclerView = new DividerLinearRecyclerView(mContextActivity);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (PermissionTool.checkReadContactPermission()) {
            mLoadAccountInfoRunnable.run();
        } else {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PermissionRequest.CODE_READ_CONTACTS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length == 0) {
            ToastManager.show(R.string.contacts_no_read_contacts_permission);
            return;
        }

        switch (requestCode) {
            case PermissionRequest.CODE_READ_CONTACTS:
                if (PermissionTool.granted(grantResults[0])) {
                    mLoadAccountInfoRunnable.run();
                } else {
                    ToastManager.show(R.string.contacts_no_read_contacts_permission);
                }
                break;
        }
    }
}
