package com.zisxks.droiddemo.fragment.view.bitmap;


import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.zisxks.droiddemo.BuildConfig;
import com.zisxks.droiddemo.R;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.ui.recycler.adapter.SimpleTitleContentAdapter;
import run.yang.common.ui.recycler.adapter.TitleContentPair;
import run.yang.common.ui.recycler.view.DividerLinearRecyclerView;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.android.Version;
import run.yang.common.util.basic.TextTool;
import run.yang.common.util.io.Closeables;
import run.yang.common.util.view.V;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;


public class BitmapInfoFragment extends BaseFragment {
    private static final int REQUEST_CODE_PICK_IMAGE = 1;

    private ImageView mImageView;
    private DividerLinearRecyclerView mRecyclerView;

    private PublishSubject<Uri> mLoadBitmapInfoSubject = PublishSubject.create();

    public BitmapInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateViewImpl(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bitmap_size, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mImageView = V.findView(view, R.id.imageview_bitmap);
        mRecyclerView = V.findView(view, R.id.recycler_view_bitmap_info);

        V.findView(view, R.id.button_select_image).setOnClickListener(v -> launchSelectImageChooser());

        mLoadBitmapInfoSubject
                .map(uri -> {
                    if (uri == null || Uri.EMPTY == uri) {
                        return null;
                    }
                    InputStream inputStream = null;
                    try {
                        try {
                            inputStream = mContextActivity.getContentResolver().openInputStream(uri);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        if (inputStream == null) {
                            return null;
                        }
                        return BitmapFactory.decodeStream(inputStream);
                    } finally {
                        Closeables.closeOneLiner(inputStream);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(bitmap -> {
                    if (bitmap == null) {
                        return;
                    }
                    mImageView.setImageBitmap(bitmap);

                    final List<TitleContentPair> list = getBitmapInfo(bitmap);
                    SimpleTitleContentAdapter adapter = new SimpleTitleContentAdapter(mContextActivity, list);
                    mRecyclerView.setAdapter(adapter);
                });

        Uri initialPicUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + BuildConfig.APPLICATION_ID + "/" + R.drawable.nav_header_bg);
        mLoadBitmapInfoSubject.onNext(initialPicUri);
    }

    private void launchSelectImageChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        Intent chooser = Intent.createChooser(intent, mContextActivity.getString(R.string.blur_image_select_picture));
        startActivityForResult(chooser, REQUEST_CODE_PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return;
            }
            final Uri uri = data.getData();
            mLoadBitmapInfoSubject.onNext(uri);
        }
    }

    private static List<TitleContentPair> getBitmapInfo(@NonNull Bitmap bitmap) {
        List<TitleContentPair> list = new ArrayList<>();
        final int width = bitmap.getWidth();
        final int height = bitmap.getHeight();
        final int byteCount = bitmap.getByteCount();

        addEntry(list, "Width, Height", TextTool.format("[%d, %d]", width, height));
        final long totalBytes;
        if (Version.atLeast(Version.V19_44)) {
            totalBytes = bitmap.getAllocationByteCount();
            addEntry(list, "getAllocationByteCount", String.valueOf(totalBytes));
        } else {
            addEntry(list, "getAllocationByteCount", "requires API 19+");
            totalBytes = byteCount;
        }

        addEntry(list, "getByteCount(API 19-)", String.valueOf(byteCount));
        addEntry(list, "getRowBytes(API 19-)", String.valueOf(bitmap.getRowBytes()));
        addEntry(list, "W * H * 4 / TotalBytes", String.valueOf(width * height * 4 / (float) totalBytes));

        return list;
    }

    private static void addEntry(List<TitleContentPair> list, CharSequence title, CharSequence content) {
        list.add(TitleContentPair.create(title, content));
    }
}
