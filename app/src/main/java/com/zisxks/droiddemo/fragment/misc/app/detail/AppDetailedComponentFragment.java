package com.zisxks.droiddemo.fragment.misc.app.detail;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.zisxks.droiddemo.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.basic.UnitTool;
import run.yang.common.util.build.BuildControl;
import run.yang.common.util.log.Logg;
import run.yang.common.util.view.PaddingHelper;
import run.yang.common.util.view.V;

/**
 * Created by Tim Yang on 2016-03-30 14:49.
 */
public class AppDetailedComponentFragment extends BaseFragment {

    private static final String TAG = Logg.getTag(AppDetailedComponentFragment.class);

    public static final String ARG_PACKAGE_NAME = BuildControl.APPLICATION_ID + ".pkg_name";

    public static Bundle getArgument(@NonNull String packageName) {
        if (TextUtils.isEmpty(packageName)) {
            throw new IllegalArgumentException("packageName cannot be empty");
        }

        Bundle bundle = new Bundle();
        bundle.putString(ARG_PACKAGE_NAME, packageName);

        return bundle;
    }

    public static AppDetailedComponentFragment newInstance(@Nullable Bundle argument) {
        AppDetailedComponentFragment fragment = new AppDetailedComponentFragment();
        if (argument != null) {
            fragment.setArguments(argument);
        }
        return fragment;
    }

    public AppDetailedComponentFragment() {
    }

    @Nullable
    @Override
    public View onCreateViewImpl(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_app_detailed_component, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        final Bundle arguments = getArguments();
        if (arguments == null) {
            return;
        }

        String packageName = arguments.getString(ARG_PACKAGE_NAME);
        List<Pair<CharSequence, List<CharSequence>>> data = AppDetailedComponentHelper.getComponentInfo(mContextActivity, packageName);

        if (data != null) {
            AppDetailedInfoAdapter adapter = new AppDetailedInfoAdapter();
            adapter.setData(data);

            ExpandableListView expandableListView = V.findView(view, R.id.expandable_list_view);
            expandableListView.setAdapter(adapter);

            TextView header = new TextView(mContextActivity);
            header.setGravity(Gravity.CENTER_VERTICAL);
            header.setText(R.string.fragment_app_detail_legend_text);
            PaddingHelper.with(header).bottom(UnitTool.dp2px_i(mContextActivity, 7)).done();
            expandableListView.addHeaderView(header);
        }
    }
}
