package com.zisxks.droiddemo.fragment.system.device;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import run.yang.common.ui.recycler.adapter.SimpleTitleContentAdapter;
import run.yang.common.ui.recycler.adapter.TitleContentPair;
import run.yang.common.ui.recycler.view.DividerLinearRecyclerView;
import run.yang.common.ui.template.BaseFragment;

public class DeviceInfoFragment extends BaseFragment {

    private DividerLinearRecyclerView mRecyclerView;

    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        mRecyclerView = new DividerLinearRecyclerView(mContextActivity);
        return mRecyclerView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final List<TitleContentPair> list = DeviceInfoHelper.getListItems();

        final SimpleTitleContentAdapter adapter = new SimpleTitleContentAdapter(mContextActivity, list);
        mRecyclerView.setAdapter(adapter);
    }
}
