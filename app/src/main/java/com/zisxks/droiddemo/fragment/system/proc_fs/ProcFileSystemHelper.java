package com.zisxks.droiddemo.fragment.system.proc_fs;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.SystemClock;
import android.text.TextUtils;

import com.zisxks.droiddemo.context.App;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import run.yang.common.util.basic.NumberTool;
import run.yang.common.util.compatibility.CharsetCompat;
import run.yang.common.util.io.file.Files;
import run.yang.common.util.log.Logg;
import run.yang.common.util.notif.ToastManager;
import run.yang.common.util.system.ProcFsTool;

/**
 * Created by Yang Tianmei on 2015-10-13.
 * <p/>
 *
 * @see <a href="http://man7.org/linux/man-pages/man5/proc.5.html" target="_blank">/proc file system manual page</a>
 */

class ProcFileSystemHelper {

    private static final String TAG = ProcFileSystemHelper.class.getSimpleName();

    /**
     * set to 100 for now. should obtain from native C code.
     * <p/>
     *
     * @see <a href="https://code.google.com/p/android-source-browsing/source/browse/libc/unistd/sysconf.c?spec=svn.platform--bionic.a985076bfe8c89f932fe33be45b887405c4fe7e3&repo=platform--bionic&r=a985076bfe8c89f932fe33be45b887405c4fe7e3" target="_blank">libc/unistd/sysconf.c</a>
     */
    public static final int SYSTEM_CLK_TCK = 100;

    /**
     * pid %d      (1) The process ID.
     */
    private static final int PID = 0;

    /**
     * utime %lu   (14) Amount of time that this process has been scheduled in user mode, measured in clock ticks (divide  by  sysconf(_SC_CLK_TCK)).   This  includes  guest
     * time, guest_time (time spent running a virtual CPU, see below), so that applications that are not aware of the guest time field do not lose that time from
     * their calculations.
     */
    private static final int UTIME = 13;

    /**
     * stime %lu   (15) Amount of time that this process has been scheduled in kernel mode, measured in clock ticks (divide by sysconf(_SC_CLK_TCK)).
     */
    private static final int STIME = 14;

    /**
     * cutime %ld  (16) Amount of time that this process's waited-for children have been scheduled in user mode, measured in clock ticks  (divide  by  sysconf(_SC_CLK_TCK)).
     * (See also times(2).)  This includes guest time, cguest_time (time spent running a virtual CPU, see below).
     */
    private static final int CUTIME = 15;

    /**
     * cstime %ld  (17) Amount of time that this process's waited-for children have been scheduled in kernel mode, measured in clock ticks (divide by sysconf(_SC_CLK_TCK)).
     */
    private static final int CSTIME = 16;

    /**
     * starttime %llu (was %lu before Linux 2.6)
     * (22) The time the process started after system boot.  In kernels before Linux 2.6, this value was expressed in jiffies.  Since Linux  2.6,  the  value  is
     * expressed in clock ticks (divide by sysconf(_SC_CLK_TCK)).
     */
    private static final int START_TIME = 21;

    static List<PerformanceMetricsBean> getPerformanceMetricsBeanList() {
        final File[] procPidPrefixFiles = ProcFsTool.getRunningProcessList();

        if (procPidPrefixFiles == null || procPidPrefixFiles.length == 0) {
            ToastManager.show("getRunningProcessInfoViaProcFS return empty or null");
            return null;
        }

        final List<PerformanceMetricsBean> perfList = getPerformanceMetricsList(procPidPrefixFiles);
        Logg.d(TAG, "ProcFileBean size: " + perfList.size());

        long uptimeMax = 0;
        for (PerformanceMetricsBean metrics : perfList) {
            if (uptimeMax < metrics.uptime) {
                uptimeMax = metrics.uptime;
            }
        }
        if (uptimeMax == 0) {
            uptimeMax = 1;
        }

        float usageSum = 0;
        for (PerformanceMetricsBean metrics : perfList) {
            metrics.cpuUsage = metrics.uptime / (float) uptimeMax * metrics.cpuUsage;
            usageSum += metrics.cpuUsage;
        }
        Logg.d(TAG, "cpu usage sum: " + usageSum);

        Collections.sort(perfList, new Comparator<PerformanceMetricsBean>() {
            @Override
            public int compare(PerformanceMetricsBean lhs, PerformanceMetricsBean rhs) {
                return Float.compare(rhs.cpuUsage, lhs.cpuUsage);
            }
        });

        return perfList;
    }

    @NonNull
    private static List<PerformanceMetricsBean> getPerformanceMetricsList(File[] procPidPrefixFiles) {
        Context context = App.context();
        final PackageManager pm = context.getPackageManager();
        final long systemUpTimeSeconds = SystemClock.elapsedRealtime() / 1000;
        final List<PerformanceMetricsBean> list = new ArrayList<>();

        for (File file : procPidPrefixFiles) {
            final String filePath = file.getAbsolutePath();

            PerformanceMetricsBean metrics = getPerformanceMetricsBean(pm, systemUpTimeSeconds, filePath);
            if (metrics == null) {
                continue;
            }

            list.add(metrics);
        }
        return list;
    }

    @Nullable
    private static PerformanceMetricsBean getPerformanceMetricsBean(
            @NonNull PackageManager pm,
            long systemUpTimeSeconds,
            @NonNull String filePathPrefix) {

        final int uid = ProcFsTool.getUid(filePathPrefix);
        if (uid == -1) {
            Logg.d(TAG, "cannot get uid for " + filePathPrefix);
            return null;
        }

        String cmdline = ProcFsTool.getCommandLine(filePathPrefix);
        if (!TextUtils.isEmpty(cmdline) && (cmdline.startsWith("/system/") || cmdline.startsWith("/sbin/"))) {
            Logg.d(TAG, "ignore " + filePathPrefix + ", cmdline: " + cmdline);
            return null;
        }

        final String[] packages = pm.getPackagesForUid(uid);
        if (packages == null || packages.length == 0) {
            if (packages != null) {
                Logg.d(TAG, "no package for: " + filePathPrefix + ", " + Arrays.toString(packages));
            }
            return null;
        }

        final String[] procStatParts = getProcStatParts(filePathPrefix);
        if (procStatParts == null) {
            Logg.d(TAG, "getListViewItems return null");
            return null;
        }

        PerformanceMetricsBean metrics = new PerformanceMetricsBean();
        metrics.pid = NumberTool.parseInt(procStatParts[PID]);
        metrics.utime = getField(procStatParts, UTIME) / SYSTEM_CLK_TCK;
        metrics.stime = getField(procStatParts, STIME) / SYSTEM_CLK_TCK;
        metrics.cutime = getField(procStatParts, CUTIME) / SYSTEM_CLK_TCK;
        metrics.cstime = getField(procStatParts, CSTIME) / SYSTEM_CLK_TCK;
        metrics.totalCpuTime = metrics.utime + metrics.stime + metrics.cutime + metrics.cstime;
        metrics.startTime = getField(procStatParts, START_TIME) / SYSTEM_CLK_TCK;
        metrics.uid = uid;
        metrics.packageName = selectPackage(packages, cmdline);
        metrics.uptime = systemUpTimeSeconds - metrics.startTime;
        if (metrics.uptime == 0) {
            metrics.uptime = 1;
        }
        metrics.cpuUsage = metrics.totalCpuTime / (float) metrics.uptime;

        if (packages.length > 1 && !TextUtils.equals(cmdline, metrics.packageName)) {
            Logg.d(TAG, Arrays.toString(packages));
            Logg.d(TAG, "pid: " + filePathPrefix + ", cmdline: " + cmdline + ", selected: " + metrics.packageName);
        }

        return metrics;
    }

    @Nullable
    static String[] getProcStatParts(@Nullable String filePathPrefix) {
        if (filePathPrefix == null) {
            ToastManager.show("getListViewItems: procPrefixFile is null");
            return null;
        }

        final File file = new File(filePathPrefix, "stat");
        if (!file.exists()) {
            ToastManager.show(file.getAbsolutePath() + " doesn't exist");
            Logg.d(TAG, file.getAbsolutePath() + " doesn't exist");
            return null;
        }
        String stat = null;
        try {
            stat = Files.readFirstLine(file, CharsetCompat.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            ToastManager.show(e.getMessage());
            return null;
        }

        if (TextUtils.isEmpty(stat)) {
            ToastManager.show(file.getAbsolutePath() + " is empty");
            Logg.d(TAG, file.getAbsolutePath() + " is empty");
            return null;
        }

        return stat.split(" ");
    }

    private static long getField(String[] procStatParts, int index) {
        if (procStatParts.length >= index) {
            return NumberTool.parseLong(procStatParts[index]);
        }
        return 0;
    }


    private static String selectPackage(@NonNull String[] packages, String expectedPackage) {
        if (packages.length == 1) {
            return packages[0];
        }

        if (!TextUtils.isEmpty(expectedPackage)) {
            for (String aPackage : packages) {
                if (expectedPackage.equals(aPackage)) {
                    return expectedPackage;
                }
            }
        }

        for (String aPackage : packages) {
            if (!TextUtils.isEmpty(aPackage)) {
                return aPackage;
            }
        }
        return null;
    }
}
