package com.zisxks.droiddemo.fragment.library.retrofit;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zisxks.droiddemo.R;
import com.zisxks.droiddemo.fragment.library.retrofit.github.Repo;

import java.net.UnknownHostException;
import java.util.List;

import androidx.annotation.Nullable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import run.yang.common.ui.recycler.adapter.TitleContentAdapter;
import run.yang.common.ui.recycler.view.DividerLinearRecyclerView;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.component.ContextTool;
import run.yang.common.util.exception.ExceptionTool;
import run.yang.common.util.view.V;

public class OkHttpRetrofitFragment extends BaseFragment {

    private DividerLinearRecyclerView mRecyclerView;
    private TextView mDescTextView;

    @Nullable
    @Override
    public View onCreateViewImpl(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_okhttp_retrofit, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = V.findView(view, R.id.recyclerView);
        mDescTextView = V.findView(view, R.id.text_description);
        mDescTextView.setMovementMethod(ScrollingMovementMethod.getInstance());
        loadRepos();
    }

    private void loadRepos() {
        final String userId = "square";
        Call<List<Repo>> listSquareRepoCall = ServiceGenerator.githubApi().listRepos(userId);
        listSquareRepoCall.enqueue(new Callback<List<Repo>>() {
            @Override
            public void onResponse(Call<List<Repo>> call, Response<List<Repo>> response) {
                if (response.isSuccessful()) {
                    TitleContentAdapter<Repo> adapter = new TitleContentAdapter<Repo>(mContextActivity, response.body()) {
                        @Override
                        protected void onBindView(ViewHolder holder, Repo item, int position) {
                            holder.title.setText(item.name);
                            holder.content.setText(item.description);

                            View.OnClickListener onClickListener = v -> {
                                if (!TextUtils.isEmpty(item.htmlUrl)) {
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.htmlUrl));
                                    ContextTool.startActivityChecked(mContext, intent);
                                }
                            };
                            holder.itemView.setOnClickListener(onClickListener);
                        }
                    };

                    mRecyclerView.setAdapter(adapter);
                    mDescTextView.setVisibility(View.GONE);
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                    mDescTextView.setText("response code: " + response.code() + "\nmessage: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Repo>> call, Throwable t) {
                mRecyclerView.setVisibility(View.GONE);
                StringBuilder sb = new StringBuilder("Failed, message:\n");
                if (t instanceof UnknownHostException) {
                    sb.append(ExceptionTool.getShortDescription(t));
                } else {
                    sb.append(Log.getStackTraceString(t));
                }
                mDescTextView.setText(sb);
            }
        });
    }
}
