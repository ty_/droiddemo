package com.zisxks.droiddemo.fragment.system.accessibility;


import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.zisxks.droiddemo.R;

import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.component.ContextTool;

public class CurrentActivityAccessibilityFragment extends BaseFragment implements View.OnClickListener {

    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_accessibility, container, false);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button button = (Button) view.findViewById(R.id.accssib_btn_open_system_settings);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.accssib_btn_open_system_settings:
                startSystemAccessibilitySettings();
                break;
        }
    }

    private void startSystemAccessibilitySettings() {
        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        ContextTool.startActivityChecked(mContextActivity, intent);
    }
}
