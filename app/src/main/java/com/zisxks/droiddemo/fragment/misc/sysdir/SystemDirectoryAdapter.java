package com.zisxks.droiddemo.fragment.misc.sysdir;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import run.yang.common.ui.recycler.adapter.TitleContentAdapter;

/**
 * Created by Yang Tianmei on 2015-09-04 16:04.
 */
public class SystemDirectoryAdapter extends TitleContentAdapter<SystemDirectoryBean> {

    public SystemDirectoryAdapter(@NonNull final Context context) {
        super(context, new ArrayList<>());

        new AsyncTask<Void, Void, List<SystemDirectoryBean>>() {
            @Override
            protected List<SystemDirectoryBean> doInBackground(final Void... params) {
                final List<SystemDirectoryBean> mapList = SystemDirectoryHelper.getSystemDirectories(context);
                SystemDirectoryHelper.sortByMethodName(mapList);
                return mapList;
            }

            @Override
            protected void onPostExecute(final List<SystemDirectoryBean> maps) {
                mDataList.clear();
                mDataList.addAll(maps);
                notifyDataSetChanged();
            }
        }.execute();
    }

    @Override
    protected void onBindView(ViewHolder holder, SystemDirectoryBean item, int position) {
        holder.title.setText(item.methodName);
        holder.content.setText(item.methodResult);
    }
}
