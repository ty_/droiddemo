package com.zisxks.droiddemo.fragment.misc.sysdir;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import run.yang.common.ui.recycler.view.DividerLinearRecyclerView;
import run.yang.common.ui.template.BaseFragment;

/**
 * Created by Tim Yang on 2015-04-23 14:51.
 */
public class SystemDirectoryFragment extends BaseFragment {

    private DividerLinearRecyclerView mRecyclerView;

    @Nullable
    @Override
    public View onCreateViewImpl(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        mRecyclerView = new DividerLinearRecyclerView(mContextActivity);
        return mRecyclerView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView.setAdapter(new SystemDirectoryAdapter(mContextActivity));
    }
}
