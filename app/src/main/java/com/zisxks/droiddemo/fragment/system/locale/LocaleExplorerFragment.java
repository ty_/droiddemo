package com.zisxks.droiddemo.fragment.system.locale;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.zisxks.droiddemo.R;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import run.yang.common.ui.recycler.adapter.SimpleTitleContentAdapter;
import run.yang.common.ui.recycler.adapter.TitleContentPair;
import run.yang.common.ui.simplified.SimpleOnItemSelectedListener;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.notif.ToastManager;
import run.yang.common.util.view.V;

public class LocaleExplorerFragment extends BaseFragment {

    private Spinner mLocaleSpinner;
    private RecyclerView mRecyclerView;
    private Locale[] mAvailableLocales;
    private SimpleTitleContentAdapter mLocaleDetailAdapter;

    public LocaleExplorerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_locale_explorer, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLocaleSpinner = V.findView(view, R.id.locale_spinner);
        mRecyclerView = V.findView(view, R.id.recyclerView);

        mAvailableLocales = Locale.getAvailableLocales();
        if (mAvailableLocales == null || mAvailableLocales.length == 0) {
            ToastManager.show(R.string.fragment_locale_no_available_locale);
            return;
        }

        ArrayAdapter<Locale> adapter = new ArrayAdapter<>(mContextActivity,
                R.layout.support_simple_spinner_dropdown_item, mAvailableLocales);
        mLocaleSpinner.setAdapter(adapter);

        List<TitleContentPair> localeDetail = LocaleExplorerHelper.getLocaleDetail(Locale.getDefault(), null);
        if (localeDetail == null) {
            localeDetail = Collections.emptyList();
        }
        mLocaleDetailAdapter = new SimpleTitleContentAdapter(mContextActivity, localeDetail);
        mRecyclerView.setAdapter(mLocaleDetailAdapter);

        mLocaleSpinner.setOnItemSelectedListener(new SimpleOnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                List<TitleContentPair> localeDetail = LocaleExplorerHelper.getLocaleDetail(mAvailableLocales[position], null);
                mLocaleDetailAdapter.setDataList(localeDetail);
                mLocaleDetailAdapter.notifyDataSetChanged();
            }
        });
    }
}
