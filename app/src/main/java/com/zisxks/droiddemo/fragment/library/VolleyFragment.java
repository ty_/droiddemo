package com.zisxks.droiddemo.fragment.library;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.zisxks.droiddemo.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.log.Logg;
import run.yang.common.util.view.V;

public class VolleyFragment extends BaseFragment {

    private static final String TEST_JSON_URL = "https://api.github.com/users/wuairc/repos";
    private static final String TAG = Logg.getTag(VolleyFragment.class);

    private TextView mTextView;

    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_volley, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mTextView = V.findView(view, R.id.textView);
        mTextView.setText("loading ...");

        makeRequest();
    }

    private void makeRequest() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContextActivity);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, TEST_JSON_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        StringBuilder sb = new StringBuilder();
                        int length = response.length();
                        for (int i = 0; i < length; i++) {
                            try {
                                JSONObject jsonObject = response.getJSONObject(i);
                                sb.append("full_name: ").append(jsonObject.optString("full_name")).append("\n");
                                sb.append("language: ").append(jsonObject.optString("language")).append("\n");
                                sb.append("size: ").append(jsonObject.optString("size")).append("\n");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            sb.append("\n");
                        }

                        mTextView.setText(sb);

                        Logg.d(TAG, sb.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String msg = error.toString();
                        if (error.networkResponse != null) {
                            msg += "\nstatus code: " + error.networkResponse.statusCode + ", headers: " + error.networkResponse.headers.toString();
                        }
                        Logg.d(TAG, msg);
                        mTextView.setText(msg);
                    }
                });

        requestQueue.add(jsonArrayRequest);
        requestQueue.start();
    }
}
