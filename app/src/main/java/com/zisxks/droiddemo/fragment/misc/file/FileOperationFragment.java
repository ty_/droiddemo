package com.zisxks.droiddemo.fragment.misc.file;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zisxks.droiddemo.R;

import run.yang.common.ui.template.BaseFragment;

public class FileOperationFragment extends BaseFragment implements View.OnClickListener {

    @Override
    public View onCreateViewImpl(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_file_operation, container, false);

        root.findViewById(R.id.file_btn_create_directory).setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.file_btn_create_directory:

                break;
        }
    }
}
