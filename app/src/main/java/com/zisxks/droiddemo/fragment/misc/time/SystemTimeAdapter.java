package com.zisxks.droiddemo.fragment.misc.time;

import android.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import run.yang.common.util.view.V;

/**
 * Created by Yang Tianmei on 2015-07-24 18:33.
 */
public class SystemTimeAdapter extends BaseAdapter {

    private final List<SystemTimeHelper.TimeClockViewer> mClockList;
    private final LayoutInflater mLayoutInflater;

    public SystemTimeAdapter(@NonNull Context context, @NonNull List<SystemTimeHelper.TimeClockViewer> clockList) {
        mClockList = clockList;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mClockList.size();
    }

    @Override
    public Object getItem(int position) {
        return mClockList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.simple_list_item_2, parent, false);
            holder = new ViewHolder();
            holder.titleView = V.findView(convertView, R.id.text1);
            holder.contentView = V.findView(convertView, R.id.text2);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final SystemTimeHelper.TimeClockViewer clock = (SystemTimeHelper.TimeClockViewer) getItem(position);

        holder.titleView.setText(clock.getClockName());
        holder.contentView.setText(clock.getTimePresentation());

        return convertView;
    }

    private static class ViewHolder {
        TextView titleView;
        TextView contentView;
    }
}
