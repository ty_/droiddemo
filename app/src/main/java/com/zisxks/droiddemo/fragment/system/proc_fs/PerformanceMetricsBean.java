package com.zisxks.droiddemo.fragment.system.proc_fs;

/**
 * Created by Yang Tianmei on 2015-10-14.
 */
public class PerformanceMetricsBean {
    public String packageName;
    public int pid;
    public int uid;
    public long utime;  // in seconds
    public long stime;  // in seconds
    public long cutime;  // in seconds
    public long cstime;  // in seconds
    public long totalCpuTime;  // in seconds
    public long uptime; // in seconds
    public long startTime;  // in seconds
    public float cpuUsage; // 0.1 stand for 10%
}
