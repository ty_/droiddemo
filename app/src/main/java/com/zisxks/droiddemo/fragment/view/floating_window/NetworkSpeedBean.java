package com.zisxks.droiddemo.fragment.view.floating_window;

/**
 * Created by Yang Tianmei on 2015-09-22.
 */
public class NetworkSpeedBean {
    public long rxBytesTotal = 0;
    public long txBytesTotal = 0;

    public NetworkSpeedBean() {
    }

    public NetworkSpeedBean(NetworkSpeedBean bean) {
        initWith(bean);
    }

    public void initWith(NetworkSpeedBean bean) {
        if (bean != null) {
            this.rxBytesTotal = bean.rxBytesTotal;
            this.txBytesTotal = bean.txBytesTotal;
        }
    }

    @Override
    public String toString() {
        return "NetworkSpeedBean{" +
                "rxBytesTotal=" + rxBytesTotal +
                ", txBytesTotal=" + txBytesTotal +
                '}';
    }
}
