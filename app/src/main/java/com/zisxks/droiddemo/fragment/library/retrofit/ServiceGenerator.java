package com.zisxks.droiddemo.fragment.library.retrofit;

import com.zisxks.droiddemo.fragment.library.retrofit.github.GithubService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by zisxks on 2016-05-11 17:26
 */
public class ServiceGenerator {
    private static GithubService sGithubService;

    public static GithubService githubApi() {
        if (sGithubService == null) {
            synchronized (GithubService.class) {
                if (sGithubService == null) {
                    Retrofit githubRetrofit = new Retrofit.Builder()
                            .baseUrl("https://api.github.com/")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    sGithubService = githubRetrofit.create(GithubService.class);
                }
            }
        }
        return sGithubService;
    }
}
