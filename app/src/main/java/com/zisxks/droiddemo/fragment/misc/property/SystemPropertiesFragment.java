package com.zisxks.droiddemo.fragment.misc.property;


import android.os.Bundle;
import android.view.View;

import com.zisxks.droiddemo.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import androidx.core.content.res.ResourcesCompat;
import run.yang.common.ui.template.BaseListFragment;

public class SystemPropertiesFragment extends BaseListFragment {

    private static final String TAG = SystemPropertiesFragment.class.getSimpleName();


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final TreeMap<String, String> treeMap = SystemPropertiesHelper.getSystemProperties();
        final List<PropertyBean> list = new ArrayList<>(treeMap.size());

        for (Map.Entry<String, String> entry : treeMap.entrySet()) {
            PropertyBean bean = new PropertyBean();
            bean.propertyName = entry.getKey();
            bean.propertyValue = entry.getValue();
            list.add(bean);
        }

        setListAdapter(new SystemPropertiesAdapter(mContextActivity, list));

        getListView().setDivider(ResourcesCompat.getDrawable(getResources(), R.drawable.recycle_view_line_divider, null));
        getListView().setDividerHeight(5);
    }
}
