package com.zisxks.droiddemo.fragment.misc.property;

import android.text.TextUtils;

import java.util.Map;
import java.util.TreeMap;

import androidx.annotation.NonNull;
import run.yang.common.util.basic.TextTool;
import run.yang.common.util.system.PropertyTool;

/**
 * Created by Yang Tianmei on 2015-09-09 21:18.
 */
class SystemPropertiesHelper {
    @NonNull
    static TreeMap<String, String> getSystemProperties() {

        final TreeMap<String, String> properties = PropertyTool.getSystemProperties();

        prettifyString(properties);
        breakClassPath(properties);

        return properties;
    }

    private static void breakClassPath(@NonNull TreeMap<String, String> map) {
        final String key = "java.boot.class.path";
        final String value = map.get(key);
        if (!TextUtils.isEmpty(value)) {
            final String newValue = value.replaceAll(":", "\n");
            map.put(key, newValue);
        }
    }

    private static void prettifyString(TreeMap<String, String> properties) {
        for (Map.Entry<String, String> entry : properties.entrySet()) {
            final String prettified = TextTool.toReadableString(entry.getValue());
            entry.setValue(prettified);
        }
    }
}
