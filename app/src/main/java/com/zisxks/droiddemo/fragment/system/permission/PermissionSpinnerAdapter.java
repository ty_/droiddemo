package com.zisxks.droiddemo.fragment.system.permission;

import android.R;
import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import run.yang.common.util.view.V;

/**
 * Created by Yang Tianmei on 2015-09-08 17:40.
 */
public class PermissionSpinnerAdapter extends BaseAdapter {

    private static final String TAG = PermissionSpinnerAdapter.class.getSimpleName();

    private final LayoutInflater mLayoutInflater;
    private final List<Pair<String, String>> mList;

    public PermissionSpinnerAdapter(@NonNull Context context, @NonNull List<Pair<String, String>> list) {
        mLayoutInflater = LayoutInflater.from(context);
        mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(final int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.simple_spinner_dropdown_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Pair<String, String> pair = mList.get(position);
        holder.mTextView.setText(pair.first);

        return convertView;
    }

    private static class ViewHolder {
        public TextView mTextView;

        public ViewHolder(final View view) {
            mTextView = V.findView(view, R.id.text1);
        }
    }
}
