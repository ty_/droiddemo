package com.zisxks.droiddemo.init.lifecycle;

import android.os.Bundle;

import com.zisxks.droiddemo.fragment.lifecycle.LifecyclePagerItemFragment;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Locale;

import androidx.fragment.app.Fragment;
import run.yang.common.ui.lifecycle.fragment.FragmentLifecycleCallback;
import run.yang.common.util.log.Logg;

/**
 * 创建时间: 2016/11/24 15:41 <br>
 * 作者: Yang Tianmei <br>
 * 描述:
 */

public class FragmentLifecycleCallbackFactory {
    public static FragmentLifecycleCallback newFragmentLifecycleCallback() {
        final ClassLoader classLoader = FragmentLifecycleCallbackFactory.class.getClassLoader();
        Class[] classes = {FragmentLifecycleCallback.class};
        return (FragmentLifecycleCallback) Proxy.newProxyInstance(classLoader, classes,
                new LogMethodNameInvocationHandler());
    }

    private static class LogMethodNameInvocationHandler implements InvocationHandler {
        private static final String TAG = "LogMethodNameInvocation";

        @Override
        public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
            final String args = Arrays.toString(Arrays.copyOfRange(objects, 1, objects.length));
            final String fragmentName = objects[0].getClass().getSimpleName();
            final String methodName = method.getName();
            int index = -1;

            Fragment fragment = (Fragment) objects[0];
            Bundle arguments = fragment.getArguments();
            if (arguments != null) {
                index = arguments.getInt(LifecyclePagerItemFragment.EXTRA_INDEX, -1);
            }

            if (index != -1) {
                Logg.d(TAG, format("%s(%d).%s(%s)", fragmentName, index, methodName, args));
            } else {
                Logg.d(TAG, format("%s.%s(%s)", fragmentName, methodName, args));
            }
            return null;
        }

        private static String format(String pattern, Object... args) {
            return String.format(Locale.ROOT, pattern, args);
        }
    }
}
