package com.zisxks.droiddemo.init;

import run.yang.common.util.build.BuildControl;
import run.yang.common.util.sdk.CommonSdkDependencyDefaultImpl;

/**
 * 创建时间: 2016/08/16 14:37 <br>
 * 作者: zisxks <br>
 * 描述:
 */
public class CommonSdkDependencyImpl extends CommonSdkDependencyDefaultImpl {
    @Override
    public boolean isDebug() {
        return BuildControl.isDebug;
    }
}
