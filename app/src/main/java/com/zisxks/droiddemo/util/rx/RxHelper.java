package com.zisxks.droiddemo.util.rx;

import androidx.annotation.Nullable;
import rx.Subscription;

/**
 * Created by Yang Tianmei on 2016-03-16 19:11.
 */
public class RxHelper {

    public static void unsubscribe(@Nullable Subscription subscription) {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }
}
