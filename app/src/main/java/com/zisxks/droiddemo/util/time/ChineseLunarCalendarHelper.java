package com.zisxks.droiddemo.util.time;

import android.icu.util.Calendar;
import android.icu.util.ChineseCalendar;
import android.util.SparseArray;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import run.yang.common.util.android.Version;

/**
 * Created by ty on 1/15/17.
 */
@RequiresApi(api = Version.V24_70)
public class ChineseLunarCalendarHelper {

    private static final int YEAR_DIFFERENCE = 2637;
    private static final SparseArray<String> MONTH_NAME_MAP = new SparseArray<>(12);
    private static final String[] DAY_NAMES;

    static {
        MONTH_NAME_MAP.put(Calendar.JANUARY, "正月");
        MONTH_NAME_MAP.put(Calendar.FEBRUARY, "二月");
        MONTH_NAME_MAP.put(Calendar.MARCH, "三月");
        MONTH_NAME_MAP.put(Calendar.APRIL, "四月");
        MONTH_NAME_MAP.put(Calendar.MAY, "五月");
        MONTH_NAME_MAP.put(Calendar.JUNE, "六月");
        MONTH_NAME_MAP.put(Calendar.JULY, "七月");
        MONTH_NAME_MAP.put(Calendar.AUGUST, "八月");
        MONTH_NAME_MAP.put(Calendar.SEPTEMBER, "九月");
        MONTH_NAME_MAP.put(Calendar.OCTOBER, "十月");
        MONTH_NAME_MAP.put(Calendar.NOVEMBER, "冬月");
        MONTH_NAME_MAP.put(Calendar.DECEMBER, "腊月");
        MONTH_NAME_MAP.put(Calendar.UNDECIMBER, "十三月"); // should not happen

        final String[] days = {
                "初一", "初二", "初三", "初四", "初五", "初六", "初七", "初八", "初九", "初十",
                "十一", "十二", "十三", "十四", "十五", "十六", "十七", "十八", "十九", "二十",
                "廿一", "廿二", "廿三", "廿四", "廿五", "廿六", "廿七", "廿八", "廿九", "三十"
        };
        DAY_NAMES = days;
    }

    public static String getMonthName(@NonNull ChineseCalendar calendar) {
        boolean isLeapMonth = calendar.get(Calendar.IS_LEAP_MONTH) != 0;
        String monthName = MONTH_NAME_MAP.get(calendar.get(Calendar.MONTH));
        if (isLeapMonth) {
            monthName = '闰' + monthName;
        }
        return monthName;
    }

    public static String getDayName(@NonNull ChineseCalendar calendar) {
        final int day = calendar.get(Calendar.DAY_OF_MONTH);

        if (day <= 0 || day > 30) {
            throw new IllegalArgumentException("day must between 1 and 30");
        }
        return DAY_NAMES[day - 1];
    }

    public static int getYear(@NonNull ChineseCalendar calendar) {
        final int extendedYear = calendar.get(Calendar.EXTENDED_YEAR);
        return extendedYear - YEAR_DIFFERENCE;
    }
}
