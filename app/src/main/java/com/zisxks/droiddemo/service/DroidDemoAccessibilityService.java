package com.zisxks.droiddemo.service;

import android.accessibilityservice.AccessibilityService;
import android.annotation.TargetApi;
import android.content.Intent;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.zisxks.droiddemo.R;
import com.zisxks.droiddemo.constant.NotifyId;

import run.yang.common.util.android.Version;
import run.yang.common.util.build.BuildControl;
import run.yang.common.util.debug.CurrentActivityNotification;
import run.yang.common.util.log.Logg;

@TargetApi(Version.V14_40)
public class DroidDemoAccessibilityService extends AccessibilityService {

    private static final String TAG = DroidDemoAccessibilityService.class.getSimpleName();
    private static final boolean DO_LOG = BuildControl.doLog;

    private CurrentActivityNotification mNotification;

    public DroidDemoAccessibilityService() {
        super();
    }

    @Override
    public void onAccessibilityEvent(final AccessibilityEvent event) {
        String packageName = String.valueOf(event.getPackageName());
        String className = String.valueOf(event.getClassName());
        if (DO_LOG) Logg.d(TAG, "class name: " + className);

        boolean isClassActivity = isClassActivity(packageName, className);
        if (!isClassActivity) {
            if (DO_LOG) Logg.d(TAG, "not an activity instance");
            return;
        }
        if (mNotification == null) {
            mNotification = new CurrentActivityNotification(
                    this, R.drawable.ic_launcher, NotifyId.FUNC_ACCESSIBILITY_CURRENT_ACTIVITY_NAME);
        }

        if (DO_LOG) {
            StringBuilder builder = new StringBuilder("onAccessibilityEvent: ");
            builder.append("EventType: ").append(AccessibilityEvent.eventTypeToString(event.getEventType()));
            builder.append("; PackageName: ").append(packageName);
            builder.append("; ClassName: ").append(className);
            builder.append("; Text: ").append(event.getText());
            builder.append("; SourceText: ").append(getSourceText(event));
            Logg.d(TAG, builder.toString());
        }

        mNotification.show(packageName, className);
    }

    private static boolean isClassActivity(String packageName, String className) {
        if (TextUtils.isEmpty(packageName) || TextUtils.isEmpty(className)) {
            return false;
        }
        return !className.startsWith("android.");
    }

    private CharSequence getSourceText(final AccessibilityEvent event) {
        AccessibilityNodeInfo source = event.getSource();
        if (source == null) {
            return null;
        }

        CharSequence text = source.getText();
        source.recycle();
        return text;
    }

    @Override
    protected boolean onKeyEvent(final KeyEvent event) {
        if (DO_LOG) Logg.d(TAG, "onKeyEvent: " + event.toString());
        return super.onKeyEvent(event);
    }

    @Override
    protected boolean onGesture(final int gestureId) {
        if (DO_LOG) Logg.d(TAG, "onGesture: " + gestureId);
        return super.onGesture(gestureId);
    }

    @Override
    public void onInterrupt() {
        if (DO_LOG) Logg.d(TAG, "onInterrupt");
    }

    @Override
    protected void onServiceConnected() {
        if (DO_LOG) Logg.d(TAG, "onServiceConnected");
        super.onServiceConnected();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        if (DO_LOG) Logg.d(TAG, "onUnbind");
        return super.onUnbind(intent);
    }
}
