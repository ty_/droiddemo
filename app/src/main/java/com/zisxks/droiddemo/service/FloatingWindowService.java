package com.zisxks.droiddemo.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.zisxks.droiddemo.context.App;
import com.zisxks.droiddemo.fragment.view.floating_window.FloatingWindowWidget;

public class FloatingWindowService extends Service {

    public static boolean startService() {
        Context context = App.context();
        Intent service = new Intent(context, FloatingWindowService.class);
        context.startService(service);
        return true;
    }

    public static boolean stopService() {
        Context context = App.context();
        return context.stopService(new Intent(context, FloatingWindowService.class));
    }

    private FloatingWindowWidget mFloatingWindow;

    @Override
    public void onCreate() {
        super.onCreate();

        mFloatingWindow = new FloatingWindowWidget();
        mFloatingWindow.start(this.getApplicationContext());
    }

    @Override
    public void onDestroy() {
        if (mFloatingWindow != null) {
            mFloatingWindow.destroy();
            mFloatingWindow = null;
        }

        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
