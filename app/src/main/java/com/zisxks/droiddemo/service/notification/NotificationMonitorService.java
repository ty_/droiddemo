package com.zisxks.droiddemo.service.notification;

import android.annotation.TargetApi;
import android.os.Build;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import run.yang.common.util.log.Logg;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class NotificationMonitorService extends NotificationListenerService {

    private static final String TAG = Logg.getTag(NotificationMonitorService.class);

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        Logg.w(TAG, "onNotificationPosted: " + sbn.getNotification());
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        Logg.w(TAG, "onNotificationRemoved: " + sbn.getNotification());
    }
}
