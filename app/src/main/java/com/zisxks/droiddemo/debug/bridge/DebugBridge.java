package com.zisxks.droiddemo.debug.bridge;

import androidx.annotation.Nullable;
import run.yang.common.util.build.BuildControl;

/**
 * Created by toybox on 11/1/15.
 */
public class DebugBridge {

    private static final class SingletonHolder {
        public static IDebugBridge sInstance = !BuildControl.isRelease
                ? new DebugBridgeImpl() : null;
    }

    @Nullable
    public static IDebugBridge getBridge() {
        return SingletonHolder.sInstance;
    }
}
