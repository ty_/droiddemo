package com.zisxks.droiddemo.debug.bridge;

import android.app.Application;

import androidx.annotation.NonNull;

/**
 * Created by toybox on 11/1/15.
 */
public interface IDebugBridge {

    /**
     * enbale {@link android.os.StrictMode}, check for network access on main thread, ANR, etc.
     */
    void initStrictMode(@NonNull Application application);

    /**
     * enable activity lifecycle callbac, for show current activity class name in notification bar.
     */
    void initActivityLifecycleCallback(@NonNull Application application);


}
