package com.zisxks.droiddemo.debug.bridge;

import android.app.Application;

import com.zisxks.droiddemo.R;
import com.zisxks.droiddemo.constant.NotifyId;
import com.zisxks.droiddemo.debug.strictmode.StrictModeSetup;

import androidx.annotation.NonNull;
import run.yang.common.util.debug.DebugTool;

/**
 * Created by toybox on 11/1/15.
 */
class DebugBridgeImpl implements IDebugBridge {

    @Override
    public void initStrictMode(@NonNull Application application) {
        StrictModeSetup.init();
    }

    @Override
    public void initActivityLifecycleCallback(@NonNull Application application) {
        DebugTool.registerActivityLifecycleCallbacks(application, NotifyId.DEBUG_SHOW_ACTIVITY_NAME, R.drawable.ic_launcher);
    }
}
