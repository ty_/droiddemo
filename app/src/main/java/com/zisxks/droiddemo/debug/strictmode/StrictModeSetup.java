package com.zisxks.droiddemo.debug.strictmode;

import android.os.StrictMode;

import run.yang.common.util.android.Version;

/**
 * Created by toybox on 11/1/15.
 */
public class StrictModeSetup {
    public static void init() {
        initThreadPolicy();
        initVmPolicy();
    }

    private static void initThreadPolicy() {
        final StrictMode.ThreadPolicy.Builder threadPolicyBuilder = new StrictMode.ThreadPolicy.Builder();

        threadPolicyBuilder.detectAll()
                .permitDiskReads()
                .permitDiskWrites()
                .penaltyLog()
                .penaltyDialog()
//                .penaltyDropBox()
//                .penaltyDeath()
        ;

        if (Version.atLeast(Version.V11_30)) {
            threadPolicyBuilder.penaltyFlashScreen();
        }
        StrictMode.setThreadPolicy(threadPolicyBuilder.build());
    }

    private static void initVmPolicy() {
        final StrictMode.VmPolicy.Builder vmPolicyBuilder = new StrictMode.VmPolicy.Builder();

        vmPolicyBuilder.detectLeakedSqlLiteObjects();

        if (Version.atLeast(Version.V11_30)) {
            vmPolicyBuilder.detectActivityLeaks()
                    .detectLeakedClosableObjects();
        }
        if (Version.atLeast(Version.V16_41)) {
            vmPolicyBuilder.detectLeakedRegistrationObjects();
        }
        if (Version.atLeast(Version.V18_43)) {
            vmPolicyBuilder.detectFileUriExposure();
        }

        vmPolicyBuilder.penaltyLog()
//                .penaltyDropBox()
//                .penaltyDeath()
        ;
        StrictMode.setVmPolicy(vmPolicyBuilder.build());
    }
}
