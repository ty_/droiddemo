package com.zisxks.droiddemo.debug.bridge;

import android.app.Application;

import androidx.annotation.NonNull;
import run.yang.common.util.build.BuildControl;

/**
 * Created by zisxks on 11/1/15.
 */
public class DebugConfigManager {

    public static void init(@NonNull Application application) {
        if (BuildControl.isRelease) {
            return;
        }

        IDebugBridge bridge = DebugBridge.getBridge();
        if (bridge == null) {
            return;
        }

        bridge.initStrictMode(application);
    }
}
