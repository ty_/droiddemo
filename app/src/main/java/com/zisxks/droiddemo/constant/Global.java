package com.zisxks.droiddemo.constant;

/**
 * Created by Tim Yang on 2015-05-14 17:54
 * <p/>
 * Function:
 */
public final class Global {
    public static final String PACKAGE_PREFIX = getPackagePrefix();
    public static final String ACTION_PREFIX = PACKAGE_PREFIX + "action.";
    public static final String LOCAL_ACTION_PREFIX = ACTION_PREFIX + "local.";

    private static String getPackagePrefix() {
        String packageName = Global.class.getPackage().getName();
        int index = packageName.lastIndexOf('.');
        if (index > 0) {
            // preserve last dot
            packageName = packageName.substring(0, index + 1);
        }
        return packageName;
    }
}
