package com.zisxks.droiddemo.constant;

/**
 * IDs for status bar notifications
 * <p>
 * Created by Yang Tianmei on 2015-09-10 11:22.
 */
public final class NotifyId {

    public static final int DEBUG_SHOW_ACTIVITY_NAME = 10;

    public static final int FUNC_ACCESSIBILITY_CURRENT_ACTIVITY_NAME = 100;
    public static final int FUNC_SELF_UPDATED = 101;
    public static final int FUNC_BACKSTACK_NOTIFICATION = 102;
}
