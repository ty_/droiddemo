package com.zisxks.droiddemo.constant;

/**
 * Created by Yang Tianmei on 2015-09-15 14:48.
 * <p>
 * Permission request code
 */
public final class PermissionRequest {
    public static final int CODE_COARSE_LOCATION = 1;
    public static final int CODE_READ_CONTACTS = 2;
}
