package com.zisxks.droiddemo.context;

import android.app.Application;
import android.content.res.Configuration;

import com.zisxks.droiddemo.app.noti.NotiChannels;
import com.zisxks.droiddemo.debug.bridge.DebugConfigManager;
import com.zisxks.droiddemo.init.CommonSdkDependencyImpl;
import com.zisxks.droiddemo.init.lifecycle.FragmentLifecycleCallbackFactory;

import java.text.Collator;
import java.util.Locale;

import androidx.core.os.ConfigurationCompat;
import androidx.core.os.LocaleListCompat;
import run.yang.common.ui.template.BaseFragment;
import run.yang.common.util.sdk.CommonUtilSdk;


/**
 * Created by Tim Yang on 2015-05-14 19:57
 * <p>
 */
public class App extends Application {

    private static App sInstance;

    /**
     * Locale at startup is user selected locale. During runtime, program may change default locale
     * by calling {@link Locale#setDefault(Locale)}
     */
    private Locale mUserSelectedLocale = Locale.getDefault();

    public static App context() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

//        hook();

        CommonUtilSdk.init(this, new CommonSdkDependencyImpl(), ProcessCheck.isBg());
        DebugConfigManager.init(this);

        BaseFragment.setLifecycleCallback(FragmentLifecycleCallbackFactory.newFragmentLifecycleCallback());

        NotiChannels.createNotiChannels(this);
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        final LocaleListCompat locales = ConfigurationCompat.getLocales(newConfig);
        if (!locales.isEmpty()) {
            mUserSelectedLocale = locales.get(0);
        }
    }

    /**
     * get user selected locale, or current locale
     */
    public Locale getUserLocale() {
        return mUserSelectedLocale;
    }

    public Collator newCollatorWithUserSelectedLocale() {
        return Collator.getInstance(mUserSelectedLocale);
    }

//    public static void hook() {
//        DexposedBridge.findAndHookMethod(NotificationManager.class, "cancel",
//            String.class, int.class, new XC_MethodHook() {
//                @Override
//                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
//                    super.afterHookedMethod(param);
//
//                    System.out.println(param);
//
//                    new Throwable().printStackTrace();
//                }
//            });
//    }
}
