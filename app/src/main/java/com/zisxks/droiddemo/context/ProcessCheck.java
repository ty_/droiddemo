package com.zisxks.droiddemo.context;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import run.yang.common.util.basic.TextTool;
import run.yang.common.util.build.BuildControl;
import run.yang.common.util.log.Logg;
import run.yang.common.util.system.ProcessTool;

/**
 * Created by Tim Yang on 2016-03-29 17:18.
 */
public class ProcessCheck {

    private static final String TAG = Logg.getTag(ProcessCheck.class);

    private static final int PROC_UNKNOWN = -1;
    private static final int PROC_UI = 1;
    private static final int PROC_BG = 2;

    @Retention(RetentionPolicy.CLASS)
    @IntDef({
            PROC_UNKNOWN,
            PROC_UI,
            PROC_BG,
    })
    public @interface ProcessId {
    }

    private static String sThisProcessName;

    @ProcessId
    private static int sThisProcessId;

    static {
        sThisProcessName = ProcessTool.currentProcessName(App.context());
        if (sThisProcessName == null) {
            sThisProcessName = "";
        }

        String suffix = TextTool.lastPartInclude(sThisProcessName, ':');
        sThisProcessId = processSuffixToProcessId(suffix);
    }

    @NonNull
    public static String currentProcessName() {
        return sThisProcessName;
    }

    @ProcessId
    public static int currentProcessId() {
        return sThisProcessId;
    }

    public static boolean isUi() {
        return sThisProcessId == PROC_UI;
    }

    public static boolean isBg() {
        return sThisProcessId == PROC_BG;
    }

    @ProcessId
    private static int processSuffixToProcessId(String processSuffix) {
        switch (processSuffix) {
            case ":ui":
                return PROC_UI;

            case ":bg":
                return PROC_BG;

            default:
                if (BuildControl.isDebug) {
                    Logg.w(TAG, "Unknown process id: " + processSuffix);
                    throw new RuntimeException("Unknown process id: " + processSuffix);
                }
                return PROC_UNKNOWN;
        }
    }
}
