package com.zisxks.droiddemo.app.noti;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

/**
 * Date: 2018-05-14 20:16 <br >
 * Author: 42<wuairc@gmail.com> <br >
 * Description:  <br >
 */
public class NotiChannels {

    public static void createNotiChannels(@NonNull Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createDefaultNotificationChannel(context);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private static void createDefaultNotificationChannel(@NonNull Context context) {
        final NotificationManager nm = context.getSystemService(NotificationManager.class);
        assert nm != null;

        NotificationChannel channel = new NotificationChannel(NotiChannelId.DEFAULT, "Default", NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription("Default Notification Type");

        nm.createNotificationChannel(channel);
    }
}
