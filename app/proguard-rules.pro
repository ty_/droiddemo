# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in E:\Portable\AndroidSDK/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# google play service
-dontwarn sun.misc.Unsafe

## okio
-dontwarn okio.**

## Retrofit2
# http://square.github.io/retrofit/
# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions

# OkHttp3
# https://github.com/square/okhttp
-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn javax.annotation.**
# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase


# retrolambda
-dontwarn java.lang.invoke.*

# android support library
-dontwarn android.support.**
-keep class android.support.** { *; }
-keep interface android.support.** { *; }


# logging, remove verbose, debug level log
# see http://www.trinea.cn/android/proguard-grammar/
-assumenosideeffects class run.yang.common.util.log.Logg {
    public static *** d(...);
    public static *** v(...);
}
-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** v(...);
}

# Guava
# https://github.com/google/guava/wiki/UsingProGuardWithGuava
-dontwarn sun.misc.Unsafe
-dontwarn com.google.common.collect.MinMaxPriorityQueue

-keepclasseswithmembers public class * {
    public static void main(java.lang.String[]);
}
-dontwarn afu.org.checkerframework.checker.**
-dontwarn org.checkerframework.checker.**
-dontwarn java.lang.**
-dontwarn javax.**
-dontwarn android.os.**
-dontwarn dalvik.**
