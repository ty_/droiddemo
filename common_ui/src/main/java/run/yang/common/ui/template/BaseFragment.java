package run.yang.common.ui.template;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import run.yang.common.ui.lifecycle.fragment.FragmentLifecycleCallback;
import run.yang.common.util.android.Version;
import run.yang.common.util.dynamicproxy.NullSafeCallback;

/**
 * Created by Tim Yang on 2015-04-27 21:22.
 */
public abstract class BaseFragment extends Fragment {
    private static final FragmentLifecycleCallback sLifecycleCallback =
            NullSafeCallback.wrap(FragmentLifecycleCallback.class);

    protected Activity mContextActivity;

    public static FragmentLifecycleCallback setLifecycleCallback(
            FragmentLifecycleCallback callback) {
        return NullSafeCallback.update(sLifecycleCallback, callback);
    }

    @CallSuper
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Version.atMost(Version.V22_51)) {
            mContextActivity = getActivity();
            sLifecycleCallback.onAttach(this);
        }
    }

    @CallSuper
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContextActivity = getActivity();
        sLifecycleCallback.onAttach(this);
    }

    @CallSuper
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sLifecycleCallback.onCreate(this, savedInstanceState);
    }

    public abstract View onCreateViewImpl(LayoutInflater inflater, @Nullable ViewGroup container,
                                          @Nullable Bundle savedInstanceState);

    @Nullable
    @Override
    public final View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                                   @Nullable Bundle savedInstanceState) {
        sLifecycleCallback.beforeCreateView(this, inflater, container, savedInstanceState);
        View view = onCreateViewImpl(inflater, container, savedInstanceState);
        sLifecycleCallback.afterCreateView(this, inflater, container, savedInstanceState);

        return view;
    }

    @CallSuper
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sLifecycleCallback.onViewCreated(this, view, savedInstanceState);
    }

    @CallSuper
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sLifecycleCallback.onActivityCreated(this, savedInstanceState);
    }

    @CallSuper
    @Override
    public void onStart() {
        super.onStart();

        sLifecycleCallback.onStart(this);
    }

    @CallSuper
    @Override
    public void onResume() {
        super.onResume();

        sLifecycleCallback.onResume(this);
    }

    @CallSuper
    @Override
    public void onPause() {
        super.onPause();

        sLifecycleCallback.onPause(this);
    }

    @CallSuper
    @Override
    public void onStop() {
        super.onStop();

        sLifecycleCallback.onStop(this);
    }

    @CallSuper
    @Override
    public void onDestroyView() {
        super.onDestroyView();

        sLifecycleCallback.onDestroyView(this);
    }

    @CallSuper
    @Override
    public void onDestroy() {
        super.onDestroy();

        sLifecycleCallback.onDestroy(this);
    }

    @Override
    public void onDetach() {
        mContextActivity = null;
        super.onDetach();

        sLifecycleCallback.onDetach(this);
    }
}
