package run.yang.common.ui.recycler.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import run.yang.common.ui.R;


/**
 * Created by Yang Tianmei on 2015-08-13 12:12.
 */
public class RecyclerViewDividerItemDecoration extends RecyclerView.ItemDecoration {

    private Drawable mDivider;

    public RecyclerViewDividerItemDecoration(@NonNull Context context) {
        mDivider = ContextCompat.getDrawable(context, R.drawable.recycle_view_line_divider);
    }

    @Override
    public void onDraw(final Canvas c, final RecyclerView parent, final RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }

    @Override
    public void getItemOffsets(final Rect outRect, final View view, final RecyclerView parent, final RecyclerView.State state) {
        outRect.bottom = mDivider.getIntrinsicHeight();
    }
}
