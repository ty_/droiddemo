package run.yang.common.ui.simplified;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

/**
 * Created by Yang Tianmei on 2015-09-15 16:39.
 */
public class SimpleLocationListener implements LocationListener {
    @Override
    public void onLocationChanged(final Location location) {
    }

    @Override
    public void onStatusChanged(final String provider, final int status, final Bundle extras) {
    }

    @Override
    public void onProviderEnabled(final String provider) {
    }

    @Override
    public void onProviderDisabled(final String provider) {
    }
}
