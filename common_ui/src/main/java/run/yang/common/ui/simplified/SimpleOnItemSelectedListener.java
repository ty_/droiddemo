package run.yang.common.ui.simplified;

import android.view.View;
import android.widget.AdapterView;

/**
 * Created by Yang Tianmei on 2016-03-24 12:10.
 */
public class SimpleOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}
