package run.yang.common.ui.simplified;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.os.Build;

/**
 * Created by Yang Tianmei on 2015-10-21.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SimpleAnimatorListener implements Animator.AnimatorListener {
    @Override
    public void onAnimationStart(Animator animation) {
    }

    @Override
    public void onAnimationEnd(Animator animation) {
    }

    @Override
    public void onAnimationCancel(Animator animation) {
    }

    @Override
    public void onAnimationRepeat(Animator animation) {
    }
}
