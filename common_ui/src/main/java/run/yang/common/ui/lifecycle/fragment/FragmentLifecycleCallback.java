package run.yang.common.ui.lifecycle.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import run.yang.common.ui.template.BaseFragment;


/**
 * 创建时间: 2016/11/24 12:01 <br>
 * 作者: ty <br>
 * 描述:
 */

public interface FragmentLifecycleCallback {

    void onAttach(BaseFragment fragment);

    void onCreate(BaseFragment fragment, Bundle savedInstanceState);

    void beforeCreateView(BaseFragment fragment, LayoutInflater inflater,
                          @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);

    void afterCreateView(BaseFragment fragment, LayoutInflater inflater,
                         @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);

    void onViewCreated(BaseFragment fragment, View view, @Nullable Bundle savedInstanceState);

    void onActivityCreated(BaseFragment fragment, Bundle savedInstanceState);

    void onStart(BaseFragment fragment);

    void onResume(BaseFragment fragment);

    void onPause(BaseFragment fragment);

    void onStop(BaseFragment fragment);

    void onDestroyView(BaseFragment fragment);

    void onDestroy(BaseFragment fragment);

    void onDetach(BaseFragment fragment);
}
