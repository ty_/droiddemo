package run.yang.common.ui.template;

import android.app.ListActivity;
import android.view.View;

import androidx.annotation.IdRes;
import run.yang.common.util.view.V;

/**
 * Created by Tim Yang on 2015-04-21 11:06.
 */
public abstract class BaseListActivity extends ListActivity {

    public <T extends View> T findView(@IdRes int viewId) {
        return V.findView(this, viewId);
    }
}
