package run.yang.common.ui.recycler.adapter;

import android.R;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import run.yang.common.util.view.V;

/**
 * Created by Yang Tianmei on 2015-09-21.
 */
public abstract class TitleContentAdapter<T> extends BaseRecyclerViewAdapter<T, TitleContentAdapter.ViewHolder> {

    public TitleContentAdapter(@NonNull Context context, @NonNull List<T> list) {
        super(context, list);
        setItemLayoutResId(R.layout.simple_list_item_2);
    }

    @Override
    protected ViewHolder newViewHolder(View rootView) {
        return new ViewHolder(rootView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView content;

        public ViewHolder(View itemView) {
            super(itemView);
            title = V.findView(itemView, R.id.text1);
            content = V.findView(itemView, R.id.text2);
        }
    }
}
