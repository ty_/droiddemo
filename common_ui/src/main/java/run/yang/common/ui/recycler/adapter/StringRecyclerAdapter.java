package run.yang.common.ui.recycler.adapter;

import android.R;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import run.yang.common.util.view.V;

/**
 * Created by Yang Tianmei on 2015-10-08.
 */
public abstract class StringRecyclerAdapter<ItemT> extends BaseRecyclerViewAdapter<ItemT, StringRecyclerAdapter.ViewHolder> {

    public StringRecyclerAdapter(@NonNull Context context, @NonNull List<ItemT> list) {
        super(context, list);
        setItemLayoutResId(R.layout.simple_list_item_1);
    }

    @Override
    protected ViewHolder newViewHolder(View rootView) {
        return new ViewHolder(rootView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = V.findView(itemView, R.id.text1);
        }
    }
}
