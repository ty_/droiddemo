package run.yang.common.ui.recycler.view;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

/**
 * Created by Yang Tianmei on 2015-09-04 17:08.
 */
public class DividerLinearRecyclerView extends DividerRecyclerView {
    public DividerLinearRecyclerView(final Context context) {
        super(context);
        init(context);
    }

    public DividerLinearRecyclerView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DividerLinearRecyclerView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(@NonNull Context context) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        setLayoutManager(linearLayoutManager);
    }
}
