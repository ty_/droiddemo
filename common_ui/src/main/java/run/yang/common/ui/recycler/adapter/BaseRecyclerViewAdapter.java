package run.yang.common.ui.recycler.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Yang Tianmei on 2015-10-08.
 */
public abstract class BaseRecyclerViewAdapter<ItemT, HolderT extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<HolderT> {

    /**
     * subclass must call {@link #setItemLayoutResId(int)} to change it.
     */
    @LayoutRes
    private int mItemLayoutResId = 0;

    protected final Context mContext;
    protected List<ItemT> mDataList;
    protected final LayoutInflater mLayoutInflater;

    public BaseRecyclerViewAdapter(@NonNull Context context, @NonNull List<ItemT> list) {
        mContext = context;
        mDataList = list;
        mLayoutInflater = LayoutInflater.from(context);
    }

    public void setDataList(@Nullable List<ItemT> dataList) {
        if (dataList == null) {
            mDataList.clear();
        } else {
            mDataList = dataList;
        }
    }

    protected abstract void onBindView(HolderT holder, ItemT item, int position);

    protected abstract HolderT newViewHolder(View rootView);

    @Override
    public HolderT onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = mLayoutInflater.inflate(mItemLayoutResId, parent, false);
        return newViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HolderT holder, int position) {
        final ItemT item = mDataList.get(position);
        onBindView(holder, item, position);
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public int getItemLayoutResId() {
        return mItemLayoutResId;
    }

    public void setItemLayoutResId(int itemLayoutResId) {
        mItemLayoutResId = itemLayoutResId;
    }
}
