package run.yang.common.ui.template;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.fragment.app.ListFragment;

/**
 * Created by Tim Yang on 2015-04-27 21:30.
 */
public abstract class BaseListFragment extends ListFragment {

    @SuppressWarnings("NullableProblems")
    @NonNull
    protected Activity mContextActivity;

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        mContextActivity = activity;
        super.onAttach(activity);
    }
}
