package run.yang.common.ui.recycler.adapter;

import android.content.Context;

import java.util.List;

import androidx.annotation.NonNull;

/**
 * Created by Yang Tianmei on 2015-10-08.
 */
public class SimpleTitleContentAdapter extends TitleContentAdapter<TitleContentPair> {

    public SimpleTitleContentAdapter(@NonNull Context context, @NonNull List<TitleContentPair> list) {
        super(context, list);
    }

    @Override
    protected void onBindView(ViewHolder holder, TitleContentPair item, int position) {
        holder.title.setText(item.title);
        holder.content.setText(item.content);
    }
}
