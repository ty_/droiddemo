package run.yang.common.ui.recycler.adapter;

import android.content.Context;

import java.util.List;

import androidx.annotation.NonNull;

/**
 * Created by Yang Tianmei on 2015-10-08.
 */
public class SimpleStringRecyclerAdapter<T extends CharSequence> extends StringRecyclerAdapter<T> {

    public SimpleStringRecyclerAdapter(@NonNull Context context, @NonNull List<T> list) {
        super(context, list);
    }

    @Override
    protected void onBindView(ViewHolder holder, CharSequence item, int position) {
        holder.textView.setText(item);
    }
}
