package run.yang.common.ui.recycler.view;

import android.content.Context;
import android.util.AttributeSet;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Yang Tianmei on 2015-08-24 20:30.
 */
public class DividerRecyclerView extends RecyclerView {

    public DividerRecyclerView(final Context context) {
        super(context);
        init(context);
    }

    public DividerRecyclerView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DividerRecyclerView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        setItemAnimator(new DefaultItemAnimator());
        addItemDecoration(new RecyclerViewDividerItemDecoration(context));
    }
}
