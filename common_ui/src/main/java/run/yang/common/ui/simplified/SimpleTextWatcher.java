package run.yang.common.ui.simplified;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by zisxks on 2015-08-13 01:42.
 */
public class SimpleTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}
