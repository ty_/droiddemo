package run.yang.common.ui.template;

import android.view.View;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import run.yang.common.util.view.V;


/**
 * Created by Tim Yang on 2015-04-21 11:02.
 */
public abstract class BaseActivity extends AppCompatActivity {

    public <T extends View> T findView(@IdRes int viewId) {
        return V.findView(this, viewId);
    }
}
