package run.yang.common.ui.template;

import android.content.Intent;

import run.yang.common.util.build.BuildControl;
import run.yang.common.util.component.IntentTool;
import run.yang.common.util.log.Logg;

/**
 * 对外暴露接口的Activity的基类，即设置了 intent-filter和 android:exporetd为 true的Activity
 * <p>
 * 基类处理了一些漏洞，如 <a href="http://jaq.alibaba.com/blog.htm?id=55">Android应用本地拒绝服务漏洞</a>
 * <p>
 * Created by Tim Yang on 2016-03-28 16:25.
 */
public abstract class BaseExportedActivity extends BaseActivity {

    private static final String TAG = Logg.getTag(BaseExportedActivity.class);

    private Intent mOriginalIntent = null;
    private Intent mIncomingIntent = null;

    @Override
    public Intent getIntent() {
        if (mIncomingIntent != null) {
            return mIncomingIntent;
        }
        mOriginalIntent = super.getIntent();
        if (IntentTool.checkIncomingIntent(mOriginalIntent)) {
            mIncomingIntent = mOriginalIntent;
        } else {
            if (BuildControl.doLog) {
                Logg.e(TAG, this.getClass().getSimpleName() + ": malformed Incoming intent, replaced with empty intent");
            }
            mIncomingIntent = new Intent();
            setIntent(mIncomingIntent);
        }
        return mIncomingIntent;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        mOriginalIntent = intent;
        if (IntentTool.checkIncomingIntent(mOriginalIntent)) {
            mIncomingIntent = mOriginalIntent;
        } else {
            if (BuildControl.doLog) {
                Logg.w(TAG, this.getClass().getSimpleName() + ": malformed Incoming intent, replaced with empty intent");
            }
            mIncomingIntent = new Intent();
        }
        setIntent(mIncomingIntent);
    }

    protected Intent getOriginalIntent() {
        return mIncomingIntent;
    }
}
